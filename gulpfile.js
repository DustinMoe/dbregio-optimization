const gulp =            require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const plugins =         gulpLoadPlugins();
const autoprefixer =    require('gulp-autoprefixer');
const concat =          require('gulp-concat');
const rename =          require('gulp-rename');
const uglify =          require('gulp-uglify');
const pump =            require('pump');
const cleanCSS =        require('gulp-clean-css');

var jsFiles = 'tickeos-customization-dbregio/plugins/oepnvStyleDBRegioPlugin/web/js/';
var jsTemp = 'tickeos-customization-dbregio/plugins/oepnvStyleDBRegioPlugin/web/js/temp';
var jsFinal = 'tickeos-customization-dbregio/plugins/oepnvStyleDBRegioPlugin/web/js/final';
var cssFiles = 'tickeos-customization-dbregio/plugins/oepnvStyleDBRegioPlugin/web/css/**/*.css';
var cssPath = 'tickeos-customization-dbregio/plugins/oepnvStyleDBRegioPlugin/web/css/';
var cssTemp = 'tickeos-customization-dbregio/plugins/oepnvStyleDBRegioPlugin/web/css/temp';
var cssFinal = 'tickeos-customization-dbregio/plugins/oepnvStyleDBRegioPlugin/web/css/final';

// concat all css files
gulp.task('concatCSS',function(){
    return gulp.src([cssPath + 'reset-min.css',
                     cssPath + 'common.css',
                     cssPath + 'standard_components.css',
                     cssPath + 'customer_common.css' ,
                     cssPath + 'layout.css',
                     cssPath + 'navigation.css',
                     cssPath + 'components.css',
                     cssPath + 'home_simple_search.css',
                     cssPath + 'print.css'])
        .pipe(concat('all.css'))
        .pipe(gulp.dest(cssTemp));
});

// concat all js files
gulp.task('concatJS',function(){
   return gulp.src([jsFiles + 'jquery.js',
                    jsFiles + 'jquery-plugins.js',
                    jsFiles + 'common.js' ,
                    jsFiles + 'shop.js',
                    jsFiles + 'jq-datepicker.js'])
       .pipe(concat('all.js'))
       .pipe(gulp.dest(jsTemp));
});

// run autoprefixer
gulp.task('autoprefixer', function(){
   var postcss = require('gulp-postcss');
   var sourcemaps = require('gulp-sourcemaps');
   var autoprefixer = require('autoprefixer');

   return gulp.src(cssTemp + '/all.css')
       .pipe(sourcemaps.init())
       .pipe(postcss([ autoprefixer() ]))
       .pipe(sourcemaps.write('.'))
       .pipe(rename({
           suffix: '.autoprefixer'
       }))
       .pipe(gulp.dest(cssTemp));
});

// minify of all css files
gulp.task('minify', function(){
    gulp.src(cssTemp + '/all.autoprefixer.css')
        .pipe(cleanCSS())
        .pipe(rename({
            basename: 'eos-all',
            suffix: '.min'
        }))
        .pipe(gulp.dest(cssPath))
    ;
});

// uglify all js files
gulp.task('compress', function(cb){
   pump([
       gulp.src(jsTemp + '/*.js'),
       uglify(),
       rename({suffix: '.min'}),
       gulp.dest(jsFinal)
    ],
    cb
   )
});



gulp.task('buildJS', ['concatJS', 'compress'], function(){

});

gulp.task('buildCSS', ['concatCSS', 'autoprefixer', 'minify'], function(){

});

gulp.task('build', ['concatCSS', 'autoprefixer', 'minify', 'concatJS', 'compress'], function(){

});

gulp.task('default', function(){
    // Default task code
    console.log('Test: Gulp')
});