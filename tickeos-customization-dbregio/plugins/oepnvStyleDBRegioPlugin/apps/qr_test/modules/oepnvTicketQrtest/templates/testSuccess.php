<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $sf_user->getCulture(); ?>" lang="<?php echo $sf_user->getCulture(); ?>">
  <head>
      <title></title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta name="viewport" content="width=480" />
      <link rel="stylesheet" type="text/css" href="<?php echo _compute_public_path_cdn('screen',sfConfig::get('app_plugin_layout_asset_dir','layout').'/css','css'); ?>" media="screen" />
      <link rel="stylesheet" type="text/css" href="<?php echo _compute_public_path_cdn('iphone',sfConfig::get('app_plugin_layout_asset_dir','layout').'/css','css'); ?>" media="only screen and (max-device-width: 480px)" />
  </head>
  <body>
       <div class="head">
          <a href="../qr_test.php/test"><img alt="Logo" class="logo" src="../layout_qr/images/logo.gif"/></a>
       </div>
      <div class="page">
        <div class="content">
          <h2><?php echo __('Ticket-Daten', null, 'qr_test'); ?></h2>
          <form action="<?php echo url_for('@test') ?>" method="post">
          <?php include_partial('global/form_header',array('form'=>$form)) ?>
          <table>
          <?php foreach ($form->getFormFieldSchema() as $field): ?>
            <?php if (!($field->getWidget() instanceof sfWidgetFormInputHidden)): ?>
              <?php include_partial('global/render_basic_table_row', array('ff' => $field)); ?>
            <?php endif; ?>
          <?php endforeach; ?>
          </table>
          <div class="cell button">
            <input type="submit" value="<?php echo __('Prüfen',array(),'qr_code') ?>" name="go">
          </div>
          </form>
        </div>
      </div>
  </body>
</html>