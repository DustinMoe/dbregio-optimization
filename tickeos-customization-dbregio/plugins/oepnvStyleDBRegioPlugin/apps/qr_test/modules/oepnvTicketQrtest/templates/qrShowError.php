<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $sf_user->getCulture(); ?>" lang="<?php echo $sf_user->getCulture(); ?>">
  <head>
      <title></title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta name="viewport" content="width=480" />
      <link rel="stylesheet" type="text/css" href="<?php echo _compute_public_path_cdn('screen',sfConfig::get('app_plugin_layout_asset_dir','layout').'/css','css'); ?>" media="screen" />
      <link rel="stylesheet" type="text/css" href="<?php echo _compute_public_path_cdn('iphone',sfConfig::get('app_plugin_layout_asset_dir','layout').'/css','css'); ?>" media="only screen and (max-device-width: 480px)" />
  </head>
  <body>
       <div class="head">
          <a href="../qr_test.php/test"><img alt="Logo" class="logo" src="/layout_qr/images/logo.gif"/></a>
       </div>
      <div class="page">
        <table>
          <tr class='invalid'>
            <td>        
              <p><?php echo $error ?></p>
            </td>
          </tr>
       </table>
      </div>
  </body>
</html>