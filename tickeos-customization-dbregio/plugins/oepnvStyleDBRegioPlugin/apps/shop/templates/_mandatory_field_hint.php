<?php $whitelist = array(
  'customer_register_process',
  'customer_login_credentials',
  'payment_manage',
  'payment',
  'customer_modify',
  'contact_form',
  'anonymous_customer',
); ?>
<?php $current_route_name = sfContext::getInstance()->getRouting()->getCurrentRouteName(); ?>

<?php if(sfConfig::get('app_form_append_star_to_required_fields',true) == 1 && in_array($current_route_name, $whitelist)): ?>
  <p class="required-fields">* <?php echo __('Pflichtfeld',array(),'common');?></p>
<?php endif; ?>