<?php if (isset($add_css_reset) && $add_css_reset): ?>
<!--  <link rel="stylesheet" type="text/css" href="--><?php //echo _compute_public_path_cdn('reset-min','css','css'); ?><!--" />-->
<?php endif; ?>
<!--<link rel="stylesheet" type="text/css" href="--><?php //echo _compute_public_path_cdn('common','css','css'); ?><!--" />-->
<!--<link rel="stylesheet" type="text/css" href="--><?php //echo _compute_public_path_cdn('standard_components','css','css'); ?><!--" />-->
<?php if (isset($v2) && $v2): ?>
<!--  <link rel="stylesheet" type="text/css" href="--><?php //echo _compute_public_path_plugable_cdn('common','css','css'); ?><!--" />-->
<!--  <link rel="stylesheet" type="text/css" href="--><?php //echo _compute_public_path_plugable_cdn('layout','css','css'); ?><!--" />-->
<?php else: ?>
  <link rel="stylesheet" type="text/css" href="<?php echo _compute_public_path_plugable_cdn('generic','css','css'); ?>" />
  <link rel="stylesheet" type="text/css" href="<?php echo _compute_public_path_plugable_cdn('structure','css','css'); ?>" />
<?php endif; ?>
<?php if (isset($jcrop) && $jcrop): ?>
  <link rel="stylesheet" type="text/css" media="screen" href="<?php echo _compute_public_path_cdn('jcrop','css','css'); ?>" />
<?php endif; ?>
<!--<link rel="stylesheet" type="text/css" href="--><?php //echo _compute_public_path_plugable_cdn('navigation','css','css'); ?><!--" />-->
<!--<link rel="stylesheet" type="text/css" href="--><?php //echo _compute_public_path_plugable_cdn('components','css','css'); ?><!--" />-->
<!--<link rel="stylesheet" type="text/css" href="--><?php //echo _compute_public_path_plugable_cdn('home_simple_search','css','css'); ?><!--" />-->
<!--<link rel="stylesheet" type="text/css" media="print" href="--><?php //echo _compute_public_path_plugable_cdn('print','css','css'); ?><!--" />-->

<link rel="stylesheet" type="text/css" href="<?php echo _compute_public_path_plugable_cdn('eos-all.min.css','css','css'); ?>" />


<!--<script type="text/javascript" src="--><?php //echo _compute_public_path_plugable_cdn('jquery','js','js'); ?><!--"></script>-->
<!--<script type="text/javascript" src="--><?php //echo _compute_public_path_plugable_cdn('jquery-plugins','js','js'); ?><!--"></script>-->
<!--<script type="text/javascript" src="--><?php //echo _compute_public_path_plugable_cdn('common','js','js'); ?><!--"></script>-->
<!--<script type="text/javascript" src="--><?php //echo _compute_public_path_plugable_cdn('shop','js','js'); ?><!--"></script>-->
<!--<script type="text/javascript" src="--><?php //echo _compute_public_path_plugable_cdn('jq-datepicker','js','js'); ?><!--"></script>-->
<script type="text/javascript" src="<?php echo _compute_public_path_plugable_cdn('all.min.js','js/final','js'); ?>"></script>

<?php if (isset($jcrop) && $jcrop): ?>
  <script type="text/javascript" src="<?php echo _compute_public_path_cdn('jcrop','js','js'); ?>"></script>
<?php endif; ?>

<!--[if lte IE 6]>
<script type="text/javascript" src="<?php echo _compute_public_path_cdn('ie-lt-7','js','js'); ?>"></script>
<![endif]-->

<?php if (sfConfig::get('sf_debug',false) && sfConfig::get('app_firebug_lite_enabled',false)): ?>
  <script type="text/javascript" src="https://getfirebug.com/firebug-lite.js"></script>
<?php endif; ?>
