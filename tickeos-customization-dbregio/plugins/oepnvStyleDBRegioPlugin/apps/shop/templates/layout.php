<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $sf_user->getCulture(); ?>" lang="<?php echo $sf_user->getCulture(); ?>">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <?php include_partial('global/common_head_parts', array('v2' => true, 'add_css_reset' => true)); ?>
    <link rel="canonical" href="<?php echo $sf_request->getURI(); ?>" />
  </head>
  <body>
    <div id="wrapper">
      <div id="head">
        <div class="left">
          <a href="<?php echo url_for('@homepage', true); ?>"><?php echo image_tag_plugable('logo-db.png', array('alt' => 'DB Regio')); ?></a>
        </div>
        <div class="right">
          <?php if (!sfConfig::get('app_disable_language_select', true)): ?>
            <ul class="languages">
              <?php
              foreach (LanguagePeer::getVisibleLanguages() as $language)
              {
                echo '<li><a class="lang-'. $language->getCulture() .'" href="' .url_for('generic/setCulture?culture='. $language->getCulture()) . '?return='. urlencode($sf_request->getURI()) . '">'. $language->getCulture() .'</a></li>';
              }
              ?>
            </ul>
          <?php endif; ?>
          <?php include_component('generic', 'searchBox') ?>
        </div>
      </div>
      <div id="nav">
        <?php echo $navi = get_component('eosNavigation', 'show', array('name' => 'navi', 'level' => 0, 'depth' => 0)); ?>
      </div>
      <div id="content-all">
        <div id="visual">

          <?php
          $base = '';
          $conf_base = sfConfig::get('app_title_base', sfConfig::get('app_client_name'));
          if ($conf_base !== null)
          {
            $base = $conf_base . ' ' . sfConfig::get('app_title_seperation_character', '-') . ' ';
          }

          $title = sfContext::getInstance()->getResponse()->getTitle();

          if (sfContext::getInstance()->getModuleName() !== 'home')
          {
            echo '<div class="bar">';
            echo '<a href="' . url_for('@homepage', true) . '">' . __('Startseite', null, 'navigation') . '</a> <span>&raquo;</span> ' . str_replace($base, '', $title);
            echo '</div>';
          }
          ?>


          <?php if (sfContext::getInstance()->getActionName() == 'index' && sfContext::getInstance()->getModuleName() == 'home'): ?>
            <div class="image"><?php echo image_tag_plugable('header_neu.png', array('alt' => 'S-Bahn RheinNeckar')); ?></div>
          <?php endif; ?>
        </div>

        <div id="sidebar">
          <div id="sidenav" class="box">

            <?php
            $navi = get_component('eosNavigation', 'show', array('name' => 'subnavi', 'level' => 1, 'depth' => 3, 'active_path_only' => true, 'cur_node' => eosNavigation::getInstance()->getNode('ticket')));
            if ($navi === '')
            {
              $navi = get_component('eosNavigation', 'show', array('name' => 'subnavi', 'level' => 1, 'depth' => 3, 'active_path_only' => true, 'cur_node' => eosNavigation::getInstance()->getNode('ticket'), 'active_path_override' => 'ticket|product_category_1'));
            }
            echo $navi;
            ?>

          </div>

          <div class="box widget"><?php include_component('shop', 'shoppingCart') ?></div>
          <?php if (sfContext::getInstance()->getModuleName() != 'sfGuardAuth'): ?>
            <div class="box widget"><?php include_component('personal_data', 'loginBox') ?></div>
          <?php endif; ?>
          <?php $news_component = get_component('oepnvNews', 'latest') ?>
          <?php if ($news_component != '' && sfContext::getInstance()->getModuleName() != 'oepnvNews'): ?>
            <div class="userpanel-inner-top"><div class="userpanel-inner-bottom"><div class="userpanel-inner-left"><div class="userpanel-inner-right"><h2><?php echo __('News', null, 'news') ?></h2><?php echo($news_component); ?></div></div></div></div>
          <?php endif; ?>
          <?php if (sfContext::getInstance()->getActionName() === 'anonymousCustomer' && sfContext::getInstance()->getModuleName() === 'personal_data'): ?>
            <div class="box widget payment">
              <h2><?php echo __('Zahlungsverfahren', null, 'payment') ?></h2>
              <p><?php echo __('Neben dem normalen Lastschriftverfahren werden auch folgende Kreditkarten akzeptiert:', null, 'payment') ?></p>
              <?php echo image_tag_plugable('payment_mastercard.png', array('alt' => 'MasterCard', 'class' => 'payment_logo', 'id' => 'mastercard', 'height' => '40px')); ?>
              <?php echo image_tag_plugable('payment_amex.png', array('alt' => 'American Express', 'class' => 'payment_logo', 'id' => 'amex', 'height' => '40px')); ?>
              <?php echo image_tag_plugable('payment_visa.png', array('alt' => 'Visa', 'class' => 'payment_logo', 'id' => 'visa', 'height' => '40px')); ?>
              <div class="dedicated-clear"></div>
            </div>
          <?php endif; ?>
        </div>



        <div id="content" class="content <?php echo sfContext::getInstance()->getModuleName() . '-' . sfContext::getInstance()->getActionName(); ?>">

          <div class="no_cookies">
            <h4 class="error">
              <?php echo __('Bitte aktivieren Sie "Cookies", um fortzufahren!', array(), 'shop') ?>
            </h4>
            <?php $cms_page = new eosCmsPage(CmsPagePeer::getWithI18nByType('cookies')); ?>
            <p>
              <?php echo __('Die Einstellungen Ihres Browsers verhindern, dass Sie Tickets online bestellen können.', array(), 'shop') ?>
              <br />
              <?php echo __('Weitere Informationen über Cookies und deren Aktivierung erhalten Sie ', array(), 'shop') ?><a href="<?php echo url_for($cms_page->getRoute()); ?>">
              <?php echo __('hier.', array(), 'shop') ?></a>
            </p>
          </div>

          <?php echo $sf_content ?>

        </div>

        <!--        <div id="sidebar" <?php #if (sfContext::getInstance()->getActionName() == 'index' && sfContext::getInstance()->getModuleName() == 'home'):     ?>class="index" <?php #endif;     ?> >

        </div>-->

      </div>
      <div id="footer">
        <div class="nav">
          <span class="copyright">&copy; 2013 Deutsche Bahn AG</span>
          <?php echo $navi = get_component('eosNavigation', 'show', array('name' => 'footer_navi', 'level' => 0, 'depth' => 0, 'navigation_name' => 'head_navi')); ?>
        </div>
      </div>
    </div>
    <?php include_component('tracking', 'insertCode') ?>
    <script language="javascript">
        function CheckCookies()
        {
            document.cookie = "name=vrn-online";

            if (!document.cookie)
            {
                $('.no_cookies').show();
            }
        }
        $(document).ready(CheckCookies);

        $(document).ready(function() {
            $('<img alt="" class="tooltip" src="/layout/images/icon_info-i_25.jpg" />').insertAfter('#sidebar .box .login h2');

            $('#sidebar .box .login .tooltip').simpletip({
                baseClass: 'dbregio_customer_login_info',
                fixed: false,
                offset: [0, -110],
                content: '<?php echo __('Sollten Sie angemeldeter Nutzer bei www.bahn.de sein, beachten Sie bitte, dass für www.dbregio-shop.de eine neue Registrierung erforderlich ist.', array(), 'shop') ?>'
            });
        });
    </script>
  </body>
</html>
