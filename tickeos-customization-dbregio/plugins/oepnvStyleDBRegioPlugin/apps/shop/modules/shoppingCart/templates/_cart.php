<?php use_helper('Currency', 'Number', 'Date'); ?>
<?php if (!isset($edit)) $edit=false; ?>
<?php $optional_cols = 0 ?>
<table class="cart" border="0">
  <?php
    $amount_width = sfConfig::get('app_shopping_cart_col_width_amount', 5);
    $price_width = sfConfig::get('app_shopping_cart_col_width_price', 10);
    $edit_col_width = sfConfig::get('app_shopping_cart_col_width_edit', 7);
    $personalization_width = sfConfig::get('app_shopping_cart_col_width_personalization', 20);
    $distribution_width = sfConfig::get('app_shopping_cart_col_width_distribution', 10);
    $main_col_width = 100 - 2 * $price_width - $amount_width;
    if ($edit)
    {
      $main_col_width = $main_col_width - $edit_col_width;
    }
    if ($shoppingCart->hasPersonalizableItem())
    {
      $main_col_width = $main_col_width - $personalization_width;
    }

    if (DistributionMethodRegistry::getInstance()->hasMultipleMethods() && !sfConfig::get('app_shopping_cart_hide_col_type', false))
    {
      $main_col_width = $main_col_width - $distribution_width ;
    }
  ?>

  <colgroup>
    <?php if (!sfConfig::get('app_shopping_cart_hide_col_amount', false)): ?><col width="<?php echo $amount_width;?>%" /><?php endif ?>
    <col width="<?php echo $main_col_width;?>%" />
    <?php if ($shoppingCart->hasPersonalizableItem()): ?>
    <col width="<?php echo $personalization_width;?>%" />
    <?php endif; ?>
    <?php if (DistributionMethodRegistry::getInstance()->hasMultipleMethods() && !sfConfig::get('app_shopping_cart_hide_col_type', false)):?>
    <col width="<?php echo $distribution_width;?>%" />
    <?php endif; ?>
		<?php if (!sfConfig::get('app_shopping_cart_hide_col_unit_price', false)): ?>
    <col width="<?php echo $price_width;?>%" />
		<?php endif; ?>
    <col width="<?php echo $price_width;?>%" />
    <?php if ($edit): ?>
    <col width="<?php echo $edit_col_width;?>%" />
    <?php endif; ?>
  </colgroup>
  <thead>
    <tr>
      <?php if (!sfConfig::get('app_shopping_cart_hide_col_amount', false)): ?>
      <th class="cart_amount">
        <?php echo __('Menge', array(), 'cart')?>
      </th>
      <?php endif ?>
      <th class="cart_description">
        <?php echo __('Produktbeschreibung', array(), 'cart')?>
      </th>
      <?php if ($shoppingCart->hasPersonalizableItem()):
        $optional_cols++;
      ?>
      <th class="cart_for">
        <?php echo __('Für', array(), 'cart')?>
      </th>
      <?php endif;?>
      <?php
      if (DistributionMethodRegistry::getInstance()->hasMultipleMethods() && !sfConfig::get('app_shopping_cart_hide_col_type', false)):
        $optional_cols++;
      ?>
      <th class="cart_type">
        <?php echo __('Typ', array(), 'cart')?>
      </th>
      <?php endif; ?>
			<?php if (!sfConfig::get('app_shopping_cart_hide_col_unit_price', false)):  ?>
      <th class="cart_unit_price">
        <?php echo __('Einzelpreis', array(), 'cart')?>
      </th>
			<?php endif; ?>
      <th class="cart_all_round_price">
        <?php echo __('Gesamtpreis', array(), 'cart')?>
      </th>
      <?php if ($edit): ?>
      <th class="cart_options">&nbsp;</th>
      <?php endif; ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $counter = 0;
      foreach ($shoppingCart->getItems() as $item):
      if($item->showInCart()):
        $product = $shoppingCart->getObject($item->getClass(), $item->getId());
        $counter ++;
        $row_class = ($counter%2 == 0) ? 'odd' : 'even';
    ?>
    <tr class="<?php echo $row_class; ?>">
      <?php if (!sfConfig::get('app_shopping_cart_hide_col_amount', false)): ?>
      <td id="rowAmount<?php echo $counter ?>"><?php echo $item->getQuantity(); ?></td>
      <?php endif ?>
      <td>
        <?php include_partial('shoppingCart/cart_item_info', array('item' => $item)); ?>
      </td>
      <?php if ($shoppingCart->hasPersonalizableItem()): ?>
      <td class="personalization" id="personalizationProduct<?php echo $counter ?>">
        <?php # TODO Workaround: $item->isPersonalizable() liefert auch true wenn es gar kein Personalisierungsfeld im eigentlichen Sinn enthält. Wie z.B. Ausweistyp ?>
        <?php $personalization = ''; ?>
        <?php if ($item->isPersonalizable()): ?>
        <?php $i = 0; ?>
          <?php foreach ($item->getPersonalizationPropertyValueAdapters() as $productPersonalizationProperty): ?>
            <?php if ($productPersonalizationProperty->getVisibleInCart() && $productPersonalizationProperty->getValueText()!=''): ?>
              <?php if (sfConfig::get('app_cart_show_personalization_labels',false)): ?>
                <?php $personalization .= $productPersonalizationProperty->getName() ?>:
              <?php endif; ?>
                    <?php
                    $value = $productPersonalizationProperty->getValueText();
                    if(isset($i) && $i++ == 0)
                    {
                        $value = $productPersonalizationProperty->getValueText().' ';
                    }
                    ?>
                    <?php $personalization .= $value . $productPersonalizationProperty->getRawValue()->getCartPostfix(); ?>
            <?php endif; ?>
          <?php endforeach; ?>
          <?php foreach($item->getAdditionalPersonalizationNames() as $key=>$value):?>
            <?php $personalization .='<p class="pers_addition '.$key.'">'.$value.'</p>';?>
          <?php endforeach;?>
        <?php endif; ?>
        <?php #elseif ($item->isPersonalizable() && !$sf_user->isAuthenticated() && !$item->isPersonalized()): ?>
        <?php if ($personalization == '' && !$item->isAddition()): ?>
			<?php if ($sf_user->isAuthenticated() && sfConfig::get('app_cart_show_personalization_name_from_login',true)): ?>
				<?php $user = $sf_user->getGuardUser()->getProfile();?>
				<?php $personalization = $user->getFirstname()." ".$user->getLastname(); ?>
			<?php else: ?>
				<?php $personalization = __('Besteller',array(),'cart') ?>
			<?php endif; ?>
        <?php endif; ?>
        <?php echo $personalization; ?>
      </td>
      <?php endif; ?>
      <?php if (DistributionMethodRegistry::getInstance()->hasMultipleMethods() && !sfConfig::get('app_shopping_cart_hide_col_type', false)): ?>
      <td>
        <?php echo image_tag_plugable_cdn($item->getDistributionMethodObject()->getIconName(),array('title'=>$item->getDistributionMethodObject()->getName(), 'alt' => '')) ?>
      </td>
      <?php endif; ?>
			<?php if (!sfConfig::get('app_shopping_cart_hide_col_unit_price', false)): ?>
      <td class="price" id="singlePriceProduct<?php echo $counter ?>"><?php echo format_currency_adv($item->getPrice(), 'EUR'); ?></td>
			<?php endif; ?>
      <td class="price" id="totalPriceProduct<?php echo $counter ?>"><?php echo format_currency_adv($item->getQuantity() * $item->getPrice(), 'EUR'); ?><br /><span class="mwst"><?php echo __('inkl.', array(), 'cart') . ' ' .$item->getTax(); ?></span></td>
      <?php if ($edit): ?>
      <td class="actions">
      <?php if ($item->isEditable() && !$shoppingCart->isLocked() && !sfConfig::get('app_shopping_cart_hide_edit_link', false)): ?>
        <?php echo link_to(__('ändern', array(), 'cart'),'@shopping_cart_edit?hash='.$item->getHash(), array('class'=>'modify', 'id'=>'linkToModify'.$counter)); ?>
      <?php endif; ?>
      <?php if ($item->isDeleteable() && !$shoppingCart->isLocked()): ?>
        <?php echo link_to(__('löschen', array(), 'cart'),'@shopping_cart_remove?hash='.$item->getHash(), array('class'=>'remove', 'id'=>'linkToRemove'.$counter)); ?>
      <?php endif; ?>
      </td>
      <?php endif; ?>
      <?php if($product->getAttributeValue('dekt_return_product')):?>
        <?php
        $display_link = true;
        foreach($shoppingCart->getItems() as $i)
        {
            if($i->getId() == $product->getAttributeValue('dekt_return_product'))
            {
                $display_link = false;
                break;
            }
        }
        ?>
        <?php if($display_link):?>
          <tr class="dekt_return">
            <?php $colspan = (1+(!sfConfig::get('app_shopping_cart_hide_col_amount', false) ? 1 : 0)+(!sfConfig::get('app_shopping_cart_hide_col_unit_price', false) ? 1 : 0)+$optional_cols); ?>
            <td colspan="<?php echo $colspan;?>">
              <?php echo link_to(__('Rückfahrt hinzufügen', array(), 'bahn'), '@product_show?product_id='.$product->getAttributeValue('dekt_return_product'), array('class' => ''));?></a>
            </td>
          </tr>
        <?php endif;?>
      <?php endif;?>
    </tr>
    <?php endif; ?>
    <?php endforeach; ?>
    <?php
     $colspan = (1+(!sfConfig::get('app_shopping_cart_hide_col_amount', false) ? 1 : 0)+(!sfConfig::get('app_shopping_cart_hide_col_unit_price', false) ? 1 : 0)+$optional_cols);
    ?>
    <?php if($shoppingCart->hasShippingCosts()):?>
    <?php if($item = $shoppingCart->getShippingCostItem()):?>
    <?php $product = $shoppingCart->getObject($item->getClass(), $item->getId()); ?>
    <tr>
      <th colspan="<?php echo $colspan ?>" scope="row"><?php echo $product->getName(); ?></th>
      <td class="total price"><?php echo format_currency_adv($item->getQuantity() * $item->getPrice(), 'EUR'); ?>
      <?php if ($item->getTaxRate() > 0):?>
      <br /><span class="mwst"><?php echo __('inkl.', array(), 'cart') . ' ' .$item->getTax(); ?></span>
      <?php endif; ?></td>
      <?php if ($edit): ?><td>&nbsp;</td><?php endif; ?>
    </tr>
    <?php endif; ?>
    <?php endif; ?>
    <?php if($shoppingCart->getDiscount()>0):?>
    <tr>
      <th colspan="<?php echo $colspan ?>" scope="row"><?php echo __('Rabatt', array(), 'cart'); ?></th>
      <td class="total"><?php echo format_currency_adv($shoppingCart->getDiscountPrice() * -1, 'EUR'); ?></td>
      <?php if ($edit): ?><td>&nbsp;</td><?php endif; ?>
    </tr>
    <?php endif; ?>


    <?php /* ------------------------------------------------------------- */ ?>
    <?php /* VOUCHER */ ?>
    <?php if (eosLicenseManager::getInstance()->isModuleLicensed(oepnvVoucherEventListener::LICENSE_NAME)) : ?>
    <?php $voucherDiscountTotal = 0.0; ?>
    <?php if($shoppingCart->hasVouchers()): ?>
      <tr>
        <th colspan="<?php echo $colspan ?>" scope="row">
          <?php echo __('Zwischensumme', array(), 'cart'); ?>
        </th>
        <td class="total" id="order-sum-total">
          <?php echo format_currency_adv($shoppingCart->getSubTotal(), 'EUR'); ?>
        </td>
        <?php if ($edit): ?>
          <td>&nbsp;</td>
        <?php endif; ?>
      </tr>
    <?php foreach($shoppingCart->getVouchers() as $voucher): ?>
      <?php include_component('oepnvVoucher', 'voucherCartItem', array('shoppingCart' => $shoppingCart, 'voucher' => $voucher, 'counter' => $counter, 'colspan' => $colspan)) ?>
    <?php endforeach; ?>
    <?php endif; ?>
    <?php endif; ?>
    <?php /* ------------------------------------------------------------- */ ?>


    <?php /* shopping cart total */ ?>
    <tr class="total">
      <th colspan="<?php echo $colspan ?>" scope="row"><?php echo __('Gesamtsumme', array(), 'cart'); ?></th>
      <td class="total" id="order-sum-total"><?php echo format_currency_adv($shoppingCart->getTotal(), 'EUR'); ?></td>
      <?php if ($edit): ?><td>&nbsp;</td><?php endif; ?>
    </tr>

    <tr class="mwst">
      <td colspan="<?php echo $colspan ?>">&nbsp;</td>
      <td colspan="2">
      <?php $tax_totals = $shoppingCart->getTaxTotals(); ?>
      <?php foreach ($shoppingCart->getTaxes() as $tax_rate => $tax_name): ?>
      <?php if ($tax_name != ''): ?>
      <?php echo __('inkl.', array(), 'cart')?> <?php echo $tax_name ?>: <?php echo format_currency_adv($tax_totals[$tax_rate], 'EUR'); ?><br />
      <?php endif; ?>
      <?php endforeach; ?>
      </td>
    </tr>
  </tbody>
</table>