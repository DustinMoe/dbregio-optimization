<?php $payment_method = $payment_method->getRawValue();?>
<?php $distribution_methods = $distribution_methods->getRawValue();?>

<div id="order_overview_customer_address_payment">
    <div id="order_overview_customer_address">
        <div id="order_overview_customer_delivery_address" class="left">
            <h2><?php echo __('Lieferadresse', array(), 'order_overview'); ?></h2>
            <div class="content">
                <?php if(!$sf_user->getShoppingCart()->hasDistributionMethod('shipping')):?>
                    <?php echo __('Diese Bestellung enthält keine Versandprodukte.', array(), 'order_overview'); ?>
                <?php elseif (oepnvCustomerFactory::getInstance()->getOepnvCustomerAuthentification()->hasCapability(oepnvCustomerAuthentification::CAPABILITY_DELIVERY_ADDRESS) && $customer->getUseDeliveryAddress()): ?>
                    <?php include_partial('customer_delivery_address', array('customer' => $customer)); ?>
                <?php else: ?>
                    <?php include_partial('customer_contract_address', array('customer' => $customer)); ?>
                <?php endif; ?>
                <?php if($sf_user->getShoppingCart()->hasDistributionMethod('shipping')):?><a class="delivery_address_edit edit_link" id="order_overview_customer_delivery_address_edit_link" href="<?php echo url_for('@customer_modify'); ?>"><?php echo __('ändern', array(), 'order_overview'); ?></a><?php endif; ?>
            </div>
        </div>
        <div id="order_overview_customer_contract_address" class="right">
            <h2><?php echo __('Rechnungsadresse', array(), 'order_overview'); ?></h2>
            <div class="content">
                <?php include_partial('customer_contract_address', array('customer' => $customer)); ?>
                <a class="contract_address_edit edit_link" id="order_overview_customer_contract_address_edit_link" href="<?php echo url_for('@customer_modify'); ?>"><?php echo __('ändern', array(), 'order_overview'); ?></a>
            </div>
        </div>
        <div class="dedicated-clear"></div>
    </div>
    <div id="order_overview_customer_distribution_payment">
        <div id="order_overview_customer_distribution" class="left">
            <h2><?php echo __('Zustellart', array(), 'order_overview'); ?></h2>
            <div class="content">
                <?php foreach($distribution_methods as $distribution_method):?>
                    <?php echo $distribution_method->getName(); ?><br/>
                <?php endforeach; ?>
            </div>
        </div>
        <div id="order_overview_customer_payment" class="right">
            <h2><?php echo __('Zahlungsweise', array(), 'order_overview'); ?></h2>
            <div class="content">
                <?php echo __($payment_method->getPaymentMethod()->getName(), array(), 'payment'); ?>
                <a class="payment_edit edit_link" id="order_overview_customer_payment_edit_link" href="<?php echo url_for('@payment'); ?>"><?php echo __('ändern', array(), 'order_overview'); ?></a>
            </div>
        </div>
        <div class="dedicated-clear"></div>
    </div>
</div>
