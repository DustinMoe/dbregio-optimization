<?php include_partial('head') ?>

<?php use_helper('Text')?>
  <?php if(sfConfig::get('app_navi_product_use_subnavi_partial', false)): ?>
    <?php slot('sub_navigation'); ?>    
    <?php include_partial('subnavi', array('product' => (isset($product) ? $product : null), 'category' => (isset($category) ? $category : null), 'category_type' => $category_type)); ?>
    <?php end_slot();?>
  <?php endif; ?>
<?php #include_component('generic','shopNavigation', array('caption_pos' => 'above')) ?>

<?php include_partial('cms_before_actual_content', array('cms_page' => $cms_page)); ?>

<div class="actual-content">

<?php //include_partial('cms_inside_actual_content', array('cms_page' => $cms_page)); ?>

<?php # Entweder die allgemeinen Kategorie bzw. Produktauflistung benutzen, oder pro Kategorietyp noch unterscheiden ?>
<?php if (isset($category) && $category !== null):?>
  <?php if (isset($subcategory) && $subcategory !== null):?>
    <?php include_partial(sfConfig::get('app_product_list_options_product_list_partial_'. $category_type, 'product_list'), array('category_type' => $category_type, 'category' => $subcategory, 'product_list' => array())); ?>
  <?php else:?>
    <?php
    $cms_page = CmsPagePeer::getWithI18nByAlias('category_'. $category->getId());
    if ($cms_page)
    {
      include_partial('homeTop', array('cms_page' => new eosCmsPage($cms_page)));
    } else
    {
      include_partial('homeTop', array('cms_page' => new eosCmsPage(CmsPagePeer::getWithI18nByType('home'))));
    }
    ?>
  <?php endif; ?>
<?php else:?>
  <?php include_partial(sfConfig::get('app_category_list_options_category_list_partial_'. $category_type, 'category_list'), array('category_type' => $category_type, 'categories' => $categories)); ?>
<?php endif; ?>

</div>
