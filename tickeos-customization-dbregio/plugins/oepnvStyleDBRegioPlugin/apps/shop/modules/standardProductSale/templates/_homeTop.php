<?php if (isset($cms_page) && $cms_page !== null):?>
	<?php 
        $counter = 0;
        $output = '';
        foreach($cms_page->getBlocks() as $block)
        {
            if($block->getType() === 'image')
            {
                if($counter % 2 !== 0)
                {
                    $output .= '<div id="product-box-' . $counter . '" class="%BOX_CLASS% last">';
                }
                else
                {
                  $output .= '<div id="product-box-' . $counter . '" class="%BOX_CLASS%">';
                }

                $output .= $block->getRawValue()->render();
                
                $counter = $counter + 1;
            }
            else if($block->getType() === 'text')
            {
                $output .= $block->getRawValue()->render();
                $output .= "</div>";
            }
            else
            {
                $output .=$block->getRawValue()->render();
            }
        }

        if($counter == 1)
        {
          $pb_class = 'product-box-single';
        }
        else
        {
          $pb_class = 'product-box';
        }
        $output = str_replace('%BOX_CLASS%', $pb_class, $output);

        echo $output;
        ?>
<?php endif; ?>