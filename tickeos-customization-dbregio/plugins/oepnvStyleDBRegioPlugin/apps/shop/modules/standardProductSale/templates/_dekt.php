<?php
$widgetSchema = $form->getWidgetSchema();
$product_type = ProductTypePeer::getFromRole('strecke');
$max = 6;
?>
<?php if($product_type):?>

<?php
  $element = '#product_product_option_'.$product_type->getId().'_product';
  $strecke = $widgetSchema['product_option_'.$product_type->getId()];?>
<?php if($strecke):?>

<?php $select = $strecke['product'];?>
<?php if($select && $select instanceof sfWidgetFormSelect):?>
    <?php
      $default_strecke = $form->getDefault('product_option_'.$product_type->getId());

      $defaults = $form->getTaintedValues();
      if($defaults != NULL)
      {
        $default_strecke = $defaults['product_option_'.$product_type->getId()];
      }
      if(array_key_exists('product', $default_strecke))
      {
        $default_strecke = $default_strecke['product'];
      }
      else
      {
        $default_strecke = NULL;
      }
      $arr_choices = $select->getOption('choices');
      $arr_choices_real = array();
      $anz = count($arr_choices);
      $divisor = $anz;

      if($anz > $max)
      {
        $i = 2;
        while ($divisor > $max)
        {
          $divisor = $anz / $i;
          $i++;
        }

        $divisor = ceil($divisor);
        $select_choices = array();
        $select_choices_values = array();
        $k = 0;
        $l = 0;
        $start = '';
        $selected_index = 0;
        foreach ($arr_choices as $key => $choice)
        {
          if ($k % $divisor == 0)
          {
            if ($k != 0)
            {
              preg_match("/(2[0-3]|[01][0-9]):([0-5][0-9])/", $last_choice, $matches);
              $select_choices[$l - 1] .= __('%time% Uhr', array('%time%' => $matches[0]), 'bahn');
            }
            preg_match("/(2[0-3]|[01][0-9]):([0-5][0-9])/", $choice, $matches);
            $select_choices[$l] = __('Abfahrten %time% Uhr bis ', array('%time%' => $matches[0]), 'bahn'); #'Abfahrten '.$matches[0].' Uhr bis ';
            $l++;
          }
          $select_choices_values[$l - 1][$key] = $choice;
          $k++;

          $last_choice = $choice;

          if ($k == count($arr_choices))
          {
            preg_match("/(2[0-3]|[01][0-9]):([0-5][0-9])/", $choice, $matches);
            $select_choices[$l - 1] .= __('%time% Uhr', array('%time%' => $matches[0]), 'bahn');
          }
          if ($key == $default_strecke)
          {
            $selected_index = $l - 1;
          }

        }

        $mandatory_field_addition = '';
        if(sfConfig::get('app_form_append_star_to_required_fields',true))
        {
          $mandatory_field_addition = '*';
        }
        $select_string = '<div class="pre_choice"><div class="field"><label class="product">' . __('Abfahrtszeitraum', null, 'bahn') . $mandatory_field_addition . '</label><select id="pre_choice" onChange="callChangePreSelect(this)">';
        foreach ($select_choices as $key => $value)
        {
          $selected = '';
          if ($key == $selected_index)
          {
            $selected = 'selected="selected"';
          }
          $select_string .= '<option value="' . $key . '" ' . $selected . '>' . $value . '</option>';
        }
        $select_string .= '</select></div></div>';
      }
      ?>
      <?php if($anz > $max):?>
      <script>
        $(document).ready(function(){
        var select_string = '<?php echo $select_string;?>';
        var element_id = "<?php echo $element;?>";
        var insert_element = $(element_id);
        var anz = "<?php echo $anz;?>";

        $(select_string).insertBefore(insert_element.parent().parent());
        var selected_index = <?php echo $selected_index;?>;
        changePreSelect(selected_index);
        });

        function callChangePreSelect(sValue)
        {
            changePreSelect(sValue.selectedIndex);
        }

        function changePreSelect(index)
        {
          var index= index;
          var element_id = '<?php echo $element;?>';
          var choices = <?php echo json_encode($select_choices_values);?>;
          var choices_index = choices[index];
          var selected_index = '<?php echo $default_strecke;?>';

          $(element_id).find('option').remove();

            for(var prop in choices_index) {
                if (choices_index.hasOwnProperty(prop))
                {
                    if(prop == selected_index)
                    {
                        $(element_id).append('<option value="'+prop+'" selected="selected">'+choices_index[prop]+'</option>');
                    }
                    else
                    {
                        $(element_id).append('<option value="'+prop+'">'+choices_index[prop]+'</option>');
                    }

                }
            }
        }
      </script>
      <?php endif;?>

<?php endif;?>
<?php endif;?>
<?php endif;?>

