<?php include_partial('standardProductSale/tarif_suggest',array('product_item'=> $product_item)); ?>

<?php if (isset($eos_cms_page) && !$hide_cms_page): ?>
  <div class="cms-content">
      <script type="text/javascript">

        function SetupTabs()
        {
            $('li.cms_tab_navigation_tab > a').each(function(){
                $(this).bind('click', function(){
                   $('div.cms_tab_navigation_content').hide();
                   $('div.cms_tab_navigation_content.content_' + $(this).attr('href').substring(5)).show();
                   $('li.cms_tab_navigation_tab').removeClass('active');
                   $(this).parent().addClass('active');
                });
            });
        }

        $(document).ready(SetupTabs);
      </script>
      <div class="cms-content-befor-tabs">
            <?php echo $eos_cms_page->getRawValue()->render(); ?>
      </div>
  </div>

  <?php include_partial('standardProductSale/text_after_cms'); ?>
  <?php if($product->getAttributeValue('dekt') == true):?>
        <br>
        <h2><?php echo $product_item->getName() #$product->getName() ?></h2>
  <?php endif;?>
  <?php else: ?>
  <h1><?php echo $product_item->getName() #$product->getName() ?></h1>
  <?php include_partial('standardProductSale/after_product_headline'); ?>
  <?php if ($product->getProductImageMedium()!==null): ?>
    <div class="product-image"><?php echo image_tag_cdn($product->getProductImageMedium()->getUrl(), array('alt' => $product->getName(),'title' => $product->getName())) ?></div>
  <?php endif; ?>
  <?php if ($product->getDescriptionShort() != ''): ?>
    <div class="description"><?php echo simple_format_text($product->getDescriptionShort()); ?></div>
  <?php endif; ?>
  
  <?php endif; ?>
  
  <?php if ($sf_user->hasFlash('error_msg_additional')): ?>
    <h4 class="error"><?php echo $sf_user->getFlash('error_msg_additional') ?></h4>
  <?php endif; ?>  
    
  <?php if (count($product_item->getRawValue()->getProductDetails())>0): ?>
    <?php include_partial('ticketinfo', array('product_item' => $product_item)); ?>
  <?php endif; ?>
  
  <?php if (!$hide_product_form) :?>

    <?php if ($single_product_mode): ?>
	    <?php if ($product->getShopProduct()!==null && $product->getShopProduct()->countProductsForApp($_app)>1 && sfConfig::get('app_product_show_distribution_methods', true)): ?>
	    <form method="post" action="<?php echo url_for('@product_distribution_dispatch'.($replaced_item!==null?'?replaced_item='.$replaced_item->getHash():'')) ?>#distribution_method">
        <div id="distribution_method" class="distribution_method">
          <h2><?php echo __('Wie möchten Sie Ihr Ticket erwerben?', array(), 'product_edit'); ?></h2>
          <?php foreach ($product->getShopproduct()->getProductsForApp($_app) as $related_product): ?>
            <?php if ($related_product->getDistributionMethodObject()===null) { continue; } ?>
            <?php if (!$related_product->isPurchasable()) { continue; } ?>
            <input onclick="this.form.submit();" type="radio"<?php echo ($related_product->getId() == $product->getId()?' checked="checked"':'' ); ?> name="product_id" id="product_<?php echo $related_product->getId(); ?>" value="<?php echo $related_product->getId(); ?>" />
            <label for="product_<?php echo $related_product->getId(); ?>"><span>
              <?php $dist_name = __($related_product->getDistributionMethodObject()->getName(), array(), 'distribution_methods'); ?>
              <?php echo image_tag_plugable_cdn($related_product->getDistributionMethodObject()->getId().'-big.gif',array('alt'=>$dist_name)) ?>
              <br /><?php echo $dist_name; ?>
             </span></label>
          <?php endforeach; ?>
        </div>
	    </form>
	    <?php endif; ?>    
    <?php endif; ?>
    <?php echo $sf_data->getRaw('form_tag'); ?>
      <?php include_partial('standardProductSale/hidden_fields_nested', array('form' => $form)); ?>
      <?php
      if ($passenger_id_card_form !== null)
      {
        include_partial('standardProductSale/hidden_fields_nested', array('form' => $passenger_id_card_form));
      }
      ?>
      <div class="options">
      <?php $no_hidden_fields = 0; ?>
      <?php  foreach ($form->getFormFieldSchema() as $field_key => $field): ?>
        <?php if (!$field->getWidget() instanceof sfWidgetFormInputHidden): ?>
          <?php $no_hidden_fields++; ?>
        <?php endif; ?>
      <?php endforeach;  ?>    
          
      <?php if(sfConfig::get('app_form_append_star_to_required_fields',true) == 1 && $no_hidden_fields > 0): ?>
        <p class="required-fields">* <?php echo __('Pflichtfelder',array(),'common');?></p> 
      <?php endif; ?>
  
      <?php // PASS 1: hidden fields ?>
      <?php /* foreach ($form->getFormFieldSchema() as $field_key => $field): ?>
        <?php if ($field->getWidget() instanceof sfWidgetFormInputHidden): ?>
          <?php echo $field->renderError().$field->render() ?>
        <?php endif; ?>
      <?php endforeach; */ ?>
      <?php if($submitted) include_partial('global/form_errors',array('forms'=>array($form,$passenger_id_card_form))); ?>

      <?php if (!sfConfig::get('app_product_option_presentation_use_legacy_mode', false)): ?>
        <?php include_partial('standardProductSale/text_before_passenger_id_card_form', array('product' => $product)); ?>
        <?php #echo $form->render(); ?>
        <?php
           
          $decorator = $form->getDecorator();
          if ($passenger_id_card_form !== null)
          {
            $decorator->setFormForBlockType(ProductPresentationLayoutBlockTypePassengerIdCard::ID, $passenger_id_card_form);
          }
          echo $decorator->renderForm($form); 
        ?>
      <?php else: ?>

      <?php
        $grouped_fields_required = $form->getProductPersonalizationGroupedFieldsRequired();
  
        $widget_order = false;
        $widget_orders = sfConfig::get('app_product_option_presentation_widget_order', false);
        
        if ($widget_orders !== false && array_key_exists($product->getDistributionMethodObject()->getId(), $widget_orders))
          $widget_order = $widget_orders[$product->getDistributionMethodObject()->getId()];
          
        if (!is_array($widget_order))
          $widget_order = array(
            'validation_date',
            'validation_end_date',
            'option_fields',
            'location',
            'via',
            'destination',
            ProductShopForm::PRODUCT_TARIF_ZONES,
            'personalization_grouped_fields_required',
            'personalization_ungrouped_fields_required',
            'person_number',
            'quantity',
            'payment_interval',
            'email_reminder',
            'captcha'
          );
        
        #      var_dump($widget_order);
          
        $product_validation_date_group_id = sfConfig::get('app_product_option_presentation_'.ProductShopForm::PRODUCT_VALIDATION_DATE, false);
  
        if ($product_validation_date_group_id !== false)
          $product_validation_date_group_id = $product_validation_date_group_id[$product->getDistributionMethodObject()->getId()];
  
        if ($product_validation_date_group_id !== false)
        {
          if (array_key_exists($product_validation_date_group_id, $grouped_fields_required))
            $grouped_fields_required[$product_validation_date_group_id] = array_merge(array(ProductShopForm::PRODUCT_VALIDATION_DATE), $grouped_fields_required[$product_validation_date_group_id]);
          else
            $grouped_fields_required[$product_validation_date_group_id] = array(ProductShopForm::PRODUCT_VALIDATION_DATE);
          $widget_order = array_flip($widget_order);
          unset($widget_order[ProductShopForm::PRODUCT_VALIDATION_DATE]);
          $widget_order = array_flip($widget_order);
        }
       
        #var_dump($widget_order);
        
        // TODO: Die Ausgabe direkt im foreach machen,
        //       da so jedes Feld an jeder Stelle stehen kann
        //       einziges Problem, nicht für jedes Widget eine
        //       neue Tabelle anzufangen.
        $fields_part1 = array();
        $fields_part2 = array();
        $fields = &$fields_part1;
        foreach ($widget_order as $widget_source)
        {
          switch ($widget_source)
          {
            case ProductShopForm::PRODUCT_VALIDATION_DATE:
            case ProductShopForm::PRODUCT_VALIDATION_END_DATE:
            case ProductShopForm::PRODUCT_LOCATION:
            case ProductShopForm::PRODUCT_VIA:
            case ProductShopForm::PRODUCT_DESTINATION:
            case ProductShopForm::PRODUCT_QUANTITY:
            case ProductShopForm::PRODUCT_TARIF_ZONES:
            case ProductShopForm::PRODUCT_PERSON_NUMBER:
            case ProductShopForm::PRODUCT_PAYMENT_INTERVAL:
            case ProductShopForm::PRODUCT_EMAIL_REMINDER:
            case ProductShopForm::PRODUCT_CAPTCHA:
              array_push($fields, $widget_source);
            break;
            case 'option_fields':
              $fields = array_merge($fields, $form->getSubProductForms());
            break;
            case 'personalization_ungrouped_fields_required':
              $fields = array_merge($fields, $form->getProductPersonalizationUngroupedFieldsRequired());
            break;
            # nach den grouped fields in fields_part2 schreiben
            case 'personalization_grouped_fields_required':
              if (count($grouped_fields_required) > 0)
                $fields = &$fields_part2;
            break;
            
          }
        }
      #$fields_part2 = $form->getProductPersonalizationFieldsOptional();
  
      #var_dump($fields_part1);
      ?>
      <?php $key_val_partial = get_partial('key_value', array('form'=>$form, 'show_errors' => $submitted, 'field_names' => $fields_part1)); ?>
      <?php if(trim($key_val_partial) != ''): ?>
      <div class="option-section">
      <?php echo $key_val_partial; ?>
      </div>
      <noscript><input type="submit" name="update" value="<?php echo __('Auswahl aktualisieren', array(), 'product_edit'); ?>"/></noscript>
      <?php endif; ?>
      <?php foreach (PersonalizationPropertyGroupPeer::getList() as $personalization_property_group): ?>
      
        <?php
          if (!array_key_exists($personalization_property_group->getId(), $grouped_fields_required))
            continue;
        ?>
        <?php #foreach ($form->getProductPersonalizationGroupedFieldsRequired() as $group_id => $field_names): ?>
        <?php #$personalization_property_group = PersonalizationPropertyGroupPeer::retrieveByPK($group_id); ?>
        <div class="option-section">
        <?php include_partial('key_value', array('form'=>$form, 'show_errors' => $submitted , 'field_names' => $grouped_fields_required[$personalization_property_group->getId()], 'title' => $personalization_property_group->getIsVisible() ? $personalization_property_group->getName() : false)); ?>
        </div>
      <?php endforeach; ?>
      
      
      <?php if (count($fields_part2)>0): ?>
      <?php include_partial('key_value', array('form'=>$form, 'show_errors' => $submitted , 'field_names' => $fields_part2)); ?>
      <?php endif; ?>
      
      <?php $personalization_fields_optional = $form->getProductPersonalizationFieldsOptional();?>
      <?php #var_dump($personalization_fields_optional); ?>
      <?php if (count($personalization_fields_optional)>0): ?>
      <div class="option-section">
      <?php $product_id = ($form->getObject()) ? $form->getObject()->getId() : null; ?>
        <?php if (
          sfConfig::get('app_product_show_personalization_usage_select', true) &&
          !in_array($product_id, sfConfig::get('app_product_personalization_usage_select_excluded_products', array())) &&
          !($form->getObject()->getDistributionMethod() === 'abo_light' && sfContext::getInstance()->getConfiguration()->getApplication() === 'major_customer')
        ): ?>
      <div class="personalization-usage-select">
          <?php $personalization = ($sf_request->getParameter("personalization-usage-$block_num") == 'personalization-used') ? true : false; ?>
        <div class="option">
          <input class="personalization-not-used" name="personalization-usage-<?php echo $block_num ?>" value="personalization-not-used" id="personalization-not-used-<?php echo $block_num ?>" type="radio" <?php if (!$personalization) echo 'checked="checked"'; ?>/>
          <label for="personalization-not-used-<?php echo $block_num ?>"><?php echo __('Ticket ist für mich', array(), 'product_edit'); ?> </label>
        </div>
        <div class="option">
          <input class="personalization-used" name="personalization-usage-<?php echo $block_num ?>" value="personalization-used" id="personalization-used-<?php echo $block_num ?>" type="radio"  />
          <label for="personalization-used-<?php echo $block_num ?>"><?php echo __('Ticket ist für jemand anderes', array(), 'product_edit'); ?> </label>
        </div>
      </div>
      <?php endif; ?>
      <div class="personalization-fields" id="personalization-fields-<?php echo $block_num ?>">
        <?php include_partial('key_value', array('form'=>$form, 'show_errors' => $submitted , 'field_names' => $personalization_fields_optional, 'title' => __('Personalisierung', array(), 'product_edit') )); ?>
      </div>
    </div>
    <?php endif; ?>

    
  	  
     
    <?php if ($passenger_id_card_form!==null): ?>
      <div class="passenger_id_card">
      <?php
      $passenger_id_card = $passenger_id_card_form->getObject();
      $headline = trim($passenger_id_card->getDescription());
      if ($headline == '')
      {
        $headline = $passenger_id_card->getName();
      }
      ?>
        <h2><?php echo $headline; ?>:</h2>
        <div id="id-card-mode-select">
          <div class="option">
            <input name="id_card_mode" id="id-card-mode-existing" type="radio" value="existing"<?php echo ($passenger_id_card_form->getMode()=='existing'?' checked="checked"':'')?> onclick="this.form.submit()"/>
            <label for="id-card-mode-existing"><?php echo $passenger_id_card->getOptionTextNo() != '' ? $passenger_id_card->getOptionTextNo() : __('Ich habe bereits einen Fahrgastausweis vom Typ %id_card_name%', array('%id_card_name%'=>$passenger_id_card->getName()), 'product_edit'); ?> </label>
          </div>
          <div class="option">
            <input name="id_card_mode" id="id-card-mode-new" type="radio" value="new"<?php echo ($passenger_id_card_form->getMode()=='new'?' checked="checked"':'')?> onclick="this.form.submit()"/>
            <label for="id-card-mode-new"><?php echo $passenger_id_card->getOptionTextYes() != '' ? $passenger_id_card->getOptionTextYes() : __('Ich benötige einen Fahrgastausweis vom Typ %id_card_name%', array('%id_card_name%'=>$passenger_id_card_form->getObject()->getName()), 'product_edit'); ?> </label>
          </div>
          <noscript><input type="submit" name="update" value="<?php echo __('Auswahl aktualisieren', array(), 'product_edit'); ?>"/></noscript>
        </div>
        
        <?php include_partial('global/form_header',array('form'=>$passenger_id_card_form, 'show_errors' => false)); ?>
        <?php $passenger_id_card_form_field_names = array()?>
        <?php foreach ($passenger_id_card_form->getFormFieldSchema() as $field_key => $field): ?>
          <?php $passenger_id_card_form_field_names[] = $field_key; ?>
        <?php endforeach; ?>
        <?php include_partial('key_value', array('form'=>$passenger_id_card_form, 'show_errors' => $submitted, 'field_names' => $passenger_id_card_form_field_names)); ?>
        
        <?php /*
        <table class="key-value">
        <?php foreach ($passenger_id_card_form->getFormFieldSchema() as $field_key => $field): ?>
          <tr>
          <?php if ($field->getWidget() instanceof sfWidgetFormInputHidden) { continue; } ?>
            <th><?php echo $field->renderLabel() ?>:</th><td><?php echo $field->renderError().$field->render() ?></td>
          </tr>
        <?php endforeach; ?>
        </table> */ ?>
      </div>
    <?php endif;?>
    <?php endif; /* neues formular_rendering */ ?>
        <p class="presentation-block-hint" style="margin-top: 10px;">
          <?php echo __('Ticket ist nur in Verbindung mit einem amtlichen Lichtbildausweis des Ticketinhabers gültig.', array(), 'product_edit'); ?>
        </p>
    </div>
  <?php else: ?>
    <?php if (isset($disable_add_to_cart_reason) && $disable_add_to_cart_reason != ''): ?>
    <p class="disabled-reason"><?php echo __(html_entity_decode($sf_data->getRaw('disable_add_to_cart_reason'))); ?></p>
    <?php endif;?>
  <?php endif; ?>
  
  <?php include_partial('standardProductSale/price',array('product_item'=> $product_item)); ?>
  <?php if (!$disable_add_to_cart) :?>    
  <?php endif; ?>
<?php /*
<?php if ($sf_user->getShoppingCart()->getNbItems() > 0): ?>

	<form method="post" action="<?php echo url_for('cart/index'); ?>">
		<input type="submit" name="proceedOrderAction" value="<?php echo __('Zum Warenkorb')?>">
	</form>

<?php endif; ?>
*/ ?>

<?php if($product->getAttributeValue('dekt') == true):?>
  <?php include_partial('standardProductSale/dekt', array('form'=> $form, 'product' => $product));?>
<?php endif;?>
