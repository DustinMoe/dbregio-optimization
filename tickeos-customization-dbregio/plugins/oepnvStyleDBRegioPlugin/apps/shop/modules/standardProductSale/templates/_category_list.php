<div class="category-row category-row-first">
<?php
  $i = 0;
  $n = count($categories);
?>
  <?php foreach ($categories->getRawValue() as $category_products): ?>
  <?php $i++; ?>
  <div class="product-category">
<?php
  $category = $category_products['category'];

  $category_image = $category->getImageObject();
  if ($category_image !== null)
  {
    echo '<img src="'.$category_image->getUrl().'"/>';
  }
  ?>
  
  <ul class="product-list">
  <?php foreach ($category_products['products'] as $product): ?>
    <div class="product">
      <?php if ($product->getProductImageSmall()!==null): ?>
      <?php echo image_tag_plugable_cdn($product->getProductImageSmall()->getUrl()); ?>
      <?php endif; ?>
      <h2><?php echo $product->getName(); ?></h2>
      <p><?php echo nl2br($product->getDescriptionShort()); ?></p>
      <p><?php echo link_to(__('zum Produkt', array(), 'product_list'), $product->getRoute());?></p>
    </div>  
  <?php endforeach; ?>
  </ul>
  </div>
<?php
  if ($i%2==0 && $i < $n):
    echo '</div>';
    if ($i < $n): ?>
<div class="category-row<?php echo (((int)($i / 2))%2!=0?' category-row-even':'') ?>">
    <?php endif;
  endif; ?>
  <?php endforeach; ?>
</div>