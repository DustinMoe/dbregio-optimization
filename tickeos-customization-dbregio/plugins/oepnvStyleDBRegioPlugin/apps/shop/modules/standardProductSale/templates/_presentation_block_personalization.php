<?php $content = trim($sf_data->getRaw('content')) ?>
<?php if ($content != ''): ?> 
<div class="presentation-block personalization">
<?php if ($headline!=''): ?>
  <h2><?php echo $headline; ?></h2>
<?php endif; ?>

<?php if ($hint_text!=''): ?>
  <p class="presentation-block-hint">
    <?php echo $hint_text; ?>
  </p>
<?php endif; ?>

  <?php if ($show_usage_select): ?>
  <div class="personalization-usage-select">

    <?php $personalization = ($sf_request->getParameter("personalization-usage-select-1") == 'personalization-used') ? true : false; ?>
      <?php if(sfContext::getInstance()->getRequest()->getParameter('personalization_required')) : ?>
            <div class="option">        
              <input class="personalization-not-used" name="personalization-usage-select-1" id="personalization-not-used" value="personalization-not-used" type="radio" <?php if (!$personalization) echo 'checked="checked"'; ?>/>
              <label for="personalization-not-used"><?php echo __('Ticket ist für mich', array(), 'product_edit'); ?> </label>
            </div>
            <div class="option">
              <input class="personalization-used" name="personalization-usage-select-1" id="personalization-used" type="radio" value="personalization-used" <?php if ($personalization) echo 'checked="checked"'; ?>/>
              <label for="personalization-used"><?php echo __('Ticket ist für jemand anderen', array(), 'product_edit'); ?> </label>
							<?php include_partial('standardProductSale/personalization_used_hint'); ?>
            </div>
      <?php else:  ?>
            <div class="option" <?php if($only_for_me): ?>style="display:none"<?php endif; ?>>
              <input class="personalization-not-used" name="personalization-usage-select-1" id="personalization-not-used" type="radio"  value="personalization-not-used" <?php if (!$personalization) echo 'checked="checked"'; ?>/>
              <label for="personalization-not-used"><?php echo __('Ticket ist für mich', array(), 'product_edit'); ?> </label>
            </div>
            <?php if($require_login && !sfContext::getInstance()->getUser()->isAuthenticated()): ?>
            <?php include_partial('standardProductSale/personalization_require_login_block'); ?>
            <?php endif; ?>
            <?php if(!$only_for_me): ?>
            <div class="option">
              <input class="personalization-used" name="personalization-usage-select-1" id="personalization-used" type="radio" value="personalization-used" <?php if ($personalization) echo 'checked="checked"'; ?>/>
              <label for="personalization-used"><?php echo __('Ticket ist für jemand anderen', array(), 'product_edit'); ?> </label>
							<?php include_partial('standardProductSale/personalization_used_hint'); ?>
            </div>
            <?php endif; ?>
      <?php endif; ?>
  </div>
  <?php endif; ?>  
  <div class="personalization-fields">
    <?php echo $content; ?>
  </div>
</div>
<?php if ($show_usage_select): ?>
<script type="text/javascript">
  $(document).ready(function ()
  {
    $('.personalization-usage-select').each (function () {
      var personalizationusageselect = $(this);
      var personalizationfields = personalizationusageselect.next('.personalization-fields');

      personalizationusageselect.show();
      var personalization_used = false;
      $('input[type=\'text\']', personalizationfields).each( function ()
      {
        if ($(this).attr('id') != 'product_product_personalization_property_11') {
          if ($(this).val() != '' || $('.personalization-used').attr('checked') == 'checked' || $('.personalization-used:checked').val() == 'personalization-used' ) {
            personalization_used = true;
          }
        }
      });
      if (!personalization_used)
      {
        personalizationfields.hide();
      }

      $('.personalization-not-used', personalizationusageselect).click(function () { personalizationfields.slideUp(); });
      $('.personalization-used', personalizationusageselect).click(function () { personalizationfields.slideDown(); });
      //$($('#personalization-used').form).submit(function ()
      $('#ticket_options').submit(function ()
      {
        if ($('.personalization-not-used:checked', personalizationusageselect).length == 1)
        {
          $('input[type=\'text\']', personalizationfields).each( function () {
            $(this).val('');

          });
        }
      });
    });
  });
</script>

<?php endif; ?>

<?php endif; ?>    