<?php if (isset($cms_page) && $cms_page !== null):?>
	<?php 
        $counter = 0;
        foreach($cms_page->getBlocks() as $block)
        {
            if($block->getType() === 'image')
            {
                if($counter % 2 !== 0)
                {
                    echo '<div id="product-box-' . $counter . '" class="product-box last">';
                }
                else
                {
                     echo '<div id="product-box-' . $counter . '" class="product-box">';
                }
                
                echo $block->getRawValue()->render();
                
                $counter = $counter + 1;
            }
            else if($block->getType() === 'text')
            {
                echo $block->getRawValue()->render();
                echo "</div>";
            }
            else
            {
                echo $block->getRawValue()->render();
            }
        }
        ?>
<?php endif; ?>