<?php
/*
 * Created on 25.08.2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
  use_helper('Number', 'Currency');
?>
<div class="cart">
  <h2><?php echo __('Warenkorb', array(), 'cart'); ?></h2>
  <?php if ($shoppingCart->isEmpty()): ?>
  <?php else: ?>
    <?php $item_count = $shoppingCart->getNbItems(true); ?>
    <p class="article-count"><?php echo $item_count; ?> <?php echo __('Artikel', array(), 'cart'); ?> </p>
    <p class="article-total">
      <?php echo format_currency_adv($shoppingCart->getTotal(), 'EUR'); ?>
      <span class="vat"><?php echo __('inkl. MwSt.', array(), 'cart'); ?></span>
    </p>
<?php /*
	  <ul>
    <?php foreach ($shoppingCart->getItems() as $cartProduct):?>
      <li>
      <?php echo $cartProduct->getQuantity(); ?> &times;
    	<?php echo $shoppingCart->getObject($cartProduct->getClass(), $cartProduct->getId())->getName(); ?>
    	<br />
    	<?php echo __('Preis')?>: <?php echo format_currency($cartProduct->getQuantity() * $cartProduct->getPrice(), 'EUR'); ?>
 		  </li>
    <?php endforeach; ?>
    </ul>
*/ ?>
    <div class="button-container">
     <?php echo link_to(__('Zum Warenkorb', array(), 'cart'), '@shopping_cart', array('class' => 'button')); ?>
    </div>
    <?php /* <p><?php echo link_to(__('Warenkorb leeren'),'shop/clearShoppingCart')?></p> */ ?>
  <?php endif;?>
</div>