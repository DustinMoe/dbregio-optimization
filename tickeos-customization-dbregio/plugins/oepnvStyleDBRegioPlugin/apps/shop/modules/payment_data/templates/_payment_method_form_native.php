<?php 
/**
 * displays the form content of _payment_method partial
 *   if the payment method provides a native form (common symfony processing logic)
 */
?>
  <form method="post" id="payment_<?php echo $payment_method_name; ?>" class="enable-hint">
    <input type="hidden" name="method" value="<?php echo $payment_method_name; ?>"/>
    <?php if ($payment_method_form !== null): ?>
        <?php include_partial('global/form_header', array('forms' => array($payment_method_form)));?>
    <?php endif; ?>
    <?php if ($payment_method_form !== null && count($payment_method_form->getFormFieldSchema()) > 1): ?>

        <?php if (!$open): ?>
            <?php if ($payment_method->isSaveable() && $payment_method->isSaved() && $payment_method->isDeletable()): ?>
                <div class="payment-method-delete-button">
                    <a class="button payment_delete_button" id="payment_delete_button_<?php echo $payment_method_name; ?>" href="<?php echo url_for('@payment_delete?id='.$payment_method->getId()); ?>"><?php echo __('Löschen', null, 'payment'); ?></a>
                </div>
                <div class="dedicated-clear"></div>
            <?php endif; ?>
            <div class="payment-method-open-button">
                <a href="" class="button payment_open_button" id="payment_method_open_<?php echo $payment_method_name; ?>"><?php echo __('Auswählen', null, 'payment'); ?></a>
            </div>
        <?php endif; ?>
        <div class="payment-method-block-parameter payment-method-block-parameter-<?php echo ($open?'open':'closed') ?>" id="payment_method_block_parameter_<?php echo $payment_method_name; ?>">
    <?php if($payment_method->getDescriptionLong()): ?>
    <p><?php echo str_replace(array('\n', "\n"), '',  $sf_data->getRaw('payment_method')->getDescriptionLong()) ?></p>
    <?php endif; ?>
            <div class="content-section payment-data">
                <table class="form-data data-display">
                    <?php $c = 0; foreach ($payment_method_form->getFormFieldSchema() as $key => $field): ?>
                        <?php if (!($field->getWidget() instanceof sfWidgetFormInputHidden)): ?>
                            <?php $c++; ?>
                            <?php include_partial('global/render_basic_table_row', array('form'=>$payment_method_form, 'name'=>$key, 'ff'=>$field)); ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </table>
            </div>
    <?php foreach($payment_method->getInfoItems() as $item): ?>
    <p><?php echo $item->getHtmlText() ? $item->getHtmlText() : $item->getText() ?></p>
    <?php endforeach; ?>
            <div class="payment-method-select-button">
                <?php if ($payment_method->isSaveable() && $payment_method->isSaved() && $payment_method->isDeletable()): ?>
                    <a class="button payment_delete_button" id="payment_delete_button_<?php echo $payment_method_name; ?>" href="<?php echo url_for('@payment_delete?id='.$payment_method->getId()); ?>"><?php echo __('Löschen', null, 'payment'); ?></a>
                <?php endif; ?>
                <input class="button payment_select_button" id="payment_select_button_<?php echo $type; ?>_<?php echo $payment_method_name; ?>" type="submit" name="go" value="<?php echo __($payment_method->isSaveable()?($payment_method->isSaved()?'Auswählen':'Hinzufügen'):'Auswählen', null, 'payment'); ?>"/>
            </div>
        </div>
    <?php else: ?>

        <?php if ($payment_method->isSaveable() && $payment_method->isSaved() && $payment_method->isDeletable()): ?>
            <div class="payment-method-delete-button">
                <a class="button payment_delete_button" id="payment_delete_button_<?php echo $payment_method_name; ?>" href="<?php echo url_for('@payment_delete?id='.$payment_method->getId()); ?>"><?php echo __('Löschen', null, 'payment'); ?></a>
            </div>
            <div class="dedicated-clear"></div>
        <?php endif; ?>
        <div class="payment-method-select-button">
            <input class="button payment_select_button" id="payment_select_button_<?php echo $type; ?>_<?php echo $payment_method_name; ?>" type="submit" name="go" value="<?php echo __('Auswählen', null, 'payment'); ?>"/>
        </div>
    <?php endif; ?>
</form>