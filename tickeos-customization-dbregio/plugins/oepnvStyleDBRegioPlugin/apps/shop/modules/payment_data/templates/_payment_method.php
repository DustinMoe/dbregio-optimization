<?php if ($payment_method->isSaveable() && $payment_method->isSaved()): ?>
<li><div class="payment-method-block-saved <?php echo $row_class; ?>" id="payment_method_block_<?php echo $payment_method_name; ?>">
        <?php else: ?>
        <div class="payment-method-block <?php echo $row_class; ?>" id="payment_method_block_<?php echo $payment_method_name; ?>">
            <?php endif; ?>
            
                <div class="payment-method-description">
                    <?php if ($payment_method->isSaveable() && $payment_method->isSaved()): ?>
                      <?php if($payment_method->getLogoURL() != ''): ?>
                        <img src="<?php echo $payment_method->getLogoURL(); ?>" alt="<?php echo $payment_method->getName(); ?>" class="small-img" />
                      <?php endif; ?>
                      <h3><?php echo $payment_method->getName(); ?></h3>
                    <?php else: ?>
                        <?php $logo_url = $payment_method->getLogoUrl(); ?>
                        <?php if ($logo_url != ''): ?>
                            <?php if ($payment_method->getNameDetail() != ''): ?>
                                <h2><?php echo isset($h2_text) ? $h2_text : $payment_method->getNameDetail(); ?></h2>
                            <?php else: ?>
                                <h2><?php echo isset($h2_text) ? $h2_text : $payment_method->getName(); ?></h2>
                            <?php endif; ?>
                            <img class="payment-method-logo" src="<?php echo $logo_url; ?>"/>
                        <?php else: ?>
                            <h2><?php echo isset($h2_text) ? $h2_text : $payment_method->getName(); ?></h2>
                        <?php endif; ?>
                        <div class="dedicated-clear"></div>
                    <?php endif; ?>
                    <?php if (!$payment_method->isSaved()): ?>
                        <p><?php echo $payment_method->getDescription(); ?></p>
                    <?php endif; ?>
                  <?php if (strpos($payment_method->getId(), 'sepa') !== false): ?>
                        <p><?php echo __('Bitte beachten Sie: Zahlungen per SEPA-Lastschriftmandat sind nur bei volljährigen Kunden möglich.', array(), 'bahn') ?></p>
                    <?php endif; ?>
                </div>

<?php if($payment_method->getPaymentParameterFormType() == PaymentMethod::PAYMENT_PARAMETER_FORM_TYPE_EXTERNAL): ?>
<?php include_partial('payment_method_form_external', array(
  'payment_method_form_html' => $sf_data->getRaw('payment_method_form_html'),
  'payment_method_name' => $payment_method_name,
  'payment_method' => $payment_method,
  'open' => $open
)); ?>
<?php else: ?>
<?php include_partial('payment_method_form_native', array(
  'payment_method_form' => $payment_method_form,
  'payment_method_name' => $payment_method_name,
  'payment_method' => $sf_data->getRaw('payment_method'),
  'open' => $open,
  'type' => $type
)); ?>
<?php endif; ?>
          
          <div class="dedicated-clear"></div>
        </div>
        <?php if ($payment_method->isSaveable() && $payment_method->isSaved()): ?>
</li>
<?php endif; ?>