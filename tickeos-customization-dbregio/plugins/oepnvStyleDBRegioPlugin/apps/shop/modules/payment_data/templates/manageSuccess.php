<script lang="javascript">
  function SetupPayment()
  {
    $('div.payment-method-block-parameter-closed').hide();
    $('div.payment-method-open-button a.payment_open_button').click(function(){
      var id = $(this).attr('id').substring(20);

      $('div.payment-method-block-parameter').hide();
      $('div.payment-method-open-button').children('.button').show();
      $('div.payment-method-block-parameter-open').show();
      
      $('div#payment_method_block_parameter_'+id).show();
      $('a#payment_delete_button_'+id).hide();
      $(this).hide();
      return false;
    });
  }
  
  $(document).ready(SetupPayment);
</script>

<div class="actual-content">
<div class="payment_data">
<?php include_partial('text_start'); ?>
<?php
  $saved_elvs = array();
  $saved_nonelvs = array();
  $unsaved_payments = array();
  foreach ($payment_methods as $payment_method_name => $payment_method)
  {
    if ($payment_method->isSaved())
    {
      if ($payment_method->getPaymentMethod()->isELV())
      {
        $saved_elvs[$payment_method_name] = $payment_method;
      }
      else
      {
        $saved_nonelvs[$payment_method_name] = $payment_method;
      }
    }
    else
    {
      $unsaved_payments[$payment_method_name] = $payment_method;
    }
  }
?>
  
<h1><?php echo __('Gespeicherte Zahlungsmittel', array(), 'payment') ?></h1>
  
<?php if(count($saved_elvs) + count($saved_nonelvs) > 0) : ?>

<div class="payment-method">
  <?php if(count($saved_elvs) > 0) : ?>
    <div class="white-box">
    <h2><?php echo __('Lastschriftkonten', array(), 'payment') ?></h2>

    <ul class="link-list">
        <?php 
        $counter = 0;
        foreach ($saved_elvs as $payment_method_name => $payment_method):
        $counter++;
        $row_class = ($counter%2 == 0) ? 'odd' : 'even';
        ?>
          <?php include_partial('payment_method_manage', array(
            'payment_method_form' => $payment_method_forms[$payment_method_name],
            'payment_method_form_html' => $payment_method_forms_html[$payment_method_name],
            'payment_method_name' => $payment_method_name,
            'payment_method' => $payment_method,
            'open' => $payment_method_name == $selected_payment_method_name,
            'row_class' => $row_class,
            'type' => 'saved_elv'
          )); ?>
        <?php endforeach; ?>
    </ul>
  </div>
  <?php endif; ?>
  <?php if(count($saved_nonelvs) > 0) : ?>
    <div class="white-box">
    <h2><?php echo __('Andere Zahlungsmittel', array(), 'payment') ?></h2>

    <ul class="link-list">
        <?php 
        $counter = 0;
        foreach ($saved_nonelvs as $payment_method_name => $payment_method): 
        $counter++;
        $row_class = ($counter%2 == 0) ? 'odd' : 'even';
        ?>
          <?php include_partial('payment_method_manage', array(
            'payment_method_form' => $payment_method_forms[$payment_method_name],
            'payment_method_form_html' => $payment_method_forms_html[$payment_method_name],
            'payment_method_name' => $payment_method_name,
            'payment_method' => $payment_method,
            'open' => $payment_method_name == $selected_payment_method_name,
            'row_class' => $row_class,
            'type' => 'saved_nonelv'
          )); ?>
        <?php endforeach; ?>
    </ul>
  </div>
  <?php endif; ?>
</div>

<?php else: ?>
  <p><?php echo __('keine gespeicherten Zahlungsmittel vorhanden', null, 'payment') ?></p>
<?php endif; ?>


<h1><?php echo __('Zahlungsmittel speichern', array(), 'payment') ?></h1>

<?php if (count($unsaved_payments) > 0): ?>
  
  <div class="payments">
  <?php
  $counter = 0;
  foreach ($unsaved_payments as $payment_method_name => $payment_method): 
  ?>
  <?php 
  $counter++;
  $row_class = ($counter%2 == 0) ? 'odd' : 'even';
  ?>

  <?php include_partial('payment_method', array(
    'payment_method_form' => $payment_method_forms[$payment_method_name],
    'payment_method_form_html' => $payment_method_forms_html[$payment_method_name],
    'payment_method_name' => $payment_method_name,
    'payment_method' => $payment_method,
    'open' => $payment_method_name == $selected_payment_method_name,
    'row_class' => $row_class,
    'type' => 'payment',
    'h2_text' => $payment_method->getName()
  )); ?>

  <?php endforeach; ?>
  </div>
  
<?php else: ?>
  <p><?php echo __('keine speicherbaren Zahlungsmittel vorhanden', null, 'payment') ?></p>
<?php endif; ?>

<div class="dedicated-clear"></div>
</div>

<div class="dedicated-clear"></div>
</div>
<?php include_partial('global/mandatory_field_hint', array()); ?>