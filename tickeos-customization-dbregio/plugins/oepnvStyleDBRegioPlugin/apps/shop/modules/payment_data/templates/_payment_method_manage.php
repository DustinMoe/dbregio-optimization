<li>
  <div class="payment-method-block-saved <?php echo $row_class; ?>" id="payment_method_block_<?php echo $payment_method_name; ?>">
    <div class="payment-method-description">
      <?php if($payment_method->getLogoURL() != ''): ?>
        <img src="<?php echo $payment_method->getLogoURL(); ?>" alt="<?php echo $payment_method->getName(); ?>" class="small-img" />
      <?php endif; ?>
      <h3><?php echo $payment_method->getName(); ?></h3>
      <?php if ($payment_method->isDefault()): ?>
        <p><?php echo __('Standard-Zahlungsmittel' ,null, 'payment') ?></p>
      <?php endif; ?>
      <?php foreach($payment_method->getInfoItems() as $item): ?>
      <p><?php echo $item->getHtmlText() ? $item->getHtmlText() : $item->getText() ?></p>
      <?php endforeach; ?>
    </div>
    <div class="payment-method-delete-button">
      <?php if ($payment_method->isSaveable() && $payment_method->isSaved() && $payment_method->isDefaultable() && !$payment_method->isDefault()): ?>
        <a class="button payment_set_default_button" id="payment_set_default_button_<?php echo $payment_method_name; ?>" href="<?php echo url_for('@payment_set_as_default?id='.$payment_method->getId()); ?>"><?php echo __('Als Standard festlegen', null, 'payment'); ?></a>
      <?php endif; ?>
      <?php if ($payment_method->isSaveable() && $payment_method->isSaved() && $payment_method->isDeletable()): ?>
        <a class="button payment_delete_button" id="payment_delete_button_<?php echo $payment_method_name; ?>" href="<?php echo url_for('@payment_delete?id='.$payment_method->getId()); ?>"><?php echo __('Löschen', null, 'payment'); ?></a>
      <?php endif; ?>
    </div>
    <div class="dedicated-clear"></div>
  </div>
</li>
