<?php if(sfConfig::get('app_form_append_star_to_required_fields',null) == 1): ?>
    <p class="required-fields">* <?php echo __('Pflichtfelder',array(),'common');?></p>
<?php endif; ?>
<?php foreach ($form->getFormFieldSchema() as $name => $field): ?>
    <?php if ($field->getWidget() instanceof sfWidgetFormInputHidden): ?>
        <?php echo $field->render() ?>
    <?php endif; ?>
    <?php if ($field->hasError()): ?>
        <script>
            $(document).ready(function() {
                window.location.hash = '#consent_error';
            });
        </script>
        <p class="error" id="consent_error"><?php echo $form->getErrorText($name); ?></p>
    <?php endif; ?>
<?php endforeach; ?>

<div class="term-field-wrapper">
    <?php foreach ($form->getFormFieldSchema() as $name => $field): ?>
        <?php $description = $form->getWidgetSchema()->getHelp($name); ?>
        <?php echo $description != '' ? '<p>'.$description.'</p>':''; ?>
        <?php if(!($field->getWidget() instanceof sfWidgetFormInputHidden)):?>
            <div class="term-field<?php echo ($field->hasError()?' error':''); ?>">
                <?php if ($field->getWidget() instanceof sfWidgetFormInputCheckbox): ?>
                    <?php echo $field->render() ?> <?php echo $field->renderLabel() ?>
                <?php else: ?>
                    <?php echo $field->renderLabel() ?>: <?php echo $field->render() ?>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
</div>

<?php
if(isset($show_text) && $show_text){
    include_partial('generic/customer_consent_text');
}
?>
