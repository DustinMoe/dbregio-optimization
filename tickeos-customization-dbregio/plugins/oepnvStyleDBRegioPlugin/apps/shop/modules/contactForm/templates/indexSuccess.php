<div class="actual-content">
  <div class="shop-contact-form">
      <?php if (isset($cms_page) && $cms_page !== null): ?>
        <?php echo $cms_page->getRawValue()->renderTillBlockType('placeholder'); ?>
      <?php endif; ?>
  <h1><?php echo __('Kontaktformular', array(), 'contact_form'); ?></h1>
    <div class="shop-contact-form-content">
    <form action="<?php echo url_for('contactForm/send') ?>" method="post">
    <?php include_partial('global/form_header',array('form'=>$form)); ?>
    <div class="presentation-block presentation-block-shop-contact-form-1">
        <?php include_partial('global/render_as_presentation_block_field', array('form' => $form, 'ffnames' => array('first_name'))); ?>
        <?php include_partial('global/render_as_presentation_block_field', array('form' => $form, 'ffnames' => array('last_name'))); ?>
        <?php include_partial('global/render_as_presentation_block_field', array('form' => $form, 'ffnames' => array('street'))); ?>
        <?php include_partial('global/render_as_presentation_block_field', array('form' => $form, 'ffnames' => array('street_number'))); ?>
        <?php include_partial('global/render_as_presentation_block_field', array('form' => $form, 'ffnames' => array('street_number_addition'))); ?>
    </div>
    <div class="presentation-block presentation-block-shop-contact-form-2">
        <?php include_partial('global/render_as_presentation_block_field', array('form' => $form, 'ffnames' => array('postal_code'))); ?>
        <?php include_partial('global/render_as_presentation_block_field', array('form' => $form, 'ffnames' => array('city'))); ?>
        <?php include_partial('global/render_as_presentation_block_field', array('form' => $form, 'ffnames' => array('phone'))); ?>
        <?php include_partial('global/render_as_presentation_block_field', array('form' => $form, 'ffnames' => array('email'))); ?>
    </div>
    <div class="presentation-block presentation-block-shop-contact-form-3">
        <?php include_partial('global/render_as_presentation_block_field', array('form' => $form, 'ffnames' => array('subject'))); ?>
        <?php include_partial('global/render_as_presentation_block_field', array('form' => $form, 'ffnames' => array('message'))); ?>
    </div>
    <div class="presentation-block presentation-block-shop-contact-form-4">
        <?php include_partial('global/render_as_presentation_block_field', array('form' => $form, 'ffnames' => array('captcha'))); ?>
    </div>
    <div class="actions">
      <div class="right">
        <?php include_partial('global/button_helper', array('content' => '<input type="submit" class="button next" id="continue_button_next" name="continue" value="'. __('Absenden', array(), 'step_actions') .'" />')); ?>
      </div>
      <div class="dedicated-clear"></div>
    </div>
    </form>
    </div>
  </div>
</div>
<?php include_partial('global/mandatory_field_hint', array()); ?>