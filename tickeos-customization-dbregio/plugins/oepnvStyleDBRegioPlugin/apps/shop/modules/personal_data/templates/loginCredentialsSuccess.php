<?php
/*
 * Created on 11.09.2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
use_helper('eosCustomerProperty');
?>
<?php include_partial('oepnvShopAddressVerification/address_autocomplete'); ?>
<div class="actual-content">
<form method="post" class="enable-hint">
<?php include_partial('text_after_headline', array('register' => isset($register) && $register)); ?>

<?php if(!(isset($register) && $register) || isset($customer_property_form['password_reset_auth_question'])) : ?>
<h1><?php echo __('Zugangsdaten ändern', array(), 'personal_data'); ?></h1>
<?php endif; ?>
  <?php if (true)://(isset($form['password_reset_auth_question']) || (isset($form['password']) && (!(isset($register) && $register) || sfConfig::get('app_registration_ask_password', false))) || isset($form['username'])): ?>
  <div class="content-section">
  <?php if ($sf_user->hasFlash('success') && $sf_user->getFlash('success')): ?>
     <p class="success"><?php echo __('Ihre Zugangsdaten wurden erfolgreich gespeichert.', array(), 'personal_data'); ?></p>  
  <?php endif;?>
    <div class="presentation-block personal-data-3">
        <div id="presentation-block-personal-data-3-password">
        <?php include_partial('global/form_header', array('forms' => array($form), 'show_errors' => $show_errors)); ?>
        <h2><?php echo __('Passwort ändern', array(), 'personal_data'); ?></h2>
        <p><?php echo __('Bitte geben Sie Ihr neues Passwort ein.', array(), 'personal_data'); ?></p>
    <?php if(isset($form['username'])):?>
      <?php include_partial('global/render_as_presentation_block_field', array('form' => $form, 'ffnames' => array('username'), 'show_errors'=>$show_errors)); ?>
  <?php endif;?>
    <?php if (isset($form['password']) && (!(isset($register) && $register) || sfConfig::get('app_registration_ask_password', true))): ?>
      <?php if(sfConfig::get('app_password_show_policy', false)):
      include_partial('password_policy');
      endif;
      ?>
      <?php //echo __('Wenn Sie das Passwort nicht ändern möchten, lassen Sie die folgenden beiden Felder bitte leer.',array(),'personal_data'); ?>
      <?php include_partial('global/render_as_presentation_block_field', array('form' => $form, 'ffnames' => array('old_password'), 'show_errors'=>$show_errors)); ?>
      <?php include_partial('global/render_as_presentation_block_field', array('form' => $form, 'ffnames' => array('password'), 'show_errors'=>$show_errors)); ?>
      <?php include_partial('global/render_as_presentation_block_field', array('form' => $form, 'ffnames' => array('password2'), 'show_errors'=>$show_errors)); ?>
    <?php endif; ?>
  </div>
      <?php if (!(isset($register) && $register)):?>
        <?php include_partial('loginCredentialsActions', array('password' => true)); ?>
      <?php endif; ?>

      <div id="presentation-block-personal-data-3-auth-question">
        <?php include_partial('global/form_header', array('forms' => array($customer_property_form), 'show_errors' => $show_errors)); ?>
    <?php if (isset($customer_property_form['password_reset_auth_question'])): ?>
      <h2><?php echo __('Sicherheitsfrage ändern', array(), 'personal_data'); ?></h2>
      <p><?php echo __('Anpassungen Ihrer Sicherheitsfrage können hier vorgenommen werden.', array(), 'personal_data'); ?></p>
      <?php include_partial('global/render_as_presentation_block_field', array('form' => $customer_property_form, 'ffnames' => array('password_reset_auth_old_password'), 'show_errors'=>$show_errors)); ?>
      <?php include_partial('global/render_as_presentation_block_field', array('form' => $customer_property_form, 'ffnames' => array('password_reset_auth_question'), 'show_errors'=>$show_errors)); ?>
    <?php endif; ?>
    <?php if (isset($customer_property_form['password_reset_auth_answer'])): ?>
      <?php include_partial('global/render_as_presentation_block_field', array('form' => $customer_property_form, 'ffnames' => array('password_reset_auth_answer'), 'show_errors'=>$show_errors)); ?>
    <?php endif; ?>
      </div>
    </div>
  
  <?php include_partial('text_under_paymentmethods', array('register' => isset($register) && $register)); ?>
    </div>  
  <?php endif; ?>
  <?php if (isset($register) && $register):?>
    <?php include_partial('registerActions'); ?>
  <?php else:?>
    <?php include_partial('loginCredentialsActions', array('password' => false, 'password_reset_auth_question' => isset($customer_property_form['password_reset_auth_question']))); ?>
  <?php endif; ?>
  <?php include_partial('text_under_actions', array('register' => isset($register) && $register)); ?>
</form>

</div>
