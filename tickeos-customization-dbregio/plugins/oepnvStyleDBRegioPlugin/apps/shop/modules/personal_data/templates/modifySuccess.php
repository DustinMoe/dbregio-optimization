<?php
  use_helper('eosCustomerProperty');
  include_partial('oepnvShopAddressVerification/address_autocomplete');
?>
<div class="actual-content">
<form method="post" class="enable-hint">
<h1><?php echo __('Persönliche Daten', array(), 'personal_data'); ?></h1>
<?php include_partial('text_after_headline', array('register' => isset($register) && $register)); ?>
<div class="content-section">
<?php
  $customerForm = $customer_form[oepnvCustomerDBDataForm::SUBFORM_CUSTOMER];
  $customer_property_form = isset($customer_property_form)? $customer_property_form : $customer_form[oepnvCustomerDBDataForm::SUBFORM_PROPERTIES];
  $validator_schema_customer = $customer_form->getSubForm(oepnvCustomerDBDataForm::SUBFORM_CUSTOMER)->getValidatorSchema();
  $validator_schema_customer_properties = $customer_form->getSubForm(oepnvCustomerDBDataForm::SUBFORM_PROPERTIES)->getValidatorSchema();

  include_partial('text_above_data', array('register' => isset($register) && $register));

  include_partial('global/form_header', array('forms' => array($customer_form), 'show_errors' => $show_errors));
  $disable_input = !(isset($register) && $register);
?>
  <div class="presentation-block personal-data-1">
    <?php
      include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('salutation'), 'show_errors'=>$show_errors));
      include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('title'), 'show_errors'=>$show_errors));
      include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('first_name'), 'show_errors'=>$show_errors));
      include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('last_name'), 'show_errors'=>$show_errors));
      include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('birthday'), 'show_errors'=>$show_errors));
      include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('company'), 'show_errors'=>$show_errors));
      if(isset($customerForm['department']))
      {
        include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('department'), 'show_errors'=>$show_errors));
      }
      if(isset($customerForm['address_addition']))
      {
        include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('address_addition'), 'show_errors'=>$show_errors));
      }
    ?>
  </div>
  <div class="presentation-block personal-data-2">
    <?php
      if(sfConfig::get('app_address_verification_enabled', false)):
        if(!sfConfig::get('app_customer_personal_data_compact', false)):
          include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('postal_code'), 'show_errors'=>$show_errors));
          include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('city'), 'show_errors'=>$show_errors));
        else:
          include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('postal_code','city'), 'show_errors'=>$show_errors));
        endif;
        if(!sfConfig::get('app_customer_personal_data_compact', false)):
          include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('street'), 'show_errors'=>$show_errors));
          include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('street_number'), 'show_errors'=>$show_errors));
          if(isset($customerForm['street_number_addition'])):
            include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('street_number_addition'), 'show_errors'=>$show_errors));
          endif;
        else:
          include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('street','street_number','street_number_addition'), 'show_errors'=>$show_errors));
        endif;
      else:
        if(!sfConfig::get('app_customer_personal_data_compact', false)):
          include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('street'), 'show_errors'=>$show_errors));
          include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('street_number'), 'show_errors'=>$show_errors));
          if(isset($customerForm['street_number_addition'])):
            include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('street_number_addition'), 'show_errors'=>$show_errors));
          endif;
        else:
          include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('street','street_number','street_number_addition'), 'show_errors'=>$show_errors));
        endif;
        if(!sfConfig::get('app_customer_personal_data_compact', false)):
          include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('postal_code'), 'show_errors'=>$show_errors));
          include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('city'), 'show_errors'=>$show_errors));
        else:
          include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('postal_code','city'), 'show_errors'=>$show_errors));
        endif;
      endif;
      include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('country'), 'show_errors'=>$show_errors));
      include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('phone'), 'show_errors'=>$show_errors));
      include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('email'), 'show_errors'=>$show_errors));
      if (isset($register) && $register):
        if (isset($email2)):
          include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('email2'), 'attributes' => array('value' => $email2), 'show_errors'=>$show_errors));
        else:
          include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('email2'), 'show_errors'=>$show_errors));
        endif;
      endif;
      include_partial('text_after_email_block_field', array('register' => isset($register) && $register));
      if (!sfConfig::get('app_customer_disable_identity_card', false) && eosLicenseManager::getInstance()->isModuleLicensed('ticket') && !($customerForm['identity_card_type_id']->getWidget() instanceof sfWidgetFormInputHidden)) :
        include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'ffnames' => array('identity_card_type_id'), 'show_errors'=>$show_errors));
      endif;
    ?>
  </div>

  <div class="dedicated-clear"></div>

  <?php
    foreach($partials_to_render as $partial_to_render)
    {
    	if(in_array($partial_to_render, $required_customer_partials->getRawValue()))
      {
        include_partial($partial_to_render, array('form' => $customer_property_form, 'mode' => 'modify'));
      }
    }

  if (count($customer_property_form) > 0):
    $table_started = false;
    $special_property_list = array_merge(array('password_reset_auth_question', 'password_reset_auth_answer'), $partial_special_field_list->getRawValue());
    $row = 0;
    foreach ($customer_property_form as $name => $form_field):
      if (!($form_field->getWidget() instanceof sfWidgetFormInputHidden) &&
          !in_array($name, $special_property_list) &&
          !isInPrefixList($name, $prefix_list)
         ):
        if (!$table_started): $table_started = true;
          echo '<div class="presentation-block personal-data-other">';
        endif;
        include_partial('global/render_as_presentation_block_field', array('form' => $customer_property_form, 'validator_schema' => $validator_schema_customer_properties, 'ffnames' => array($name), 'show_errors'=>$show_errors));
      endif;
      $row++;
    endforeach;
    if ($table_started):
      echo '</div>';
    endif;
  endif;
  if (oepnvCustomerFactory::getInstance()->getOepnvCustomerAuthentification()->hasCapability(oepnvCustomerAuthentification::CAPABILITY_DELIVERY_ADDRESS) && isset($customerForm['use_delivery_address'])): ?>
    <script>
      $(document).ready(function(){
        var checkbox = $('#customer_data_customer_use_delivery_address');
        if (!checkbox.attr('checked'))
        {
          $('div.delivery-address').hide();
        }
        $('#customer_data_customer_use_delivery_address').click(function(){
          var checkbox = $(this);
          if (!checkbox.attr('checked'))
            $('div.delivery-address').slideUp();
          else
            $('div.delivery-address').slideDown();
        });
      });
    </script>
  <p class="use-delivery-address-select">
    <?php
      echo $customerForm['use_delivery_address']->render(array('tabindex'=>600));
      echo $customerForm['use_delivery_address']->renderLabel();
    ?>
  </p>
  <div class="delivery-address">
    <h2><?php echo __('Lieferadresse', array(), 'customer'); ?></h2>

    <div class="presentation-block delivery-1">
      <?php
        include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'show_optional_info' => false, 'ffnames' => array('delivery_salutation'), 'show_errors'=>$show_errors));
        include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'show_optional_info' => false, 'ffnames' => array('delivery_title'), 'show_errors'=>$show_errors));
        include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'show_optional_info' => false, 'ffnames' => array('delivery_first_name'), 'show_errors'=>$show_errors));
        include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'show_optional_info' => false, 'ffnames' => array('delivery_last_name'), 'show_errors'=>$show_errors));
        include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'show_optional_info' => false, 'ffnames' => array('delivery_company'), 'show_errors'=>$show_errors));
        include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'show_optional_info' => false, 'ffnames' => array('delivery_address_addition'), 'show_errors'=>$show_errors));
      ?>
    </div>
        <div class="presentation-block delivery-2">
            <?php if(sfConfig::get('app_address_verification_enabled',false)):?>
                <?php if(!sfConfig::get('app_customer_personal_data_compact', false)):?>
                    <?php include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'show_optional_info' => false, 'ffnames' => array('delivery_postal_code'), 'show_errors'=>$show_errors)); ?>
                    <?php include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'show_optional_info' => false, 'ffnames' => array('delivery_city'), 'show_errors'=>$show_errors)); ?>
                <?php else:?>
                    <?php include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'show_optional_info' => false, 'ffnames' => array('delivery_postal_code','delivery_city'), 'show_errors'=>$show_errors)); ?>
                <?php endif;?>
                <?php if(!sfConfig::get('app_customer_personal_data_compact', false)):?>
                    <?php include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'show_optional_info' => false, 'ffnames' => array('delivery_street','delivery_street_number'), 'show_errors'=>$show_errors)); ?>
                    <?php include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'show_optional_info' => false, 'ffnames' => array('delivery_street_number_addition'), 'show_errors'=>$show_errors)); ?>
                <?php else:?>
                    <?php include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'show_optional_info' => false, 'ffnames' => array('delivery_street','delivery_street_number','delivery_street_number_addition'), 'show_errors'=>$show_errors)); ?>
                <?php endif;?>                
            <?php else:?>
                <?php if(!sfConfig::get('app_customer_personal_data_compact', false)):?>
                    <?php include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'show_optional_info' => false, 'ffnames' => array('delivery_street','delivery_street_number'), 'show_errors'=>$show_errors)); ?>
                    <?php include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'show_optional_info' => false, 'ffnames' => array('delivery_street_number_addition'), 'show_errors'=>$show_errors)); ?>
                <?php else:?>
                    <?php include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'show_optional_info' => false, 'ffnames' => array('delivery_street','delivery_street_number','delivery_street_number_addition'), 'show_errors'=>$show_errors)); ?>
                <?php endif;?>
                <?php if(!sfConfig::get('app_customer_personal_data_compact', false)):?>
                    <?php include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'show_optional_info' => false, 'ffnames' => array('delivery_postal_code'), 'show_errors'=>$show_errors)); ?>
                    <?php include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'show_optional_info' => false, 'ffnames' => array('delivery_city'), 'show_errors'=>$show_errors)); ?>
                <?php else:?>
                    <?php include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'show_optional_info' => false, 'ffnames' => array('delivery_postal_code','delivery_city'), 'show_errors'=>$show_errors)); ?>
                <?php endif;?>                
            <?php endif;?>
            <?php include_partial('global/render_as_presentation_block_field', array('form' => $customerForm, 'validator_schema' => $validator_schema_customer, 'show_optional_info' => false, 'ffnames' => array('delivery_country'), 'show_errors'=>$show_errors)); ?>
        </div>
  </div>

  <?php
    endif;

    if (isset($customer_form[oepnvCustomerDBDataForm::SUBFORM_CONSENT]) && count($customer_form[oepnvCustomerDBDataForm::SUBFORM_CONSENT]) > 0)
    {
      echo '<br />';
      include_partial('generic/customer_consent', array('form'=>$customer_form->getSubForm(oepnvCustomerDBDataForm::SUBFORM_CONSENT), 'field_schema' => $customer_form[oepnvCustomerDBDataForm::SUBFORM_CONSENT], 'show_text' => true));
      echo '<br />' ;
    }
    include_partial('text_under_data', array('register' => isset($register) && $register)); ?>
  </div>

<?php
  if ((isset($register) && $register) || sfContext::getInstance()->getUser()->isAuthenticatedNotRegisteredShopUser()):
  if($disable_input && isset($customer_property_form['password_reset_auth_question']))
  {
    echo '<h1>' .  __('Zugangsdaten', array(), 'personal_data') . '</h1>';
  }

  if (isset($customer_property_form['password_reset_auth_question']) || (isset($customerForm['password']) && ($disable_input || sfConfig::get('app_registration_ask_password', false))) || isset($customerForm['username'])): ?>
    <h1><?php echo __('Zugangsdaten', array(), 'personal_data'); ?></h1>
    <div class="content-section">
    <?php include_partial('text_above_paymentmethods', array('register' => isset($register) && $register)); ?>

    <div class="presentation-block personal-data-3">

    <?php if($disable_input) : ?>
        <div class="field">
          <?php echo '<label>' . __('Benutzername', array(), 'personal_data') . '</label>'; ?>
            <div class="field-widget readonly"><?php echo $customerForm['email']->getValue(); ?></div>
        </div>
    <?php endif;

    if(isset($customerForm['username']))
    {
      include_partial('global/render_as_presentation_block_field',
        array(
          'form' => $customerForm,
          'validator_schema' => $validator_schema_customer,
          'ffnames' => array('username'),
          'show_errors' => $show_errors
        )
      );
    }

    if (isset($customerForm['password']) && ($disable_input || sfConfig::get('app_registration_ask_password', true)))
    {
      if(sfConfig::get('app_password_show_policy', false))
      {
        include_partial('password_policy');
      }
      if(!isset($register))
        echo __('Wenn Sie das Passwort nicht ändern möchten, lassen Sie die folgenden beiden Felder bitte leer.',array(),'personal_data') . '<br/><br/>';

      include_partial('global/render_as_presentation_block_field',
        array(
          'form'             => $customerForm,
          'validator_schema' => $validator_schema_customer,
          'ffnames'          => array('password'),
          'show_errors'      => $show_errors
        )
      );
      include_partial('global/render_as_presentation_block_field',
        array(
          'form'             => $customerForm,
          'validator_schema' => $validator_schema_customer,
          'ffnames'          => array('password2'),
          'show_errors'      => $show_errors
        )
      );
    }

    if (isset($customer_property_form['password_reset_auth_question']))
    {
      include_partial('global/render_as_presentation_block_field',
        array(
          'form'             => $customer_property_form,
          'validator_schema' => $validator_schema_customer_properties,
          'ffnames'          => array('password_reset_auth_question'),
          'show_errors'      => $show_errors
        )
      );
    }

    if (isset($customer_property_form['password_reset_auth_answer']))
    {
      include_partial('global/render_as_presentation_block_field',
        array(
          'form'             => $customer_property_form,
          'validator_schema' => $validator_schema_customer_properties,
          'ffnames'          => array('password_reset_auth_answer'),
          'show_errors'      => $show_errors
        )
      );
    }
    echo '</div>';
    include_partial('text_under_paymentmethods', array('register' => isset($register) && $register));
    echo '</div>';
  endif;
  endif;

  if (isset($register) && $register)
  {
    include_partial('registerActions');
  }
  else
  {
    include_partial('modifyActions', array('has_missing_field_values' => $has_missing_field_values, 'has_missing_security_values' => $has_missing_security_values));
  }
  include_partial('text_under_actions', array('register' => isset($register) && $register));
  ?>
</form>
</div>