<?php header('HTTP/1.1 503 Maintenance'); ?>
<?php header('Status: 503 Maintenance'); ?>
<?php $path = sfConfig::get('sf_relative_url_root', preg_replace('#/[^/]+\.php5?$#', '', isset($_SERVER['SCRIPT_NAME']) ? $_SERVER['SCRIPT_NAME'] : (isset($_SERVER['ORIG_SCRIPT_NAME']) ? $_SERVER['ORIG_SCRIPT_NAME'] : ''))) ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
<head>

<link rel="shortcut icon" href="/favicon.ico" />

<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $path ?>/backend/css/generic.css" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $path ?>/backend/css/structure.css" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $path ?>/layout/css/layout.css" />
  
  <script type="text/javascript" src="<?php echo $path ?>/js/jquery.js"></script>
  <!--[if lt IE 7]>
    <script type="text/javascript" src="<?php echo $path ?>/js/ie-lt-7.js"></script>
  <![endif]-->
  
  <script type="text/javascript" src="<?php echo $path ?>/js/jquery-plugins.js"></script>
  <script type="text/javascript" src="<?php echo $path ?>/js/jquery-ui.js"></script>
  <script type="text/javascript" src="<?php echo $path ?>/js/tickeos.js"></script>
  
</head>
  <body class="maintenance">
  <div id="wrapper">
    <img src="/layout/images/wartung_bg.png">
    <div class="overlay"></div>
    <div class="info">
      <h1>Wartungsarbeiten / Maintenance</h1>
      <p>Für die kontinuierliche Verbesserung unseres Angebotes sind Wartungsarbeiten an unseren Systemen notwendig.
        Wir sind so schnell wie möglich wieder für Sie da, so dass Sie den Onlineshop wie gewohnt nutzen können.
        Wir bitten um Ihr Verständnis.</p>
      <p>Our system is currently unavailable due to scheduled maintenance in the course of our continuous service improvement.
        The necessary revisions will be resolved as soon as possible and you will be able to access the online shop again.
        Thank you for your understanding.</p>
    </div>
  </div>
  </body>
</html>