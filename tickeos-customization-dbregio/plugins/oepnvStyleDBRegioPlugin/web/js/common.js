function add_showmoreorless(container_sel, content_sel, title_sel, title_clickable, hide_others)
  {
    if($(container_sel).size() < 100)
    {
        $(container_sel).each(function ()
        {
              var container = $(this); 
	      // &#9660; = Down Triangle
	      // &#9650; = Up Triangle
	      // Erstes Element nach der Überschrift durchsuchen
	      var content = $(content_sel, this).eq(0);
	      
	      if (content.length == 0)
	        return;
	      
	      if (container.hasClass('content-is-hidden'))
	      {
	        content.hide();
	      }
	      var showmore = !content.is(':visible');
	      
	      var title = $(title_sel, this);
	      title.prepend('<a href="#" class="icon '+ (showmore?'showmore':'showless') +'" style="float:right"><span>' + (showmore?'&#9660;':'&#9650;') + '</span></a>');
	      if (title_clickable != undefined && title_clickable)
	      {
	    	  var click_target = $(title);
	      } else
	      {
	    	  var click_target = $('a.icon', title);
	      }
	      
	      click_target.click(function ()
	        {
	    	  if (hide_others != undefined && hide_others)
	    	  {
	            $(container_sel).not(container).not('.content-is-hidden').find(title_sel).click();
	    	  }
	    	  
	          var content_is_visible = content.is(':visible');
                  
                  if($.browser.msie && parseInt($.browser.version, 10) < 9)
                  {
                     content.toggle(); 
                  }
                  else
                  {
                    content.slideToggle('normal');
                  }

	          content_is_visible = !content_is_visible;
	          
	          if ($(this).is('a.icon'))
	          {
	            var icon = $(this);	          
	          } else 
	          {
	        	var icon = $('a.icon', this);
	          }
	          if (content_is_visible)
	          {
	            container.removeClass('content-is-hidden');
	            $('span', icon.removeClass('showmore').addClass('showless')).html('&#9650;');
	          } else
	          {
	            container.addClass('content-is-hidden');
	            $('span', icon.removeClass('showless').addClass('showmore')).html('&#9660;');          
	          }
	          
	          return false;
	        }
	      )  
	    }
	  );  
    }        
  }

function add_showmoreorless_shop(container_sel, content_sel, title_sel)
{
	var containers = $(container_sel);
	if (containers.length > 1)
    containers.each(function ()
    {
      var container = $(this); 
      // &#9660; = Down Triangle
      // &#9650; = Up Triangle
      // Erstes Element nach der Überschrift durchsuchen
      var content = $(content_sel, this).eq(0);
      
      if (content.length == 0)
        return;
      
      if (container.hasClass('content-is-hidden'))
      {
        content.hide();
      }
      var showmore = !content.is(':visible');
      
      var title = $(title_sel, this);
      title.prepend('<a href="#" class="icon '+ (showmore?'showmore':'showless') +'" style="float:right"><span>' + (showmore?'&#9660;':'&#9650;') + '</span></a>');
      //$('a.icon', title).click(function ()
      $(title).click(function ()
        {
    	  //title_clickable
          $(container_sel).not(container).not('.content-is-hidden').find(title_sel).click();
          var content_is_visible = content.is(':visible');
          content.slideToggle('normal');
          content_is_visible = !content_is_visible;
          
          var icon = $('a.icon', this);

          if (content_is_visible)
          {
            container.removeClass('content-is-hidden');
            $('span', icon.removeClass('showmore').addClass('showless')).html('&#9650;');
          } else
          {
            container.addClass('content-is-hidden');
            $('span', icon.removeClass('showless').addClass('showmore')).html('&#9660;');          
          }
          
          return false;
        }
      )  
    }
  );  
}