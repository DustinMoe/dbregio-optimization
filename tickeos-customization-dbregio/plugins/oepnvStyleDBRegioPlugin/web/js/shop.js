function SetupBasics()
{
  $('form.enable-hint input[type=text]').hint('shows-title-as-hint');
  
  
  if ($.fn.datePicker)
  {
    var validation_date = $('div.enable-calendar #product_validation_date_date.days');
    
    if (validation_date.length > 0)
    {  
      
      var start_date = $('option:first',validation_date).val();
      if (start_date == '')
        start_date = $('option:nth-child(2)',validation_date).val();
      
      var end_date = $('option:last',validation_date).val();
      var selected_date = $('option:selected',validation_date).val();
      
      $('div', validation_date.hide().parent().addClass('has-calendar').append('<div></div>')).datePicker({
        startDate: Date.fromString(start_date, 'yyyy-mm-dd'),
        endDate: Date.fromString(end_date, 'yyyy-mm-dd'),      
        showYearNavigation:false,
        createButton:false, 
        inline:true,
        renderCallback:function($td, thisDate, month, year){
            if(!validation_date.find('option[value="' + thisDate.asString('yyyy-mm-dd') + '"]').length){
                $td.addClass('disabled');
            }
        }
      })
      .dpSetSelected(Date.fromString(selected_date, 'yyyy-mm-dd').asString())
      .bind(
       'dateSelected',
       function(e, selectedDate, $td)
       {
         var new_date = new Date(selectedDate);         
         $('option[value=\''+ new_date.asString('yyyy-mm-dd') +'\']',validation_date).attr('selected', 'selected');
         validation_date.trigger('change');
       }
      );

    }
  }

  if (typeof add_showmoreorless !== 'undefined'){
    add_showmoreorless(".relation_search_result_container:not(.no_moreorless)", '.relation_search_result_form', 'h3', true, true);
  }

   // NUR FÜR IE, allen Option-Elementen ihren Inhalt als Title, falls diese abgeschnitten werden
  /*@cc_on
  $('select option').each (function(index, item){
    $(item).attr('title', $(item).text());
  });
  @*/
}  

$(document).ready(SetupBasics);

$(document).ready(changeAction);

function changeAction()
{
  $('.navi li').each(function(index)
  {
      var link = $(this).children().attr('href');
      if(link)
      {
        var shop2 = link.lastIndexOf("/");
        link = link.substr(0, shop2);
        shop2 = link.lastIndexOf("/");
        link = link.substr(shop2+1, link.length);
        $(this).children().addClass("new-navi-class-" + link);
        $(this).children().attr("id", "new-navi-id-" + link);
      }
  });

  $('.new-navi-class-schedule').click(function()
  {
      var path = window.location.pathname;
      var shop = path.indexOf(".php");
      var history = path.substr(shop+4, path.length);

      if(history == '/relation/search' || history == '/relation/search_commutation')
      {
          $('.relation_search_form .enable-hint').attr('action', $(this).attr('href') + '#results');
          $('.new-navi-class-schedule').removeAttr('href');
          $('.relation_search_form form:first').submit();
      }
  });

  $('.new-navi-class-relation').click(function()
  {
     var path = window.location.pathname;
     var shop = path.indexOf(".php");
     var history = path.substr(shop+4, path.length);

     if(history == '/schedule/search' || history == '/schedule/search/0/0/0')
      {
          $('.relation_search_form .enable-hint').attr('action', $(this).attr('href') + '#results');
          $('.new-navi-class-relation').removeAttr('href');
          $('.relation_search_form form:first').submit();
      }
  });
}

