<?php
class oepnvStyleDBRegioEventListener
{
  static public function ticketTypesRegistryReadyEvent(sfEvent $event)
  {
    TicketDBRegioStandard::register();
    TicketDBRegioStandardSubproducts::register();
    TicketDBRegioStandardSignature::register();
    VVSStudiTicketDBRegio::register();
    NaldoDBRegio::register();
    KVVStudiKarteDBRegio::register();
    VRNSemesterTicket::register();
    CombiTicketDBRegioAutozug::register();
    TicketDBRegioGRP::register();
    TicketDBRegioEuropass::register();
    TicketDBRegioBernstein::register();
    TicketDBRegioAirportExpress::register();
    TicketDBRegioStudiSparTicket::register();
    TicketDBRegioDeutschlandPass::register();
    TicketDBRegioKulturzug::register();
    TicketDBRegioDEKT::register();
  }
      
  public static function semesterticketStudentVerificationServiceRegistryReadyEvent(sfEvent $event)
  {
    SemesterticketStudentVerificationServiceSSBDefault::register();
    SemesterticketStudentVerificationServiceHsEsslingen::register();
    SemesterticketStudentVerificationServiceUniHeidelberg::register();
    SemesterticketStudentVerificationServiceHSZBW::register();
  }

  static public function priceCalculationRegistryReadyEvent(sfEvent $event)
  {
    PriceCalculationDBRegioStudiSparTicket::register();
  }

  static public function uicLayoutRegistryReadyEvent(sfEvent $event)
  {
    Uic918_3_autozug::register();
  }
  
  static public function productPostValidationRegistryReadyEvent(sfEvent $event)
  {
    ProductPostValidationDbRegio::register();
    ProductPostValidationDeutschlandPass::register();
    ProductPostValidationDbregioMitfahrer::register();
  }
}