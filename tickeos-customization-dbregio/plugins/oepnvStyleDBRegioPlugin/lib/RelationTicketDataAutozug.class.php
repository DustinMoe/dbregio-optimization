<?php
/**
 * additional ticket data:
 * - return journey
 * (- reservations)
 */
class RelationTicketDataAutozug extends BaseTicketData
{
  /**
   * valid from return
   *
   * UIC918-3: (UIC Layout):    $valid_from_return
   * @var int (timestamp)
   */
  protected $valid_from_return;

  /**
   * valid till return
   *
   * UIC918-3: (UIC Layout):    $valid_till_return
   * @var int (timestamp)
   */
  protected $valid_till_return;
  
  /**
   * couch class  return (1, 2 or empty string)
   *
   * UIC918-3: (UIC Layout):    $couch_class_return
   * @var str(1)
   */
  protected $couch_class_return;
  
  /**
   * from name return
   * 
   * UIC918-3: (UIC Layout):    $from_return
   * @var str
   */  
  protected $from_name_return;
  
  /**
   * to name return
   * 
   * UIC918-3: (UIC Layout):    $to_return
   * @var str
   */  
  protected $to_name_return;
  
  /**
   * via name(s) return
   * 
   * @var str
   */  
  protected $via_name_return;
  
  /**
   * names of object properties
   * @var array<str>
   */
  protected $PROPERTIES = array(
    // from BaseTicketData
    'ticket_id', 'ticket_type', 'status', 'product_name', 'tariff_zone',
    'passenger_first_name', 'passenger_last_name', 'passenger_birthday',
    'verifying_document_name', 'verifying_document_short', 'verifying_document_number',
    'info_text', 'valid_from', 'valid_till', 'created_at', 'valid_datetime_string', 'couch_class',
    'from_name', 'to_name', 'via_name',
    'passenger_type_1_name', 'passenger_type_1_number', 'passenger_type_2_name', 'passenger_type_2_number',
    'price', 'vat_rate', 'currency', 'payment_method',
    // self
    'valid_from_return', 'valid_till_return', 'couch_class_return', 'from_name_return', 'to_name_return', 'via_name_return',
	'ticket_type2','car_h_value','car_r_value','car_h_place_value','car_r_place_value','befoerderer_value','tarif_out','tarif_ret'
  );

  public function setTarifRet($v)
  {
    $this->tarif_ret = $v;
  }
  
  public function getTarifRet()
  {
    return $this->tarif_ret;
  }
  
  public function setTarifOut($v)
  {
    $this->tarif_out = $v;
  }
  
  public function getTarifOut()
  {
    return $this->tarif_out;
  }
  
  public function setBefoerdererValue($v)
  {
    $this->befoerderer_value = $v;
  }
  
  public function getBefoerdererValue()
  {
    return $this->befoerderer_value;
  }
  
  public function setCarRPlaceValue($v)
  {
    $this->car_r_place_value = $v;
  }
  
  public function getCarRPlaceValue()
  {
    return $this->car_r_place_value;
  }
  
  public function setCarHPlaceValue($v)
  {
    $this->car_h_place_value = $v;
  }
  
  public function getCarHPlaceValue()
  {
    return $this->car_h_place_value;
  }
  
  public function setCarRValue($v)
  {
    $this->car_r_value = $v;
  }
  
  public function getCarRValue()
  {
    return $this->car_r_value;
  }
  
  public function setCarHValue($v)
  {
    $this->car_h_value = $v;
  }
  
  public function getCarHValue()
  {
    return $this->car_h_value;
  }
  
  public function setTicketType2($v)
  {
    $this->ticket_type2 = $v;
  }
  
  public function getTicketType2()
  {
    return $this->ticket_type2;
  }
  
  
  public function setValidFromReturn($v)
  {
    $this->valid_from_return = $v;
  }
  public function getValidFromReturn($format=null)
  {
    if($format!=null && $this->valid_from_return!=null)
    {
      if(!is_int($this->valid_from_return))
      {
        $ts = strtotime($this->valid_from_return);
        if ($ts === -1 || $ts === false)
        {
          #throw new Exception("Unable to parse value of [valid_from] as date/time value: " . var_export($this->valid_from, true));
          return null;
        }
        return date($format, $ts);
      }
      return date($format, $this->valid_from_return);
    }
    return $this->valid_from_return;
  }

  public function setValidTillReturn($v)
  {
    $this->valid_till_return = $v;
  }
  public function getValidTillReturn($format=null)
  {
    if($format!=null && $this->valid_till_return!=null)
    {
      if(!is_int($this->valid_till_return))
      {
        $ts = strtotime($this->valid_till_return);
        if ($ts === -1 || $ts === false)
        {
          #throw new Exception("Unable to parse value of [valid_till] as date/time value: " . var_export($this->valid_from, true));
          return null;
        }
        return date($format, $ts);
      }
      return date($format, $this->valid_till_return);
    }
    return $this->valid_till_return;
  }
  
  public function setCouchClassReturn($v)
  {
    $this->couch_class_return = $v;
  }
  public function getCouchClassReturn()
  {
    return $this->couch_class_return;
  }

  public function setFromNameReturn($v)
  {
    $this->from_name_return = $v;
  }
  public function getFromNameReturn()
  {
    return $this->from_name_return;
  }
  
  public function setToNameReturn($v)
  {
    $this->to_name_return = $v;
  }
  public function getToNameReturn()
  {
    return $this->to_name_return;
  }
  
  public function setViaNameReturn($v)
  {
    $this->via_name_return = $v;
  }
  public function getViaNameReturn()
  {
    return $this->via_name_return;
  }
  
}
