<?php

class ProductPostValidationDbRegio extends ProductPostValidation
{

  public static function register()
  {
    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent(new self(), 'oepnv.product.post_validation.register'));
  }

  public function getId()
  {
    return 'dbregio';
  }

  public function getName()
  {
    return 'DBRegio';
  }

  public function getValidatorMessages()
  {
    return array(
      'mitfahrer2_missing' => 'Mitfahrer 2 fehlt.',
      'mitfahrer3_missing' => 'Mitfahrer 3 fehlt.',
      'mitfahrer4_missing' => 'Mitfahrer 4 fehlt.',
      'mitfahrer5_missing' => 'Mitfahrer 5 fehlt.',
    );
  }

  protected function isFilled($string)
  {
	if ($string!="" && $string !==null) {
	return true;
	} else {
	return false;
	}
  }
  
  public function validate($validator, $values, $options, oepnvOptionProduct $preview_object, OptionProductShopForm $form = null)
  {
    $product_type = ProductTypePeer::getFromRole('strecke');
  
    $errors = array();
	
	if($product_type!=null){	
		$product_type_id = $product_type->getId();
		$numberOfPersons=$values['product_option_'.$product_type_id]['person_number'];
		if ($numberOfPersons>=2){
			$id='product_personalization_property_'.PersonalizationPropertyPeer::retrieveByRole('mitreisender2')->getId();
			if (!$this->isFilled($values[$id])) {
				$errors[$id] = new sfValidatorError($validator,'mitfahrer2_missing');
			}
		}
		if ($numberOfPersons>=3){
			$id='product_personalization_property_'.PersonalizationPropertyPeer::retrieveByRole('mitreisender3')->getId();
			if (!$this->isFilled($values[$id])) {
				$errors[$id] = new sfValidatorError($validator,'mitfahrer3_missing');
			}
		}
		if ($numberOfPersons>=4){
			$id='product_personalization_property_'.PersonalizationPropertyPeer::retrieveByRole('mitreisender4')->getId();
			if (!$this->isFilled($values[$id])) {
				$errors[$id] = new sfValidatorError($validator,'mitfahrer4_missing');
			}
		}
		if ($numberOfPersons>=5){
			$id='product_personalization_property_'.PersonalizationPropertyPeer::retrieveByRole('mitreisender5')->getId();
			if (!$this->isFilled($values[$id])) {
				$errors[$id] = new sfValidatorError($validator,'mitfahrer5_missing');
			}
		}
	}
    
    if (sfContext::getInstance()->getConfiguration()->getApplication() != 'backend')
    {
      if (count($errors) > 0)
      {
        throw new sfValidatorErrorSchema($validator, $errors);
      }
    }

    return $values;
  }

  public function validatePassengerIdentityCard($validator, $values, $options,
                                                OepnvShoppingCartItemAdditionIdCard $id_card, oepnvOptionProduct $preview_object)
  {
    return $values;
  }
}