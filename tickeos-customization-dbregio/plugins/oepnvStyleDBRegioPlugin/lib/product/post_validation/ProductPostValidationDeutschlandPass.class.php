<?php

class ProductPostValidationDeutschlandPass extends ProductPostValidation
{

  public static function register()
  {
    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent(new self(), 'oepnv.product.post_validation.register'));
  }

  public function getId()
  {
    return 'dbregio_deutschland_pass';
  }

  public function getName()
  {
    return 'DB-Regio Deutschland-Pass';
  }

  public function getValidatorMessages()
  {
    return array();
  }

  public function validate($validator, $values, $options, oepnvOptionProduct $preview_object, OptionProductShopForm $form = null)
  {
    $errors = array();
    if ($preview_object !== null) {

      $birthday_field_id = 'product_personalization_property_'.PersonalizationPropertyPeer::retrieveByRole('birthday_deutschland_pass')->getId();
      $birthday = $values[$birthday_field_id];

      if ($birthday != "")
      {
        $birthday = new DateTime($birthday);
        $date = new DateTime(date('Y-m-d', $values['validation_date']));
        $age = $date->diff($birthday)->y;

        $max_age = $this->getMaxAge($preview_object);
        $min_age = $this->getMinAge($preview_object);

        if ($max_age != null)
        {
          if ($age > $max_age)
            $errors[$birthday_field_id] = new sfValidatorError($validator, sfContext::getInstance()->getI18N()->__('Der Ticketnutzer darf höchstens %max Jahre alt sein.', array('%max' => $max_age), 'validator'));
        }

        if ($min_age != null)
        {
          if ($age < $min_age)
            $errors[$birthday_field_id] = new sfValidatorError($validator, sfContext::getInstance()->getI18N()->__('Der Ticketnutzer muss mindestens %min Jahre alt sein.', array('%min' => $min_age), 'validator'));
        }

        if (sfContext::getInstance()->getConfiguration()->getApplication() != 'backend')
        {
          if (count($errors) > 0)
          {
            throw new sfValidatorErrorSchema($validator, $errors);
          }
        }
      }
    }
    return $values;
  }

  private static function getMaxAge($product)
  {
    if ($product !== null)
    {
      $max_age = $product->getMaxAgePurchasable();
      if ($max_age != '')
      {
        return $max_age;
      }
      else
      {
        foreach ($product->getSubProductSelections() as $selection)
        {
          if (($selection->getChild1Product()->getMaxAgePurchasable() != '') && ($selection->getChild1Product()->getMaxAgePurchasable() != null))
          {
            return $selection->getChild1Product()->getMaxAgePurchasable();
          }
        }
      }
    }
    return null;
  }

  private static function getMinAge($product)
  {
    if ($product !== null)
    {
      $min_age = $product->getMinAgePurchasable();
      if ($min_age != '')
      {
        return $min_age;
      }
      else
      {
        foreach ($product->getSubProductSelections() as $selection)
        {
          if (($selection->getChild1Product()->getMinAgePurchasable() != '') && ($selection->getChild1Product()->getMinAgePurchasable() != null))
          {
            return $selection->getChild1Product()->getMinAgePurchasable();
          }
        }
      }
    }
    return null;
  }

  public function validatePassengerIdentityCard($validator, $values, $options,
                                      OepnvShoppingCartItemAdditionIdCard $id_card, oepnvOptionProduct $preview_object)
  {
    return $values;
  }
}