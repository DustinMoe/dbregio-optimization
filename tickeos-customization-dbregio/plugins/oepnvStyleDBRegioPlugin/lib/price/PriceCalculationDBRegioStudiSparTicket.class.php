<?php

/**
 * Class PriceCalculationDBRegioStudiSparTicket
 * @author pwild <pascal.wild@eos-uptrade.de>
 */
class PriceCalculationDBRegioStudiSparTicket extends PriceCalculationBasic
{

  const ID = 'dbregio_flexicard';

  /**
   * register PriceCalculationDBRegioStudiSparTicket
   */
  public static function register()
  {
    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent(new self(),'oepnv.price.calculation.register'));
  }

  /**
   * Calculate price
   *
   * @param oepnvOptionProduct $product
   *
   * @return float complete price
   */
  public function calculatePrice(oepnvOptionProduct $product, $price = 0, $validity_begin = null)
  {
    $price = parent::calculatePrice($product, $price, $validity_begin);

    if ($price)
    {
      foreach ($product->getOepnvSubProductSelections() as $selection)
      {
        $child_product = $selection->getChild1Product();
        if ($child_product instanceof oepnvOptionProduct)
        {
          $person_number = $child_product->getProductOptionValue('person_number');
          if ($person_number)
          {
            $price = $price * $person_number;
          }
        }
      }
    }

    return $price;
  }

  /**
   * @return string
   */
  public function getId()
  {
    return self::ID;
  }

  /**
   * @return string
   */
  public function getName()
  {
    return sfContext::getInstance()->getI18N()->__('DB Regio Studi-Spar-Ticket', null, 'dbregio');
  }
}