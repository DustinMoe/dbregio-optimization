<?php

class VRNSemesterTicket extends TicketStandard
{
  const ID = 'vrn_semester_ticket';

  public static function register()
  {
    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent(new self(),'oepnv.ticket_types.register'));
  }

  public function getId()
  {
    return self::ID;
  }

  public function getName()
  {
    return "VRN-Semesterticket";
  }

  // ----------------------------------------------------------------------------------------------
  
  /**
   * @var arr<str> available ticket elements
   * (can be overloaded by child classes)
   */
  protected $ELEMENTS = array (
    self::ELEMENT_AZTEC_BARCODE,
    #self::ELEMENT_QR_BARCODE,
    self::ELEMENT_SECURITY_SYMBOL,
    self::ELEMENT_SERIAL_CODE,
    self::ELEMENT_SERIAL_CODE_MIRRORED
  );
  
  /**
   * returns the template-dependent data fields (data by a TicketDataProvider)
   * (array with replacer_names as keys and the value as value)
   * @return arr<field_name::str>, field_name can be a ticket role
   */
  public function getDataFields()
  {
    $ret = array();

    $identity_card = $this->getDataProviderValue(TicketDataProvider::PASSENGER, TicketDataProviderPassenger::IDENTITY_CARD_NAME);
    $product_tax_7 = $this->getDataProviderValue(TicketDataProvider::ORDER, TicketDataProviderOrder::PPSUM_TAX_7);
    $product_price = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PRICE);
    $ret['field01'] = $this->getDataProviderValue('tickeos_product', TicketDataProviderTickeosProduct::NAME);
    $ret['field02'] = 'Gültig im gesamten Gebiet des VRN';
	  $ret['field021'] = '(außer Westpfalz)';
	
    $ret['field03'] = $this->getDataProviderValue('tickeos_product', TicketDataProviderTickeosProduct::VALID_DATE_STRING);

    $ret['field04'] = $this->getDataProviderValue(TicketDataProvider::PASSENGER, TicketDataProviderPassenger::FIRST_AND_LAST_NAME).', '.$this->getDataProviderValue(TicketDataProvider::PASSENGER, TicketDataProviderPassenger::BIRTHDAY);
    $ret['field05'] = $this->getDataProviderValue('customer', TicketDataProviderCustomer::SEMESTERTICKET_UNIVERSITY) . ' - Matrikelnummer: ' . $this->getDataProviderValue('customer', TicketDataProviderCustomer::SEMESTERTICKET_MATRICULATION_NUMBER);

    $ret['field06'] =  'gekauft am: ' . $this->getDataProviderValue('order', TicketDataProviderOrder::CREATED_AT);
    $ret['field07'] = 'Preis: €' . $product_price . ' (incl. 7% MwSt. €' . $product_tax_7 . ')';
    $ret['field08'] =   'Nur gültig mit Studierendenausweis und ' . $identity_card;
	
    $ret['authid'] = $identity_card{0};
    $ret['ticketid'] = $this->getDataProviderValue('tickeos_product', TicketDataProviderTickeosProduct::SERIAL_NUMBER);
    
    // folgende zeilen aus getElementReplacements() + getBackgroundDataFields() übernommen
    // background text (use cache)
    $bt = $ret;
    unset($bt['field02']);
    $bt['validity_text'] = $this->getDataProviderValue('passenger', TicketDataProviderPassenger::IDENTITY_CARD_NAME);
    $ret['background_text'] = $this->generateBackgroundTextBase($bt, 4);
    //seltsam, ist aber in allen childs auch so gemacht
    $ret['security_code'] = $this->generateSerialNumberMirrored();
    
    return $ret;
  }

  
  /**
   * returns base ticket data, used by all kind of data getters.
   * this class can be overloaded by concrete ticket processing class
   * to customize fields/values
   * @return BaseTicketData
   */
  public function getTicketData()
  {
    $tdata = parent::getTicketData();
    $tdata->setPassengerType1Name('Student');
    $tdata->setVerifyingDocumentName('Studentenausweis');
    return $tdata;
  }
  
  /**
   * returns the data for UIC918
   * @return UicLayoutContainer Data in UIC918-3 structure (head, body)
   */
  public function getTicketUic918()
  {
    return $this->getTicketUic918_3Default();
  }

}
