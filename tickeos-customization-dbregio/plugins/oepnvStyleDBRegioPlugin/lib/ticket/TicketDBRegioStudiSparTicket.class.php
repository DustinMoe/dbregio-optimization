<?php
class TicketDBRegioStudiSparTicket extends TicketDBRegioStandard
{
  const ID = 'db_regio_studi_spar_ticket';

    public static function register()
    {
        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent(new self(),'oepnv.ticket_types.register'));
    }

    public function getId()
    {
        return self::ID;
    }

  public function getName()
  {
    return "DB Regio Studi-Spar-Ticket";
  }
  
  /**
   * returns the template-dependent data fields (data by a TicketDataProvider)
   * (array with replacer_names as keys and the value as value)
   * @return arr<field_name::str>, field_name can be a ticket role
   */
  public function getDataFields()
  {
    $ret = parent::getDataFields();   
    
    $ret['product_info_text'] = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::INFO_TEXT);
    $person_number = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PERSON_NUMBER, null, self::SCOPE_STRECKE);
    $ret['person_number'] = $person_number;
    $persons_names = $this->getDataProviderValue(TicketDataProvider::CUSTOMER, TicketDataProviderCustomer::EXTRACT_PERSONALIZATION_VARS);
    
    if ($person_number > 1)
      $ret['person_label'] = 'Mitreisende:';
    else
      $ret['person_label'] = '';
    
    //Max. Anzahl Personen
    for ($i = 2; $i <= 5; $i++)
    {
      if ($i <= ($person_number))
        $ret['person_' . $i] = $i-1 . '. ' . $persons_names['personalization_mitreisender' . $i];
      else
        $ret['person_' . $i] = '';
    }
    $ret['direction'] = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::NAME, null, self::SCOPE_STRECKE);
    return $ret;
  }
}
