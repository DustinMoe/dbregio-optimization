<?php

class KVVStudiKarteDBRegio extends TicketStandard
{
  const ID = 'db_regio_kvv_studi_karte';
	
  public static function register()
  {
    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent(new self(), 'oepnv.ticket_types.register'));
  }

  public function getId()
  {
    return self::ID;
  }

  public function getName()
  {
    return "DB Regio KVV Studi Karte";
  }
  
  /** 
   * @var arr<str> available ticket elements
   * (can be overloaded by child classes)
   */
  protected $ELEMENTS = array (
    self::ELEMENT_AZTEC_BARCODE,
    self::ELEMENT_QR_BARCODE,
    self::ELEMENT_SECURITY_SYMBOL,
    self::ELEMENT_SERIAL_CODE
  );
  
  /**
   * returns the template-dependent data fields (data by a TicketDataProvider)
   * (array with replacer_names as keys and the value as value)
   * @return arr<field_name::str>, field_name can be a ticket role
   */
  public function getDataFields()
  {
    $months = array(
        1 => "JAN",
        2 => "FEB",
        3 => "MÄR",
        4 => "APR",
        5 => "MAI",
        6 => "JUN",
        7 => "JUL",
        8 => "AUG",
        9 => "SEP",
        10 => "OKT",
        11 => "NOV",
        12 => "DEZ"
    );
    
    $valid_from_date = strtotime($this->getDataProviderValue('tickeos_product', TicketDataProviderTickeosProduct::VALID_FROM_DATE));
    $valid_to_date = strtotime($this->getDataProviderValue('tickeos_product', TicketDataProviderTickeosProduct::VALID_TO_DATE));
    
    $ticket_id_str = substr((string) $this->getTicketData()->getTicketId(), 1, 7);
    $tarif_group = 'Netz';
    $valid_from = $months[date('n', $valid_to_date)] . "/" . date('y', $valid_to_date);
    $product_tax_7 = $this->getDataProviderValue(TicketDataProvider::ORDER, TicketDataProviderOrder::PPSUM_TAX_7);
    $product_price = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PRICE);
    $ret = array(
        'head_line_1' => $this->getDataProviderValue('tickeos_product', TicketDataProviderTickeosProduct::NAME),
        'head_line_2' => $tarif_group,
        
        'line_1' => substr($this->getLastName() . ", " . $this->getFirstName(),0,40),
        'line_2' => $this->getBirthday(),
        'line_3' => "Gültig ab " . date('d.m.Y', $valid_from_date),
        'line_4' => "Gekauft am " . $this->getDataProviderValue('tickeos_product', TicketDataProviderTickeosProduct::CREATED_DATE_STRING) . ", " . $this->getDataProviderValue('tickeos_product', TicketDataProviderTickeosProduct::CREATED_TIME_STRING) . " Uhr",
        'line_5' => $ticket_id_str,
        'line_6' => "Nur gültig mit Personalausweis oder Reisepass.",
        'line_7' => "Es gelten die Tarifbestimmungen und Beförderungsbedingungen.",
        'line_8' => $valid_from,
        'line_9' => "Preis: €" . $product_price . ' (incl. 7% MwSt. €' . $product_tax_7 . ')',
        'id_card'        => 'Kontrollmedium:',
        'id_card_name'   => $this->getDataProviderValue(TicketDataProvider::PASSENGER, TicketDataProviderPassenger::IDENTITY_CARD_NAME),
        
        'side_line_1' => $this->getDataProviderValue('tickeos_product', TicketDataProviderTickeosProduct::NAME).' '.$tarif_group,
        'side_line_2' => "Gültig ab: " . substr($this->getDataProviderValue('tickeos_product', TicketDataProviderTickeosProduct::VALID_FROM_DATE), 0, 10),
        'side_line_3' => "Gültig bis: " . substr($this->getDataProviderValue('tickeos_product', TicketDataProviderTickeosProduct::VALID_TO_DATE), 0, 10),
        'side_line_4' => substr($this->getLastName() . ", " . $this->getFirstName(),0,30),
        'side_line_5' => $this->getBirthday(),
        
        'ticket_id_for_symbol' => $ticket_id_str
    );

    //geschützte Bindestriche + Leerzeichen einfügen, um Zeilenumbruch zu vermeiden
    $ret['line_1'] = str_replace('-', "\xE2\x80\x91", $ret['line_1']);
    $ret['line_1'] = str_replace(' ', "\xC2\xA0", $ret['line_1']);

    $serial_code = preg_split('//', $ticket_id_str, -1, PREG_SPLIT_NO_EMPTY);
    
    $ret['serial_code_0'] = '';
    $ret['serial_code_1'] = '';
    $ret['serial_code_2'] = '';
    $ret['serial_code_3'] = '';
    $ret['serial_code_4'] = '';
    $ret['serial_code_5'] = '';
    $ret['serial_code_6'] = '';
	
    foreach($serial_code as $k=>$v)
    {
      $ret['serial_code_'.($k)] = $v;
    }
    
    return $ret;
  }
  
  public function getTicketData()
  {
    $tdata = parent::getTicketData();
    
    $tdata->setPassengerFirstName($this->getFirstName());
    $tdata->setPassengerLastName($this->getLastName());
    $tdata->setPassengerBirthday($this->getBirthday());
    
    return $tdata;
  }
  
    /**
   * returns the data for UIC918
   * @return UicLayoutContainer Data in UIC918-3 structure (head, body)
   */
  public function getTicketUic918()
  {
    return $this->getTicketUic918_3Default();
  }

  /**
   * @return string
   */
  public function getFirstName()
  {
    $first_name = null;
    try
    {
      $first_name = $this->getDataProviderValue('passenger', TicketDataProviderPassenger::FIRST_NAME_SHIBBOLETH);
    }
    catch(Exception $e)
    {

    }

    if(!$first_name)
    {
      $first_name = $this->getDataProviderValue('passenger', TicketDataProviderPassenger::FIRST_NAME);
    }
    return $first_name;
  }

  /**
   * @return string
   */
  public function getLastName()
  {
    $last_name = null;
    try
    {
      $last_name = $this->getDataProviderValue('passenger', TicketDataProviderPassenger::LAST_NAME_SHIBBOLETH);
    }
    catch(Exception $e)
    {

    }
    if(!$last_name)
    {
      $last_name = $this->getDataProviderValue('passenger', TicketDataProviderPassenger::LAST_NAME);
    }
    return $last_name;
  }

  /**
   * @return string
   */
  public function getBirthday()
  {
    $birthday = null;
    try
    {
      $birthday = $this->getDataProviderValue('passenger', TicketDataProviderPassenger::BIRTHDAY_SHIBBOLETH);
    }
    catch(Exception $e)
    {

    }
    if(!$birthday)
    {
      $birthday = ($this->getDataProviderValue('passenger', TicketDataProviderPassenger::BIRTHDAY)) ? $this->getDataProviderValue('passenger', TicketDataProviderPassenger::BIRTHDAY) : $this->getDataProviderValue('customer', TicketDataProviderPassenger::BIRTHDAY);
    }
    return $birthday;
  }
  
}