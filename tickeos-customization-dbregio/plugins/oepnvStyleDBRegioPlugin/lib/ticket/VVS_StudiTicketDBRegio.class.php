<?php
/**
 * SSB Base (Standard)
 *
 * @author bahlers
 *
 */
class VVSStudiTicketDBRegio extends TicketStandard
{
  const ID = 'db_regio_vvs_studi_ticket';

  // *************************************************************************************

  public static function register()
  {
    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent(new self(),'oepnv.ticket_types.register'));
  }

  public function getId()
  {
    return self::ID;
  }

  public function getName()
  {
    return "DB Regio VVS Studi Ticket";
  }

  // *************************************************************************************

  /**
   * returns the template-dependent data fields (data by a TicketDataProvider)
   * (array with replacer_names as keys and the value as value)
   * @return arr<field_name::str>, field_name can be a ticket role
   */
  public function getDataFields()
  {
    $ret = array();

    // Produktname
    $ret['ticket_name']     = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::NAME);
    // Fahrbereich
    $ret['tariff_name']     = 'Netz';//$this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::EXTRACTED_VAR, 'option_tarifgruppe_printticket_text');
    // Konfort
    $ret['comfort']         = '2. Klasse';
    // Zonen
    //$ret['tariff_zones']    = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::EXTRACTED_VAR, 'option_tarifgruppe');
    $tariff_zones           = str_replace(',', '/', $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::TARIFF_ZONE));
    $ret['tariff_zones']    = ($tariff_zones == '') ? 'Netz' : $tariff_zones;
    // Gültigkeit
    $ret['validity']        = trim(str_replace('00:00', '', $this->getDataProviderValue('tickeos_product', TicketDataProviderTickeosProduct::VALID_FROM_DATE))) . ' - '. trim(str_replace('23:59', '', $this->getDataProviderValue('tickeos_product', TicketDataProviderTickeosProduct::VALID_TO_DATE)));
    // Vorname Nachname
    $ret['passenger_name']  = $this->getFirstName() . " " . $this->getLastName();
//    $ret['passenger_first_name']  = $this->getFirstName();
//    $ret['passenger_last_name']  = $this->getLastName();

    //max. 15 Zeichen pro Zeile (gemessen an "w") + 3 Zeichen zusätzlich
    //3 Zeilen für Vorname + Nachname möglich
    //max 2 Zeilen für Nachname, min 1 Zeile für Vorname
    $first_name = $this->getFirstName();
    $last_name = $this->getLastName();
    $count_chars_name = strlen($last_name) + strlen($first_name);
    if ($count_chars_name > 36)
    {
     if(strlen($last_name) <= 18) {
       //Wenn Nachname nur 1 Zeile braucht, dann kann Vorname 2 Zeilen bekommen
        $ret['passenger_first_name'] = substr($first_name, 0, 36);
      } else {
       $ret['passenger_first_name'] = substr($first_name, 0, 18);
       $ret['passenger_last_name'] = substr($last_name, 0, 36);
      }
    } else {
      $ret['passenger_last_name'] = $last_name;
      $ret['passenger_first_name'] = $first_name;
    }
    //geschützte Bindestriche + Leerzeichen einfügen, um Zeilenumbruch zu vermeiden
    $ret['passenger_last_name'] = str_replace('-', "\xE2\x80\x91", $ret['passenger_last_name']);
    $ret['passenger_last_name'] = str_replace(' ', "\xC2\xA0", $ret['passenger_last_name']);
    $ret['passenger_first_name'] = str_replace('-', "\xE2\x80\x91", $ret['passenger_first_name']);
    $ret['passenger_first_name'] = str_replace(' ', "\xC2\xA0", $ret['passenger_first_name']);

    $ret['created_at']      = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::CREATED_STRING);

    $ret['id_card']         = 'Kontrollmedium:';
    $ret['id_card_name']    = $this->getDataProviderValue(TicketDataProvider::PASSENGER, TicketDataProviderPassenger::IDENTITY_CARD_NAME);

    $ret['matriculation_number'] = 'Matrikelnummer: ' . $this->getDataProviderValue(TicketDataProvider::CUSTOMER, TicketDataProviderCustomer::SEMESTERTICKET_MATRICULATION_NUMBER);
    $ret['university']       =  $this->getDataProviderValue(TicketDataProvider::CUSTOMER, TicketDataProviderCustomer::SEMESTERTICKET_UNIVERSITY);
    
    $ret['price']           = '€ ' . str_replace(' €', '', $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PRICE_WITH_SYMBOL));

    $ret['ticket_id']       = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::SERIAL_NUMBER);
    
    // folgende zeilen aus getElementReplacements() übernommen
    $bt = $ret;
    unset($bt['created_at'], $bt['price'], $bt['ticket_id'], $bt['tariff_zones'], $bt['passenger_first_name'], $bt['passenger_last_name']);
    $ret['background_text'] = $this->generateBackgroundTextBase($bt, 4);

    return $ret;
  }

  public function getTicketData()
  {
    $tdata = parent::getTicketData();
    $tdata->setProductName($this->getDataProviderValue('tickeos_product', 'ticket_text'));
  
    $tariff_zones = str_replace(',', '/', $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::TARIFF_ZONE));
    $tariff_zones = ($tariff_zones == '') ? 'Netz' : $tariff_zones;    
    
    $tdata->setDescription($tariff_zones);
    $tdata->setTariffZone($tariff_zones);
    
    return $tdata;
  }  
  
  /**
   *
   * returns the data for UIC918
   * @return UicLayoutContainer Data in UIC918 structure (head, body)
   */
  public function getTicketUic918()
  {
    $ticket_head = new UicUheadLayout();
    $ticket_head->setTicketKey($this->getTicketData()->getTicketId(true));

    $uic918 = new Uic918_plain();
    $uic918->setPassengerName($this->getTicketData()->getPassengerFirstName().' '.$this->getTicketData()->getPassengerLastName());
    $uic918->setProductName($this->getDataProviderValue('tickeos_product', 'ticket_text'));
    $tariff_zones = str_replace(',', '/', $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::TARIFF_ZONE));
    $tariff_zones = ($tariff_zones == '') ? 'Netz' : $tariff_zones;
    $uic918->setTariffZone($tariff_zones);
    $uic918->setValidityText($this->getTicketData()->getValidFrom().'-'.$this->getTicketData()->getValidTill());
    $uic918->setVerifyingDocument($this->getTicketData()->getVerifyingDocumentShort());
    $uic918->setBirthday($this->getTicketData()->getPassengerBirthday());
    $uic918->setStartLocation($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::EXTRACTED_VAR, 'location'));

    $uic_ut_layout = $uic918->createUicUtLayout();
    $ticket_container = new UicLayoutContainer($ticket_head, $uic_ut_layout);
    return $ticket_container;
  }
  
  /**
   * @return string
   */
  public function getFirstName()
  {
    $first_name = $this->getDataProviderValue('passenger', TicketDataProviderPassenger::FIRST_NAME_SHIBBOLETH);
    if(!$first_name)
    {
      $first_name = $this->getDataProviderValue('passenger', TicketDataProviderPassenger::FIRST_NAME);
    }
    return $first_name;
  }

  /**
   * @return string
   */
  public function getLastName()
  {
    $last_name = $this->getDataProviderValue('passenger', TicketDataProviderPassenger::LAST_NAME_SHIBBOLETH);
    if(!$last_name)
    {
      $last_name = $this->getDataProviderValue('passenger', TicketDataProviderPassenger::LAST_NAME);
    }
    return $last_name;
  }

  /**
   * @return string
   */
  public function getBirthday()
  {
    $birthday = $this->getDataProviderValue('passenger', TicketDataProviderPassenger::BIRTHDAY_SHIBBOLETH);
    if(!$birthday)
    {
      $birthday = ($this->getDataProviderValue('passenger', TicketDataProviderPassenger::BIRTHDAY)) ? $this->getDataProviderValue('passenger', TicketDataProviderPassenger::BIRTHDAY) : $this->getDataProviderValue('customer', TicketDataProviderPassenger::BIRTHDAY);
    }
    return $birthday;
  }

}
