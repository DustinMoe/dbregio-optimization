<?php
class TicketDBRegioEuropass extends TicketDBRegioStandard
{
  const ID = 'db_regio_standard_europass';
  
  /**
   * sub product scopes
   */
  const SCOPE_EUROPASS = 's_europass';
  
  public static function register()
  {
    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent(new self(),'oepnv.ticket_types.register'));
  }

  public function getId()
  {
    return self::ID;
  }

  public function getName()
  {
    return "DB Regio Ticket Europass";
  }

  /**
   * init the dataproviders,
   *   add some product scopes
   * 
   * @param string[] $required_model_roles
   * @param $source_objects
   */
  protected function initDataProviders($required_model_roles, $source_objects)
  {
    parent::initDataProviders($required_model_roles, $source_objects);

    /** @var TicketDataProviderTickeosProduct $dp_product */
    $dp_product = $this->data_providers[TicketDataProvider::TICKEOS_PRODUCT];

    $dp_product->addSubProductScope(self::SCOPE_EUROPASS, oepnvDBRegioEnums::PRODUCT_TYPE_EUROPASS);
  }
  
  /**
   * returns the template-dependent data fields (data by a TicketDataProvider)
   * (array with replacer_names as keys and the value as value)
   * @return arr<field_name::str>, field_name can be a ticket role
   */
  public function getDataFields()
  {
    $ret = parent::getDataFields();
    $scope_name_internal = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::NAME_INTERNAL, null, self::SCOPE_EUROPASS);
    $name_internal = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::NAME_INTERNAL);
    if ($scope_name_internal == 'EUROPASS Mini' || $name_internal == 'TGO-Fahrradkarte')
    {
      $ret['person_number'] = '1';
    }
    else
    {
      $ret['person_number'] = '2';
    }

    $ret['product_name'] = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::NAME, null, self::SCOPE_EUROPASS);
    if (empty($ret['product_name']))
    {
      $ret['product_name'] = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::NAME);
    }

    return $ret;
  }
}
