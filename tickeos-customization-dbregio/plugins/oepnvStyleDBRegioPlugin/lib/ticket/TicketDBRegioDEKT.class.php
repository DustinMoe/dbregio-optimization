<?php
class TicketDBRegioDEKT extends TicketDBRegioStandard
{
  const ID = 'db_regio_dekt';

  const SCOPE_KINDER = 's_anzahl_kinder';

  public static function register()
  {
    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent(new self(),'oepnv.ticket_types.register'));
  }

  public function getId()
  {
    return self::ID;
  }

  public function getName()
  {
    return "DB Regio Ticket DEKT";
  }
  /**
   * init the dataproviders,
   *   add some product scopes
   *
   * @param string[] $required_model_roles
   * @param $source_objects
   */
  protected function initDataProviders($required_model_roles, $source_objects)
  {
    parent::initDataProviders($required_model_roles, $source_objects);

    /** @var TicketDataProviderTickeosProduct $dp_product */
    $dp_product = $this->data_providers[TicketDataProvider::TICKEOS_PRODUCT];

    $dp_product->addSubProductScope(self::SCOPE_KINDER, 'anzahl_kinder', null, self::SCOPE_STRECKE);
  }

  /**
   * returns the template-dependent data fields (data by a TicketDataProvider)
   * (array with replacer_names as keys and the value as value)
   * @return arr<field_name::str>, field_name can be a ticket role
   */
  public function getDataFields()
  {
    $ret = parent::getDataFields();

    $ret['product_info_text'] = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::INFO_TEXT);
    $ret['product_info_text_strecke'] = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::DESCRIPTION_SHORT, null, self::SCOPE_STRECKE);
    $ret['product_strecke'] = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PRINTTICKET_TEXT, null, self::SCOPE_STRECKE);
    $ret['product_strecke_name'] = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PRODUCT_NAME, null, self::SCOPE_STRECKE);

    $anz_kinder = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PRINTTICKET_TEXT, null, self::SCOPE_KINDER);
    if($anz_kinder != '0')
    {
      $ret['anz_kinder'] = 'Kinder: '.$anz_kinder;
    }
    else
    {
      $ret['anz_kinder'] = '';
    }

    $ret['product_name_ticket'] = $ret['product_name'];
    $ret['product_name'] .= ' '.$ret['product_strecke_name'];

    return $ret;
  }

  public function getTicketData()
  {
    parent::getTicketData();

    $this->ticket_data->setTariffZone($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PRODUCT_NAME, null, self::SCOPE_STRECKE));
    $this->ticket_data->setPassengerType1Name('Anzahl Kinder');
    $this->ticket_data->setPassengerType1Number($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PRINTTICKET_TEXT, null, self::SCOPE_KINDER));

    return $this->ticket_data;
  }
}
