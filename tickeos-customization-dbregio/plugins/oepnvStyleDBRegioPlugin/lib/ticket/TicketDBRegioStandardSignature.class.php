<?php
class TicketDBRegioStandardSignature extends TicketDBRegioStandard
{
  const ID = 'db_regio_standard_signature';

    public static function register()
    {
        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent(new self(),'oepnv.ticket_types.register'));
    }

    public function getId()
    {
        return self::ID;
    }

  public function getName()
  {
    return "DB Regio Ticket Standard Unterschrift";
  }
  
  /**
   * returns the template-dependent data fields (data by a TicketDataProvider)
   * (array with replacer_names as keys and the value as value)
   * @return arr<field_name::str>, field_name can be a ticket role
   */
  public function getDataFields()
  {
    $ret = parent::getDataFields();   
    
    $ret['product_info_text'] = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::INFO_TEXT);
	
	$code = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PRODUCT_CODE_BASE);
	if($code == 377){
		$ret['product_valid'] = '03.08.2013'.' - '.'04.08.2013';
	}else if($code != 375){
		$ret['product_valid'] = date('d.m.Y', strtotime($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_FROM_DATE))) .
                                ' - ' . date('d.m.Y', strtotime($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_TO_DATE)));
	}

    return $ret;
  }
}
