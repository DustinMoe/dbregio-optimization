<?php
class TicketDBRegioBernstein extends TicketDBRegioStandard
{
  const ID = 'db_regio_bernstein';
  
  public static function register()
  {
    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent(new self(),'oepnv.ticket_types.register'));
  }

  public function getId()
  {
    return self::ID;
  }

  public function getName()
  {
    return "DB Regio Ticket Bernstein";
  }

  
  /**
   * returns the template-dependent data fields (data by a TicketDataProvider)
   * (array with replacer_names as keys and the value as value)
   * @return arr<field_name::str>, field_name can be a ticket role
   */
  public function getDataFields()
  {
    $ret = parent::getDataFields();
    $ret['product_name'] .= ' ' . $ret['person_number'];
    $ret['coach_class'] = '2';

    if ($ret['person_number'] == 'Minigruppe')
    {
      $ret['age_group'] = '';
      $ret['person_number'] = 'max. 2 Erwachsene und 3 Kinder (6-14 Jahre)';
    }
    else
    {
      $ret['person_number'] == 'Erwachsener' ? $ret['age_group'] = 'Erw.:' : $ret['age_group'] = 'Kinder:';
      $ret['person_number'] = '1';
    }
      
    return $ret;
  }

 }
