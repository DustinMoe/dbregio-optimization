<?php
/**
 * DB Rail&Fly specific UIC918-3/RCT2 layout class
 */
class Uic918_3_autozug implements Uic918Interface
{
  const ID = 'Uic918_3_autozug';

  // RCT2 for international layouts,
  // requires composing the layout according definition in UIC 918-3
  protected $LAYOUT_STANDARD = 'RCT2';
  protected $LAYOUT_VERSION  = '01';

  protected $LAYOUT_RCT2 = array(
    'type'=>UicUtLayout::RECORD_TYPE,
    'fields'=>array(

      ### Zeile 0 ###
      array('role'=>'db',            'line'=>0,  'column'=>1, 'height'=>1, 'width'=>2),
      // Fahrausweisname (ticket_type), z.B. Fahrkarte, Einzelfahrt, Hin- und Rückfahrt, Zeitkarte, Pauschalticket
      array('role'=>'ticket_type',          'line'=>0,  'column'=>18, 'height'=>1, 'width'=>33, 'format'=>UicUtLayout::ITALIC_STRING_IDENTIFIER),
      // Nachname
      array('role'=>'last_name',            'line'=>0,  'column'=>52, 'height'=>1, 'width'=>9),
      // Vorname
      array('role'=>'first_name',           'line'=>0,  'column'=>62, 'height'=>1, 'width'=>9),

      ### Zeile 1 ###
      // Anzahl von Kundengruppe 1 (numerisch), z.B: 5
      array('role'=>'num_customer_group_1', 'line'=>1,  'column'=>52, 'height'=>1, 'width'=>2),
      // Kundengruppe 1, z.B. Erwachsener oder Person(en)
      array('role'=>'customer_group_1',     'line'=>1,  'column'=>55, 'height'=>1, 'width'=>16),

      ### Zeile 2 ###
	  array('role'=>'ticket_type2',          'line'=>1,  'column'=>18, 'height'=>1, 'width'=>33, 'format'=>UicUtLayout::ITALIC_STRING_IDENTIFIER),
      array('role'=>'civ',           		'line'=>2,  'column'=>1, 'height'=>1, 'width'=>3),
      array('role'=>'ten_eighty',          'line'=>2,  'column'=>5, 'height'=>1, 'width'=>4),
      // Fahrzeugart (train_type)
      array('role'=>'train_type',           'line'=>2,  'column'=>18, 'height'=>1, 'width'=>33),
      // Anzahl von Kundengruppe 2 (numerisch), z.B: 5
      array('role'=>'num_customer_group_2', 'line'=>2,  'column'=>52, 'height'=>1, 'width'=>2),
      // Kundengruppe 2, z.B. Erwachsener oder Person(en)
      array('role'=>'customer_group_2',     'line'=>2,  'column'=>55, 'height'=>1, 'width'=>16),

      ### Zeile 3 ###
      // Gültig von - Jahr
      array('role'=>'valid_from_year',      'line'=>3,  'column'=>1,  'height'=>1, 'width'=>4),
      // Tariff Zone
      array('role'=>'tariff_zone',          'line'=>3,  'column'=>18, 'height'=>1, 'width'=>33),
      // Geburtsdatum
      array('role'=>'birthday',             'line'=>3,  'column'=>52, 'height'=>1, 'width'=>10),

      ### Zeile 6 / Hinfahrt ###
      // Gültig von - Datum (TT.MM)
      array('role'=>'valid_from_date',      'line'=>6,  'column'=>1,  'height'=>1, 'width'=>5),
      // Gültig von - Zeit (HH.MM)
      array('role'=>'valid_from_time',      'line'=>6,  'column'=>7,  'height'=>1, 'width'=>5),
      // Von
      array('role'=>'from',                 'line'=>6,  'column'=>13, 'height'=>1, 'width'=>20),
      // Nach
      array('role'=>'to',                   'line'=>6,  'column'=>34, 'height'=>1, 'width'=>17),
      // Gültig bis - Datum
      array('role'=>'valid_till_date',      'line'=>6,  'column'=>52, 'height'=>1, 'width'=>5),
      // Gültig bis - Zeit
      array('role'=>'valid_till_time',      'line'=>6,  'column'=>58, 'height'=>1, 'width'=>5),
      // Klasse (train_class oder coach_class)
      array('role'=>'coach_class',          'line'=>6,  'column'=>66, 'height'=>1, 'width'=>5),

      ### Zeile 7 / Rueckfahrt ###
      // Gültig von - Datum (TT.MM)
      array('role'=>'valid_from_date_return', 'line'=>7,  'column'=>1,  'height'=>1, 'width'=>5,  'optional'=>true),
      // Gültig von - Zeit (HH.MM)
      array('role'=>'valid_from_time_return', 'line'=>7,  'column'=>7,  'height'=>1, 'width'=>5,  'optional'=>true),
      // Von
      array('role'=>'from_return',            'line'=>7,  'column'=>13, 'height'=>1, 'width'=>20, 'optional'=>true),
      // Nach
      array('role'=>'to_return',              'line'=>7,  'column'=>34, 'height'=>1, 'width'=>17, 'optional'=>true),
      // Gültig bis - Datum
      array('role'=>'valid_till_date_return', 'line'=>7,  'column'=>52, 'height'=>1, 'width'=>5,  'optional'=>true),
      // Gültig bis - Zeit
      array('role'=>'valid_till_time_return', 'line'=>7,  'column'=>58, 'height'=>1, 'width'=>5,  'optional'=>true),
      // Klasse (train_class oder coach_class)
      array('role'=>'coach_class_return',     'line'=>7,  'column'=>66, 'height'=>1, 'width'=>5,  'optional'=>true),

      ### Zeile 8++ / via ###

      array('role'=>'via',                  'line'=>8,  'column'=>1,  'height'=>1, 'width'=>70, 'optional'=>true),

      ### Zeile 8++ / Reservations ###
      // TODO:

      ### Zeile 10 ###
      array('role'=>'car_h',                  'line'=>10,  'column'=>1,  'height'=>1, 'width'=>8, 'optional'=>true),
      array('role'=>'car_h_value',            'line'=>10,  'column'=>10,  'height'=>1, 'width'=>3, 'optional'=>true),
      array('role'=>'car_h_place',            'line'=>10,  'column'=>14,  'height'=>1, 'width'=>5, 'optional'=>true),
      array('role'=>'car_h_place_value',      'line'=>10,  'column'=>20,  'height'=>1, 'width'=>23, 'optional'=>true),
      // Preisstufe (z.B: "Preisstufe 36" (ODEG))
      array('role'=>'tarif_out',              'line'=>10, 'column'=>52, 'height'=>1, 'width'=>19,'optional'=>true),

      ### Zeile 11###
      array('role'=>'car_r',                  'line'=>11,  'column'=>1,  'height'=>1, 'width'=>8, 'optional'=>true),
      array('role'=>'car_r_value',            'line'=>11,  'column'=>10,  'height'=>1, 'width'=>3, 'optional'=>true),
      array('role'=>'car_r_place',            'line'=>11,  'column'=>14,  'height'=>1, 'width'=>5, 'optional'=>true),
      array('role'=>'car_r_place_value',      'line'=>11,  'column'=>20,  'height'=>1, 'width'=>23, 'optional'=>true),
	  array('role'=>'tarif_ret',              'line'=>11,  'column'=>52,  'height'=>1, 'width'=>19,'optional'=>true),

      ### Zeile 12 ###
      // gültigkeitstext
      array('role'=>'validity_text',        'line'=>12, 'column'=>1,  'height'=>2, 'width'=>50, 'format'=>UicUtLayout::ITALIC_STRING_IDENTIFIER),
      array('role'=>'befoerderer',          'line'=>12,  'column'=>41,  'height'=>1, 'width'=>10, 'optional'=>true),

      ### Zeile 13 ###
      // Währung(seinheit)
      array('role'=>'currency',             'line'=>13, 'column'=>52, 'height'=>1, 'width'=>3),
      // Preis
      array('role'=>'price',                'line'=>13, 'column'=>56, 'height'=>1, 'width'=>15),

      ### Zeile 14 ###
      // ausweisdokument
      array('role'=>'verifying_document',   'line'=>14, 'column'=>1,  'height'=>1, 'width'=>25),
      // Zahlungsweise
      array('role'=>'payment_method',       'line'=>14, 'column'=>52, 'height'=>1, 'width'=>19),
      array('role'=>'befoerderer_value',    'line'=>14, 'column'=>41,  'height'=>1, 'width'=>4, 'optional'=>true)
    )
  );

  protected $db                  ='DB';
  protected $civ                 ='CIV';
  protected $ten_eighty          ='1080';
  protected $car_h               ='H: WAGEN';
  protected $car_h_value         ='';
  protected $car_h_place         ='PLATZ';
  protected $car_h_place_value   ='';
  protected $car_r               ='R: WAGEN';
  protected $car_r_value         ='';
  protected $car_r_place         ='PLATZ';
  protected $car_r_place_value   ='';
  protected $befoerderer         ='BEFORDERER';
  protected $befoerderer_value   ='';
  protected $tarif_out		     ='a';
  protected $tarif_ret		     ='b';

  // Zone 1: Tpe of document, CIV, responsible carrier in code, obliteration, stamp,validity
  protected $ticket_type          = ''; # str
  protected $ticket_type2          = ''; # str
  protected $product_name         = ''; # str
  protected $train_type           = ''; # str
  protected $tariff_zone          = ''; # str
  protected $birthday             = ''; # str

  // Zone 2: Names / first names of the travellers and the number of adults and children
  protected $first_name           = ''; # str
  protected $last_name            = ''; # str
  protected $customer_group_1     = ''; # str
  protected $num_customer_group_1 = 0;  # int
  protected $customer_group_2     = ''; # str
  protected $num_customer_group_2 = ''; # int

  // Zone 3: Travel distance, departure date and – time, arrival date and - time
  protected $valid_from           = ''; # format i.e. 15.12.2010 00:00
  protected $valid_from_year      = ''; # format i.e. 2010
  protected $valid_from_date      = ''; # format i.e. 15.12
  protected $valid_from_time      = ''; # format i.e. 00:00
  protected $valid_till           = ''; # format i.e. 15.12.2010 00:00
  protected $valid_till_date      = ''; # format i.e. 15.12
  protected $valid_till_time      = ''; # format i.e. 00:00

  protected $valid_from_return      = ''; # format i.e. 15.12.2010 00:00
  protected $valid_from_date_return = ''; # format i.e. 15.12
  protected $valid_from_time_return = ''; # format i.e. 00:00
  protected $valid_till_return      = ''; # format i.e. 15.12.2010 00:00
  protected $valid_till_date_return = ''; # format i.e. 15.12
  protected $valid_till_time_return = ''; # format i.e. 00:00

  protected $from                 = ''; # str
  protected $to                   = ''; # str
  protected $from_return          = ''; # str
  protected $to_return            = ''; # str

  // Zone 4: Class
  protected $coach_class          = '';
  protected $coach_class_return   = '';

  // Zone 5: Train, wagon, reserved seat (if not with compulsory reservation)
  protected $via                  = ''; # str

  // Zone 6: Used rates, used commercial conditions (from 1 till n carriers)
  protected $validity_text        = ''; # str/text
  protected $verifying_document   = ''; # str
  protected $price_level          = ''; # str

  // Zone 7: Currency (ISO code out of GIV) and total amount for each ticket
  protected $currency             = ''; # str
  protected $price                = 0;  # int
  protected $payment_method       = ''; # str

  public static function getId()
  {
    return self::ID;
  }

  public static function register()
  {
    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent(new self(),'oepnv.uic_layout.register'));
  }

  /**
   * returns the layout definition array
   * @param bool $with_labels (optional, default false)
   * @return array
   */
  public function getLayoutDefinition($with_labels=false)
  {
    if(!$with_labels)
    {
      return array($this->LAYOUT_U_TLAY);
    }

    $ret = array();
    $labels = $this->getLabels();
    foreach($this->LAYOUT_U_TLAY['fields'] as $a_field)
    {
      $role = $a_field['role'];
      $a_field['label'] = $labels[$role];
      if(isset($a_field['format'])) unset($a_field['format']);
      $ret[] = $a_field;
    }
    return array(array('type'=>$this->LAYOUT_U_TLAY['type'], 'fields'=>$ret));
  }

  /**
   * returns the labels
   * @return array<role:label>
   */
  public function getLabels()
  {
    $i18n = sfContext::getInstance()->getI18N();
    return array(
      'ticket_type'            => $i18n->__('Fahrausweisname',null,'ticket'),
      'last_name'              => $i18n->__('Nachname',null,'ticket'),
      'first_name'             => $i18n->__('Vorname',null,'ticket'),
      'product_name'           => $i18n->__('Produktname',null,'ticket'),
      'num_customer_group_1'   => $i18n->__('Anzahl von Kundengruppe 1',null,'ticket'),
      'customer_group_1'       => $i18n->__('Kundengruppe 1',null,'ticket'),
      'num_customer_group_2'   => $i18n->__('Anzahl von Kundengruppe 2',null,'ticket'),
      'customer_group_2'       => $i18n->__('Kundengruppe 2',null,'ticket'),
      'train_type'             => $i18n->__('Fahrzeugart',null,'ticket'),
      'valid_from_year'        => $i18n->__('Gültig von - Jahr',null,'ticket'),
      'tariff_zone'            => $i18n->__('Tarifzone',null,'ticket'),
      'birthday'               => $i18n->__('Geburtsdatum',null,'ticket'),
      'valid_from_date'        => $i18n->__('Gültig von - Datum',null,'ticket'),
      'valid_from_time'        => $i18n->__('Gültig von - Zeit',null,'ticket'),
      'valid_till_date'        => $i18n->__('Gültig bis - Datum',null,'ticket'),
      'valid_till_time'        => $i18n->__('Gültig bis - Zeit',null,'ticket'),
      'valid_from_date_return' => $i18n->__('Gültig von - Datum (Rückfahrt)',null,'ticket'),
      'valid_from_time_return' => $i18n->__('Gültig von - Zeit (Rückfahrt)',null,'ticket'),
      'valid_till_date_return' => $i18n->__('Gültig bis - Datum (Rückfahrt)',null,'ticket'),
      'valid_till_time_return' => $i18n->__('Gültig bis - Zeit (Rückfahrt)',null,'ticket'),
      'from'                   => $i18n->__('Von - Ort',null,'ticket'),
      'to'                     => $i18n->__('Nach - Ort',null,'ticket'),
      'from_return'            => $i18n->__('Von - Ort',null,'ticket'),
      'to_return'              => $i18n->__('Nach - Ort',null,'ticket'),
      'coach_class'            => $i18n->__('Klasse',null,'ticket'),
      'coach_class_return'     => $i18n->__('Klasse (Rückfahrt)',null,'ticket'),
      'via'                    => $i18n->__('Via',null,'ticket'),
      'validity_text'          => $i18n->__('Gültigkeitstext',null,'ticket'),
      'currency'               => $i18n->__('Währung',null,'ticket'),
      'price'                  => $i18n->__('Preis',null,'ticket'),
      'verifying_document'     => $i18n->__('Kontrollmedium',null,'ticket'),
      'price_level'            => $i18n->__('Preisstufe',null,'ticket'),
      'payment_method'         => $i18n->__('Zahlungsweise',null,'ticket')
    );
  }

  public function setTarifOut($v)
  {
    $this->tarif_out = $v;
  }
  
  public function setTarifRet($v)
  {
    $this->tarif_ret = $v;
  }
  
  public function setTicketType($v)
  {
    $this->ticket_type = $v;
  }

  public function setProductName($v)
  {
    $this->product_name = $v;
  }

  public function setTrainType($v)
  {
    $this->train_type = $v;
  }

  public function setTariffZone($v)
  {
    $this->tariff_zone = $v;
  }

  public function setFirstName($v)
  {
    $this->first_name = $v;
  }

  public function setLastName($v)
  {
    $this->last_name = $v;
  }

  /**
   * set the birthday (DD.MM.YYYY)
   * @param string $v
   */
  public function setBirthday($v)
  {
    if(strlen($v)>0 && strlen($v)!=10)
    {
      throw new Exception('Uic918_3#setBirthday(): invalid format ('.$v.'), must be DD.MM.YYYY');
    }
    $this->birthday = $v;
  }

  public function setCustomerGroup1($v)
  {
    $this->customer_group_1 = $v;
  }

  public function setNumCustomerGroup1($v)
  {
    $this->num_customer_group_1 = $v;
  }

  public function setCustomerGroup2($v)
  {
    $this->customer_group_2 = $v;
  }

  public function setNumCustomerGroup2($v)
  {
    $this->num_customer_group_2 = $v;
  }

  public function setValidFrom($v)
  {
    $this->valid_from = $v;
    $this->valid_from_date = substr($v, 0, 5);
    $this->valid_from_year = substr($v, 6, 4);
    $this->valid_from_time = str_replace(":", ".", substr($v, 11, 5));
    if($this->valid_from_time=='')
    {
      $this->valid_from_time='00.00';
    }
  }

  public function setValidTill($v)
  {
    $this->valid_till = $v;
    $this->valid_till_date = substr($v, 0, 5);
    $this->valid_till_time = str_replace(":", ".", substr($v, 11, 5));
    if($this->valid_till_time=='')
    {
      $this->valid_till_time='23.59';
    }
  }

  public function setValidFromReturn($v)
  {
    $this->valid_from_return = $v;
    $this->valid_from_date_return = substr($v, 0, 5);
    $this->valid_from_time_return = str_replace(":", ".", substr($v, 11, 5));
    if($v!=null && $this->valid_from_time_return=='')
    {
      $this->valid_from_time_return='00.00';
    }
  }

  public function setValidTillReturn($v)
  {
    $this->valid_till_return = $v;
    $this->valid_till_date_return = substr($v, 0, 5);
    $this->valid_till_time_return = str_replace(":", ".", substr($v, 11, 5));
    if($v!=null && $this->valid_till_time_return=='')
    {
      $this->valid_till_time_return='23.59';
    }
  }

  public function setFrom($v)
  {
    $this->from = $v;
  }

  public function setTo($v)
  {
    $this->to = $v;
  }

  public function setFromReturn($v)
  {
    $this->from_return = $v;
  }

  public function setToReturn($v)
  {
    $this->to_return = $v;
  }

  public function setVia($v)
  {
    if($v)
    {
      $this->via = 'VIA '.$v;
    }
  }

  /**
   * set the coach class
   * (1 or 2, all other values will be ignored)
   * @param string $v
   */
  public function setCoachClass($v)
  {
    if(''.$v=='1' || ''.$v=='2')
    {
      $this->coach_class = $v;
    }
  }

  /**
   * set the coach class for return journey
   * (1 or 2, all other values will be ignored)
   * @param string $v
   */
  public function setCouchClassReturn($v)
  {
    if(''.$v=='1' || ''.$v=='2')
    {
      $this->coach_class_return = $v;
    }
  }

  public function setValidityText($v)
  {
    $this->validity_text = $v;
  }

  public function setCurrency($v)
  {
    $this->currency = $v;
  }

  public function setPrice($v)
  {
    $this->price = $v;
  }

  public function setPaymentMethod($v)
  {
    $this->payment_method = $v;
  }

  public function setVerifyingDocument($v)
  {
    $this->verifying_document = $v;
  }

  public function setPriceLevel($v)
  {
    $this->price_level = $v;
  }
  
  public function setCarHValue($v)
  {
	$this->car_h_value=$v;
  }
  
  public function setCarRValue($v)
  {
	$this->car_r_value=$v;
  }
  
  public function setCarHPlaceValue($v)
  {
	$this->car_h_place_value=$v;
  }
  
  public function setCarRPlaceValue($v)
  {
	$this->car_r_place_value=$v;
  }
  
  public function setBefoerdererValue($v)
  {
	$this->befoerderer_value=$v;
  }
  
  public function setTicketType2($v)
  {
	$this->ticket_type2=$v;
  }
  
  public function createUicUtLayout(UicUtLayout $record = null)
  {
    if($record === null)
    {
      $record = new UicUtLayout('01', 'RCT2');
    }
    
    $record->redefineMaxLine(19);
    $record->redefineMaxColumn(77);
    
    foreach($this->LAYOUT_RCT2['fields'] as $afield)
    {
      $role = $afield['role'];
      $record->addField($afield['line'], $afield['column'], substr($this->$role, 0, $afield['width']), $afield['height'], $afield['width'], isset($afield['format'])?$afield['format']:'');
    }

    return $record;
  }

}
