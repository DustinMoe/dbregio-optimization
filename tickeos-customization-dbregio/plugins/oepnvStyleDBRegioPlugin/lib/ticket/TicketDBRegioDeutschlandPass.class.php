<?php
class TicketDBRegioDeutschlandPass extends TicketDBRegioGRP
{
  const ID = 'db_regio_deutschlandpass';
  
  public static function register()
  {
    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent(new self(),'oepnv.ticket_types.register'));
  }

  public function getId()
  {
    return self::ID;
  }

  public function getName()
  {
    return "DB Regio DeutschlandPass";
  }
  
  /**
   * returns the template-dependent data fields (data by a TicketDataProvider)
   * (array with replacer_names as keys and the value as value)
   * @return arr<field_name::str>, field_name can be a ticket role
   */
  public function getDataFields()
  {
    $ret = parent::getDataFields();

    //$ret['product_valid'] = date('d.m.Y', strtotime($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_FROM_DATE)));
    $ret['product_name'] .= ': ' . $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::EXTRACTED_VAR, 'option_ticketart');
    if (strpos($ret['product_name'], '2 Erwachsene') != false)
      $ret['person_number'] = 2;
    else
      $ret['person_number'] = 1;
    $ret['birthday'] = $this->getDataProviderValue('passenger', TicketDataProviderPassenger::BIRTHDAY);

//    $birthday = new DateTime(date('Y-m-d' ,strtotime($this->getDataProviderValue('customer', TicketDataProviderCustomer::BIRTHDAY))));
    $birthday = new DateTime(date('Y-m-d' ,strtotime($this->getDataProviderValue('passenger', TicketDataProviderPassenger::BIRTHDAY))));
    $date = new DateTime(date('Y-m-d', strtotime($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_FROM_DATE))));
    $ret['age'] = $date->diff($birthday)->y;
FB::log($ret);
    return $ret;
  }

  /**
   * returns the data for UIC918
   *
   * @return UicLayoutContainer Data in UIC918 structure (head, body)
   */
  public function getTicketUic918()
  {
    return $this->getTicketUic918_3Default();
  }
}
