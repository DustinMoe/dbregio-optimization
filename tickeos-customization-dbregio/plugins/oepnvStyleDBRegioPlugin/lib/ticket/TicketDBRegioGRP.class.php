<?php

class TicketDBRegioGRP extends TicketStandard
{
  const ID = 'db_regio_grp';


//  const SCOPE_FAHROPTIONEN = 's_fahroptionen';
//  const SCOPE_GUELTIGKEIT = 's_gueltigkeit';
//  const SCOPE_TICKETART = 's_ticketart';
//  const SCOPE_TICKETS = 's_tickets';

  const SCOPE_TARIFGRUPPE = 's_tarifgruppe';

  public static function register()
  {
    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent(new self(), 'oepnv.ticket_types.register'));
  }

  public function getId()
  {
    return self::ID;
  }

  public function getName()
  {
    return "DB Regio Ticket German Rail Pass";
  }

  protected function initDataProviders($required_model_roles, $source_objects)
  {
    parent::initDataProviders($required_model_roles, $source_objects);

    /** @var TicketDataProviderTickeosProduct $dp_product */
    $dp_product = $this->data_providers[TicketDataProvider::TICKEOS_PRODUCT];
    $dp_product->addSubProductScope(self::SCOPE_TARIFGRUPPE, oepnvDBRegioEnums::PRODUCT_TYPE_TICKETART, null, self::SCOPE_TARIFGRUPPE);
  }

  /**
   * returns the template-dependent data fields (data by a TicketDataProvider)
   * (array with replacer_names as keys and the value as value)
   * @return arr<field_name::str>, field_name can be a ticket role
   */
  public function getDataFields()
  {
    $is_elv = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PAYMENT_METHOD_IS_ELV);
    $product_payment_infotext = $is_elv ? 'Ihr Konto' : 'Ihre Kreditkarte';
    $product_payment_infotext .= ' wurde mit dem oben angegebenen Betrag belastet.';

    //Gültigkeit 5 oder 10 Tage
    $product_validity = round(abs((strtotime($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_TO_DATE)) - strtotime($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_FROM_DATE))) / 60 / 60 / 24));
    $product_validity .= ' days';

    //Adult, Youth oder Twin
    $ticketart = strtoupper($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::EXTRACTED_VAR, 'option_tarifgruppe_printticket_text'));
    $ticketart_ref_id = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::EXTRACTED_VAR, 'option_tarifgruppe_reference_id');

    //Anzahl Personen
    if ($ticketart_ref_id == 'grp_twin') {
      $anz_personen = 2;
    } else {
      $anz_personen = 1;
    }

    // 1. oder 2. Klasse
    $product_class = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::EXTRACTED_VAR, 'option_tarifgruppe_option_class_printticket_text');
    if (!$product_class) {
      $product_class = 2;
    }

    // Preisberechnung (ohne Berücksichtigung Versand weil Printticket)
    $product_price = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PRICE);
    $product_vat_value = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VAT_VALUE);
    $order_ppsum = $this->getDataProviderValue(TicketDataProvider::ORDER, TicketDataProviderOrder::PPSUM);

    $product_price_19 = '0,00';
    $product_price_7 = '0,00';
    $order_ppsum_19 = '0,00';
    $order_ppsum_7 = '0,00';
    if (substr($product_vat_value, 0, 2) == '19') {
      $product_price_19 = $product_price;
      $order_ppsum_19 = $product_price;
    } else {
      $product_price_7 = $product_price;
      $order_ppsum_7 = $product_price;
    }

    $order_ppsum_tax_19 = $this->getDataProviderValue(TicketDataProvider::ORDER, TicketDataProviderOrder::PPSUM_TAX_19);
    $order_ppsum_tax_7 = $this->getDataProviderValue(TicketDataProvider::ORDER, TicketDataProviderOrder::PPSUM_TAX_7);

    // product_tax_19 / _7 erstmal gleicher wert wie order_ppsum_tax_19 / _7
    $product_tax_19 = $order_ppsum_tax_19;
    $product_tax_7 = $order_ppsum_tax_7;
    
    $id_card = $this->getDataProviderValue('passenger', TicketDataProviderPassenger::IDENTITY_CARD_NAME);

    $ret = array(
      'product_serial_number' => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::SERIAL_NUMBER),
      'product_name' => strtoupper($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::NAME)),

      'product_payment_method' => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PAYMENT_METHOD),
      'product_payment_infotext' => $product_payment_infotext,
      'person_number' => $anz_personen,
      'ticketart' => $ticketart,
      'product_validity' => $product_validity,
      'passport_number' => $this->getDataProviderValue(TicketDataProvider::CUSTOMER, TicketDataProviderCustomer::EXTRACT_PERSONALIZATION_VAR, 'reisepassnummer'),

      'class_info_text' => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::EXTRACTED_VAR, 'option_tarifgruppe_option_class'),

      'product_price' => $product_price,
      'product_price_19' => $product_price_19,
      'product_price_7' => $product_price_7,
      'product_tax_19' => $product_tax_19,
      'product_tax_7' => $product_tax_7,

      'product_created_date_string' => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::CREATED_DATE_STRING),
      'product_info_text' => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::INFO_TEXT),
      'product_valid' => date('d.m.Y', strtotime($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_FROM_DATE))) . ' - ' . date('d.m.Y', strtotime($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_TO_DATE))),
      'valid_from' => date('d.m.Y', strtotime($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_FROM_DATE))),
      'valid_to' => date('d.m.Y', strtotime($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_TO_DATE))),
      'product_valid_outward' => isset($outward_date)?$outward_date:'',
      'product_valid_return' => isset($return_date)?$return_date:'',
      'return_text' => isset($return_text)?$return_text:'',
      'coach_class' => $product_class,
      'train_type' => 'Fahrkarte',
      'ticket_type' => 'Fahrkarte',

      'customer_first_and_last_name' => $this->getDataProviderValue('passenger', TicketDataProviderPassenger::FIRST_AND_LAST_NAME),
      'customer_birthday' => $this->getDataProviderValue('passenger', TicketDataProviderPassenger::BIRTHDAY),
      'customer_identity_card_name'  => isset($id_card)? $id_card : '',
      'customer_identity_card_number' => $this->getDataProviderValue('passenger', TicketDataProviderPassenger::IDENTITY_CARD_NUMBER),

      'order_serial_number' => $this->getDataProviderValue(TicketDataProvider::ORDER, TicketDataProviderOrder::SERIAL_NUMBER),
      'order_ppsum' => $product_price,

      'order_ppsum_19' => $order_ppsum_19,
      'order_ppsum_7' => $order_ppsum_7,
      'order_ppsum_tax_19' => $order_ppsum_tax_19,
      'order_ppsum_tax_7' => $order_ppsum_tax_7,

      'product_vat' => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VAT),
      'product_vat_price' => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VAT_PRICE),
      'created_at' => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::CREATED_STRING),

      'do_not_fold_text' => $this->getDoNotFoldBarcodeText(),

      'twin_label' => '',
      'twin_name' => ''

    );

    //twin-pass
    if ($ticketart_ref_id == 'grp_twin')
    {
      if ($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PRODUCT_NAME)=='German Rail Pass Promotion'){
        $ret['product_name'] = 'German Rail Twin Pass Promotion';
      }
      else {
        $ret['product_name'] = 'German Rail Twin Pass';
      }
      $ret['twin_label'] = 'Mitreisende:';
      $twin_name = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PERSONALIZATION_PROPERTY, 'rail_twin_name', self::SCOPE_TARIFGRUPPE);
      if (is_object($twin_name))
        $ret['twin_name']=$twin_name->getValue();
    }

    return $ret;
  }

  /**
   * returns ticket fields visible by qr_test app
   * @return arr<role:arr<name:value>>
   */
  public function getQrTestData()
  {
    $ret = parent::getQrTestData();

    //Anzahl Personen
    $ticketart_ref_id = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::EXTRACTED_VAR, 'option_tarifgruppe_reference_id');
    if ($ticketart_ref_id == 'grp_twin') {
      $anz_personen = 2;
    } else {
      $anz_personen = 1;
    }

    // 1. oder 2. Klasse
    $product_class = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::EXTRACTED_VAR, 'option_tarifgruppe_option_class_printticket_text');
    if (!$product_class) {
      $product_class = 2;
    }

    $ret['age_group'] = array(
      'name' => 'Fahrgast-Typ',
      'value' => 'Person(en)'
    );
    $ret['person_number'] = array(
      'name' => 'Fahrgast Anzahl',
      'value' => $anz_personen
    );

    $ret['coach_class'] = array(
      'name' => 'Klasse',
      'value' => $product_class
    );

    return $ret;
  }


  /**
   * @return BaseTicketData|null|TicketData
   */
  public function getTicketData()
  {
    if ($this->ticket_data != null) {
      return $this->ticket_data;
    }

    parent::getTicketData(new RelationTicketData());

    $ticket_data_fields = $this->getDataFields();
    $this->ticket_data->setPassengerType1Number($ticket_data_fields['person_number']);
//    var_dump($this->ticket_data->getPassengerType1Number());
//    die();

    return $this->ticket_data;
  }

  /**
   * returns the data for UIC918
   *
   * @return UicLayoutContainer Data in UIC918 structure (head, body)
   */
  public function getTicketUic918()
  {
    return $this->getTicketUic918_3Default();
  }

//  protected function initDataProviders($required_model_roles, $source_objects)
//  {
//    parent::initDataProviders($required_model_roles, $source_objects);
//
//    /** @var TicketDataProviderTickeosProduct $dp_product */
//    $dp_product = $this->data_providers[TicketDataProvider::TICKEOS_PRODUCT];
//
//    $x = $dp_product->addSubProductScope(self::SCOPE_TICKETS, oepnvDBRegioEnums::PRODUCT_TYPE_TICKETS);
//    FB::log($x, '5');
//    $x = $dp_product->addSubProductScope(self::SCOPE_TICKETART, oepnvDBRegioEnums::PRODUCT_TYPE_TICKETART, 0, self::SCOPE_TICKETS);
//    FB::log($x, '6');
//    $x = $dp_product->addSubProductScope(self::SCOPE_FAHROPTIONEN, oepnvDBRegioEnums::PRODUCT_TYPE_FAHROPTIONEN, 0, self::SCOPE_TICKETART);
//    FB::log($x, '7');
//    $x = $dp_product->addSubProductScope(self::SCOPE_GUELTIGKEIT, oepnvDBRegioEnums::PRODUCT_TYPE_GUELTIGKEIT, 0, self::SCOPE_FAHROPTIONEN);
//    FB::log($x, '8');
//    $x = $dp_product->addSubProductScope(self::SCOPE_GUELTIGKEIT, oepnvDBRegioEnums::PRODUCT_TYPE_GUELTIGKEIT, 0, self::SCOPE_TICKETART);
//    FB::log($x, '9');
//
//  }

}