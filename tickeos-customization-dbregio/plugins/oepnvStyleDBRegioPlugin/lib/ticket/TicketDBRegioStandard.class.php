<?php
class TicketDBRegioStandard extends TicketStandard
{
  const ID = 'db_regio_standard';

  /**
   * sub product scopes
   */
  const SCOPE_AIRPORTEXPRESSTICKET = 's_airportexpressticket';
  const SCOPE_FAHROPTIONEN = 's_fahroptionen';
  const SCOPE_RICHTUNG = 's_richtung';
  const SCOPE_RICHTUNG2 = 's_richtung_ret';
  const SCOPE_STRECKE = 's_strecke';
  const SCOPE_PERSONEN = 's_personenzahl';

  public static function register()
  {
    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent(new self(),'oepnv.ticket_types.register'));
  }

  public function getId()
  {
    return self::ID;
  }

  public function getName()
  {
    return "DB Regio Ticket Standard";
  }

  /**
   * init the dataproviders,
   *   add some product scopes
   *
   * @param string[] $required_model_roles
   * @param $source_objects
   */
  protected function initDataProviders($required_model_roles, $source_objects)
  {
    parent::initDataProviders($required_model_roles, $source_objects);

    /** @var TicketDataProviderTickeosProduct $dp_product */
    $dp_product = $this->data_providers[TicketDataProvider::TICKEOS_PRODUCT];

    $dp_product->addSubProductScope(self::SCOPE_AIRPORTEXPRESSTICKET, oepnvDBRegioEnums::PRODUCT_TYPE_AIRPORTEXPRESSTICKET);
    $dp_product->addSubProductScope(self::SCOPE_FAHROPTIONEN, oepnvDBRegioEnums::PRODUCT_TYPE_FAHROPTIONEN, null, self::SCOPE_AIRPORTEXPRESSTICKET);
    $dp_product->addSubProductScope(self::SCOPE_RICHTUNG, oepnvDBRegioEnums::PRODUCT_TYPE_RICHTUNG, null, self::SCOPE_FAHROPTIONEN);
    $dp_product->addSubProductScope(self::SCOPE_RICHTUNG2, oepnvDBRegioEnums::PRODUCT_TYPE_RICHTUNG, 1, self::SCOPE_FAHROPTIONEN);
    $dp_product->addSubProductScope(self::SCOPE_STRECKE, oepnvDBRegioEnums::PRODUCT_TYPE_STRECKE, null, self::SCOPE_STRECKE);
    $dp_product->addSubProductScope(self::SCOPE_PERSONEN, 'personenzahl', null, self::SCOPE_PERSONEN);
  }

  /**
   * returns the template-dependent data fields (data by a TicketDataProvider)
   * (array with replacer_names as keys and the value as value)
   * @return arr<field_name::str>, field_name can be a ticket role
   */
  public function getDataFields()
  {

    $is_elv = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PAYMENT_METHOD_IS_ELV);
    $product_payment_infotext = $is_elv ? 'Ihr Konto' : 'Ihre Kreditkarte';
    $product_payment_infotext .= ' wurde mit dem oben angegebenen Betrag belastet.';

    // Preisberechnung (ohne Berücksichtigung Versand weil Printticket)
    $product_price     = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PRICE);
    $product_vat_value = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VAT_VALUE);
    $order_ppsum       = $this->getDataProviderValue(TicketDataProvider::ORDER, TicketDataProviderOrder::PPSUM);

    $product_price_19 = '0,00';
    $product_price_7  = '0,00';
    $order_ppsum_19   = '0,00';
    $order_ppsum_7    = '0,00';
    if(substr($product_vat_value, 0, 2)=='19')
    {
      $product_price_19 = $product_price;
      $order_ppsum_19   = $product_price;
    }
    else
    {
      $product_price_7 = $product_price;
      $order_ppsum_7   = $product_price;
    }

    $order_ppsum_tax_19 = $this->getDataProviderValue(TicketDataProvider::ORDER, TicketDataProviderOrder::PPSUM_TAX_19);
    $order_ppsum_tax_7  = $this->getDataProviderValue(TicketDataProvider::ORDER, TicketDataProviderOrder::PPSUM_TAX_7);

    // product_tax_19 / _7 erstmal gleicher wert wie order_ppsum_tax_19 / _7
    $product_tax_19 = $order_ppsum_tax_19;
    $product_tax_7  = $order_ppsum_tax_7;

// beispiele für scopes:
    //$x = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::NAME, null,  self::SCOPE_AIRPORTEXPRESSTICKET);
//FB::log($x); # = z.B: "Erwachsene 2"

    //$x = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::NAME, null,  self::SCOPE_RICHTUNG);
//FB::log($x); # = z.B. "Ingolstadt - Flughafen München"

    $date1=$this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_FROM_DATE, null,  self::SCOPE_RICHTUNG);
    $date2=$this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_FROM_DATE, null,  self::SCOPE_RICHTUNG2);
    $return_text=utf8_encode("Rückfahrt:");
    if ($date1!==null && $date2!==null)
    {
      if ($date1<=$date2)
      {
        $outward_date=date('d.m.Y',strtotime($date1));
        $return_date=date('d.m.Y',strtotime($date2));
      }
      else
      {
        $outward_date=date('d.m.Y',strtotime($date2));
        $return_date=date('d.m.Y',strtotime($date1));
      }
    }
    elseif ($date1!==null)
    {
      $outward_date=date('d.m.Y',strtotime($date1));
      $return_date=null;
      $return_text="";
    }
    elseif ($date2!==null)
    {
      $outward_date=date('d.m.Y',strtotime($date2));
      $return_date=null;
      $return_text="";
    }
    else
    {
      $outward_date=null;
      $return_date=null;
      $return_text="";
    }

    $id_card = $this->getDataProviderValue('passenger', TicketDataProviderPassenger::IDENTITY_CARD_NAME);

    $ret = array(
      'product_direction'            => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::NAME, null, self::SCOPE_RICHTUNG),
      'product_direction_2'          => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::NAME, null, self::SCOPE_RICHTUNG2),
      'product_serial_number'        => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::SERIAL_NUMBER),
      'product_name'                 => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::NAME),

      'product_payment_method'       => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PAYMENT_METHOD),
      'product_payment_infotext'     => $product_payment_infotext,

      'product_price'                => $product_price,
      'product_price_19'             => $product_price_19,
      'product_price_7'              => $product_price_7,
      'product_tax_19'               => $product_tax_19,
      'product_tax_7'                => $product_tax_7,

      'product_created_date_string'  => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::CREATED_DATE_STRING),
      'product_info_text'            => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::INFO_TEXT),
	    'product_valid' 				       => date('d.m.Y', strtotime($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_FROM_DATE))),
      'product_valid_outward' 			 => $outward_date,
      'product_valid_return' 			   => $return_date,
      'return_text'                  => $return_text,
      'coach_class'                  => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::TARIFF_COACH_CLASS_NUMERIC),
      'train_type'                   => 'Fahrkarte',
      'ticket_type'                  => 'Fahrkarte',


      'customer_first_and_last_name' => $this->getDataProviderValue('passenger', TicketDataProviderPassenger::FIRST_AND_LAST_NAME),
      'customer_birthday'            => $this->getDataProviderValue('passenger', TicketDataProviderPassenger::BIRTHDAY),
      'customer_identity_card_name'  => isset($id_card)? $id_card : '',
      'customer_identity_card_number'  => $this->getDataProviderValue('passenger', TicketDataProviderPassenger::IDENTITY_CARD_NUMBER),

      'order_serial_number'          => $this->getDataProviderValue(TicketDataProvider::ORDER, TicketDataProviderOrder::SERIAL_NUMBER),
      'order_ppsum'                  => $product_price,

      'order_ppsum_19'               => $order_ppsum_19,
      'order_ppsum_7'                => $order_ppsum_7,
      'order_ppsum_tax_19'           => $order_ppsum_tax_19,
      'order_ppsum_tax_7'            => $order_ppsum_tax_7,

      'product_vat'                  => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VAT),
      'product_vat_price'            => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VAT_PRICE),
      'created_at'                   => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::CREATED_STRING),

      'do_not_fold_text'             => $this->getDoNotFoldBarcodeText()

    );

    $person_number = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::EXTRACTED_VAR, 'option_anzahl_personen');
    $person_number2 = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::EXTRACTED_VAR, 'option_tarifgruppe_option_anzahl_personen');

    if ($person_number)
    {
      $ret['person_number'] = $person_number;
    }
    elseif($person_number2)
    {
      $ret['person_number'] = $person_number2;
    }
    else
    {
      $ret['person_number'] = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PERSON_NUMBER);
    }

    //Mitfahrer
    $persons_names = $this->getDataProviderValue(TicketDataProvider::CUSTOMER, TicketDataProviderCustomer::EXTRACT_PERSONALIZATION_VARS);

    if ($ret['person_number'] > 1 && $persons_names)
    {
      $ret['person_label'] = 'Mitreisende:';
    }
    else
    {
      $ret['person_label'] = '';
    }

    for ($i = 2; $i <= 5; $i++)
    {
      if ($i <= ($ret['person_number']) && $persons_names)
      {
        $ret['person_' . $i] = $i-1 . '. ' . $persons_names['personalization_mitreisender' . $i];
      }
      else
      {
        $ret['person_' . $i] = '';
      }
    }

    $ret['person_number2'] = '';
    $code = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PRODUCT_CODE_BASE);
    if($code == 128)
    {
      $ret['age_group'] = 'Fahrräder:';
      $ret['person_number'] = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::EXTRACTED_VAR, 'option_anzahl_fahrraeder');
    }
    else if ($code == 373)
    {
      $ret['age_group'] = 'Person(en):';
      $ret['person_number'] = '1 Schüler bis einschl. 15 Jahren';
      $ret['person_number2'] = 'bzw Vollzeit-Schüler bis 22 J mit Nachweis';
    }
    else
    {
      $ret['age_group'] = 'Person(en):';
    }

    $validity_text = self::splitTextToLines($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::INFO_TEXT), 4, 75, true);

    foreach($validity_text as $k=>$v)
    {
      $ret['product_validity_text_0'.($k+1)] = $v;
    }
    return $ret;
  }

  /**
   * returns ticket fields visible by qr_test app
   * @return arr<role:arr<name:value>>
   */
  public function getQrTestData()
  {
    $ret = parent::getQrTestData();

    $ret['age_group'] = array(
      'name'  => 'Fahrgast-Typ',
      'value' => 'Person(en)'
    );
    $ret['person_number'] = array(
      'name'  => 'Fahrgast Anzahl',
      'value' => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PERSON_NUMBER)
    );

    $ret['coach_class'] = array(
      'name'  => 'Klasse',
      'value' => $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::TARIFF_COACH_CLASS_NUMERIC)
    );

    return $ret;
  }

  /**
   * @return BaseTicketData|null|TicketData
   */
  public function getTicketData()
  {
    if($this->ticket_data!=null)
    {
      return $this->ticket_data;
    }

    parent::getTicketData(new RelationTicketData());

    $person_number = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::EXTRACTED_VAR, 'option_anzahl_personen');
    $person_number2 = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::EXTRACTED_VAR, 'option_tarifgruppe_option_anzahl_personen');

    $this->ticket_data->setPassengerType1Number($person_number);
    $this->ticket_data->setPassengerType2Number($person_number2);

    // Hin und Rückfahrt Gültigkeiten
    /** @var OrderProductLocation $from */
    $date1=$this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_FROM_DATE, null,  self::SCOPE_RICHTUNG);
    $date2=$this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_FROM_DATE, null,  self::SCOPE_RICHTUNG2);
    $date1_til=$this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_TO_DATE, null,  self::SCOPE_RICHTUNG);
    $date2_til=$this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_TO_DATE, null,  self::SCOPE_RICHTUNG2);
    $return_text=utf8_encode("Rückfahrt:");
    if ($date1!==null && $date2!==null)
    {
      if ($date1<=$date2)
      {

        $this->ticket_data->setValidFrom(date('d.m.Y',strtotime($date1)));
        $this->ticket_data->setValidFromReturn(date('d.m.Y',strtotime($date2)));
        $this->ticket_data->setValidTill(date('d.m.Y',strtotime($date1_til)));
        $this->ticket_data->setValidTillReturn(date('d.m.Y',strtotime($date2_til)));
      }
      else
      {
        $this->ticket_data->setValidFrom(date('d.m.Y',strtotime($date2)));
        $this->ticket_data->setValidFromReturn(date('d.m.Y',strtotime($date1)));
        $this->ticket_data->setValidTill(date('d.m.Y',strtotime($date2_til)));
        $this->ticket_data->setValidTillReturn(date('d.m.Y',strtotime($date1_til)));
      }
      $this->ticket_data->setInfoText('');
    }
    elseif ($date1!==null)
    {
      $this->ticket_data->setValidFrom(date('d.m.Y',strtotime($date1)));
      $this->ticket_data->setValidTill(date('d.m.Y',strtotime($date1_til)));
      $this->ticket_data->setInfoText('');
    }
    elseif ($date2!==null)
    {
      $this->ticket_data->setValidFrom(date('d.m.Y',strtotime($date2)));
      $this->ticket_data->setValidTill(date('d.m.Y',strtotime($date2_til)));
      $this->ticket_data->setInfoText('');
    }

    return $this->ticket_data;
  }

  /**
   * returns the data for UIC918
   *
   * @return UicLayoutContainer Data in UIC918 structure (head, body)
   */
  public function getTicketUic918()
  {
    return $this->getTicketUic918_3Default();
  }
}
