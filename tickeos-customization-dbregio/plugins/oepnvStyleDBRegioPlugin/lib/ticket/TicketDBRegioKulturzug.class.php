<?php
class TicketDBRegioKulturzug extends TicketDBRegioStandard
{
  const ID = 'db_regio_kulturzug';

  
  public static function register()
  {
    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent(new self(),'oepnv.ticket_types.register'));
  }

  public function getId()
  {
    return self::ID;
  }

  public function getName()
  {
    return "DB Regio Ticket Kulturzug";
  }
  
  /**
   * returns the template-dependent data fields (data by a TicketDataProvider)
   * (array with replacer_names as keys and the value as value)
   * @return arr<field_name::str>, field_name can be a ticket role
   */
  public function getDataFields()
  {

    $ret = parent::getDataFields();
    $code = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PRODUCT_CODE_BASE);
    if($code == 'B-W')
    {
      $ret['product_from'] = 'Berlin-Lichtenberg/ Berlin Ostkreuz/ Cottbus/ Forst (Lausitz)';
      $ret['product_to'] = 'Zary/ Zagan/ Legnica/ Wroclaw Gl.';
    }
    else
    {
      $ret['product_from'] = 'Wroclaw Gl./ Legnica/ Zagan/ Zary';
      $ret['product_to'] = 'Forst (Lausitz)/ Cottbus/ Berlin Ostkreuz/ Berlin-Lichtenberg';
    }

    return $ret;
  }

  /**
   * returns the data for UIC918
   *
   * @return UicLayoutContainer Data in UIC918 structure (head, body)
   */
  public function getTicketData()
  {
    if($this->ticket_data!=null)
    {
      return $this->ticket_data;
    }

    parent::getTicketData(new RelationTicketData());

    $code = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PRODUCT_CODE_BASE);
    if($code == 'B-W')
    {
      $this->ticket_data->setInfoText('Einzelfahrt: Berlin - Wroclaw');
    }
    else
    {
      $this->ticket_data->setInfoText('Einzelfahrt: Wroclaw - Berlin');
    }

    return $this->ticket_data;
  }
}
