<?php
class TicketDBRegioStandardSubproducts extends TicketDBRegioStandard
{
  const ID = 'db_regio_standard_subproducts';
  const SCOPE_TICKETART = 'db_regio_standard_ticketart';

    public static function register()
    {
        sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent(new self(),'oepnv.ticket_types.register'));
    }

    public function getId()
    {
        return self::ID;
    }

  public function getName()
  {
    return "DB Regio Ticket Standard Unterprodukte";
  }


  /**
   * init the dataproviders,
   *   add some product scopes
   *
   * @param string[] $required_model_roles
   * @param $source_objects
   */
  protected function initDataProviders($required_model_roles, $source_objects)
  {
    parent::initDataProviders($required_model_roles, $source_objects);

    /** @var TicketDataProviderTickeosProduct $dp_product */
    $dp_product = $this->data_providers[TicketDataProvider::TICKEOS_PRODUCT];
    $dp_product->addSubProductScope(self::SCOPE_TICKETART, oepnvDBRegioEnums::PRODUCT_TYPE_TICKETART);

  }
  
  /**
   * returns the template-dependent data fields (data by a TicketDataProvider)
   * (array with replacer_names as keys and the value as value)
   * @return arr<field_name::str>, field_name can be a ticket role
   *
   */
  public function getDataFields()
  {
    $ret = parent::getDataFields();

    $temp_coach_class = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::TARIFF_COACH_CLASS_NUMERIC, null,  self::SCOPE_TICKETART);
    if(!empty($temp_coach_class))
    {
      $ret['coach_class'] = $temp_coach_class;
    }

    $temp_product_valid = date('d.m.Y', strtotime($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_FROM_DATE, null,  self::SCOPE_TICKETART)));
    if(!empty($temp_product_valid) && strtotime($temp_product_valid) >= strtotime(date('d.m.Y')))
    {
      $ret['product_valid'] = $temp_product_valid;
    }

    $temp_product_info_text = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::INFO_TEXT, null,  self::SCOPE_TICKETART);
    if(!empty($temp_product_info_text))
    {
      $ret['product_info_text'] = $temp_product_info_text;
    }

    $temp_product_name = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PRINTTICKET_TEXT, null,  self::SCOPE_TICKETART);
    if(!empty($temp_product_name))
    {
      $ret['product_name'] = $temp_product_name;
    }
    else
    {
      $temp_product_name = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::NAME, null,  self::SCOPE_TICKETART);
      if(!empty($temp_product_name))
      {
        $ret['product_name'] = $temp_product_name;
      }
    }

    $temp_person_number = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PERSON_NUMBER, null,  self::SCOPE_TICKETART);
    if(!empty($temp_person_number))
    {
      $ret['person_number'] = $temp_person_number;
    }
    return $ret;
  }

  /**
   * returns base ticket data, used by all kind of data getters.
   * this class can be overloaded by concrete ticket processing class
   * to customize fields/values
   * @param BaseTicketData $ticket_data (optional)
   * @return BaseTicketData
   */
  public function  getTicketData($ticket_data=null)
  {
    $ticket_data = parent::getTicketData();

    $temp_product_name = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::PRINTTICKET_TEXT, null,  self::SCOPE_TICKETART);
    if(!empty($temp_product_name))
    {
      $ticket_data->setProductName($temp_product_name);
    }
    else
    {
      $temp_product_name = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::NAME, null,  self::SCOPE_TICKETART);
      if(!empty($temp_product_name))
      {
        $ticket_data->setProductName($temp_product_name);
      }
    }

    return $ticket_data;
  }
}