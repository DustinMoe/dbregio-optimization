<?php
class TicketDBRegioAirportExpress extends TicketDBRegioStandard
{
  const ID = 'db_regio_standard_airport_express';

  public static function register()
  {
    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent(new self(),'oepnv.ticket_types.register'));
  }

  public function getId()
  {
    return self::ID;
  }

  public function getName()
  {
    return "DB Regio Ticket Airport Express";
  }

  /**
   * init the dataproviders,
   *   add some product scopes
   *
   * @param string[] $required_model_roles
   * @param $source_objects
   */
  protected function initDataProviders($required_model_roles, $source_objects)
  {
    parent::initDataProviders($required_model_roles, $source_objects);

    /** @var TicketDataProviderTickeosProduct $dp_product */
    $dp_product = $this->data_providers[TicketDataProvider::TICKEOS_PRODUCT];
  }

  /**
   * returns the template-dependent data fields (data by a TicketDataProvider)
   * (array with replacer_names as keys and the value as value)
   * @return arr<field_name::str>, field_name can be a ticket role
   */
  public function getDataFields()
  {
    $ret = parent::getDataFields();
    $direction_2 = $ret['product_direction_2'];
    if ($ret['product_direction_2'] == NULL) {
      //Einzelfahrt
      $ret['product_ae_valid_from'] = date('d.m.Y', strtotime($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_FROM_DATE, null,  self::SCOPE_RICHTUNG)));
    } else {
      //Hin- und Rückfahrt

      //erste Fahrt ist immer der untere Abschnitt, unabhängig von der Strecke, deshalb Gültigkeiten und Richtungen tauschen
      $first_tour_time = strtotime($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_FROM_DATE, null,  self::SCOPE_RICHTUNG));
      $secend_tour_time = strtotime($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::VALID_FROM_DATE, null,  self::SCOPE_RICHTUNG2));
      if ($first_tour_time > $secend_tour_time)
      {
        $ret['product_ae_valid_from'] = date('d.m.Y', $secend_tour_time);
        $ret['product_ae_valid_from_return'] = date('d.m.Y', $first_tour_time);
        $direction_first_tour = $ret['product_direction'];
        $direction_second_tour = $ret['product_direction_2'];
        $ret['product_direction'] = $direction_second_tour;
        $ret['product_direction_2'] = $direction_first_tour;
      } else
      {
        $ret['product_ae_valid_from'] = date('d.m.Y', $first_tour_time);
        $ret['product_ae_valid_from_return'] = date('d.m.Y', $secend_tour_time);
      }
    }

    $ret['product_name'] = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::NAME) . ': ' .
      $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::NAME, null, self::SCOPE_AIRPORTEXPRESSTICKET) . ' ' .
      $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::NAME, null, self::SCOPE_FAHROPTIONEN);

    $ret['customer_identity_card_name_number'] = $ret['customer_identity_card_name'] . ' ' . $ret['customer_identity_card_number'];

    // we have to work with separate backgroundtexts here so we can have different dates for different personalization_areas
    $bt = array();
    $bt['passenger_name'] = $this->getDataProviderValue(TicketDataProvider::PASSENGER, TicketDataProviderPassenger::FIRST_AND_LAST_NAME);
    $bt['ticket_id'] = $ret['product_serial_number'];
    $bt['ticket_name'] = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::NAME);
    $bt['valid_to'] = $ret['product_ae_valid_from'];
    $ret['background_text_to'] = $this->generateBackgroundTextBase($bt, 5);

    $bt['valid_return'] = $ret['product_ae_valid_from_return'];
    $ret['background_text'] = $this->generateBackgroundTextBase($bt, 5);

    unset($bt['valid_to']);
    $ret['background_text_return'] = $this->generateBackgroundTextBase($bt, 5);
    return $ret;
  }

  /**
   * @return BaseTicketData|null|TicketData
   */
  public function getTicketData()
  {
    parent::getTicketData();

    $this->ticket_data->setPassengerType1Name($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT,
                                                    TicketDataProviderTickeosProduct::NAME, null,
                                                    self::SCOPE_AIRPORTEXPRESSTICKET));

    $journey1_from = $this->ticket_data->getValidFrom();
    $journey1_till = $this->ticket_data->getValidTill();
    $journey2_from = $this->ticket_data->getValidFromReturn();
    $journey2_till = $this->ticket_data->getValidTillReturn();
    $name_from = $this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT,
      TicketDataProviderTickeosProduct::NAME,
      null, self::SCOPE_RICHTUNG);

    if (!is_null($journey2_from) && strtotime($journey1_from) > strtotime($journey2_from))
    {
      $this->ticket_data->setValidFrom($journey2_from);
      $this->ticket_data->setValidTill($journey2_till);
      $this->ticket_data->setValidFromReturn($journey1_from);
      $this->ticket_data->setValidTillReturn($journey1_till);
    }

    if (!is_null($journey2_from))
    {
      $this->ticket_data->setInfoText('Hin- und Rückfahrt: ' . $name_from);
    }
    else
    {

      $this->ticket_data->setInfoText('Einzelfahrt: ' . $name_from);
    }

    return $this->ticket_data;
  }
}
