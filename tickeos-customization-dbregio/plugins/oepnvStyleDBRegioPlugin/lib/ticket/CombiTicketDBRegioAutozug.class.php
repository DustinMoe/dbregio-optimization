<?php
class CombiTicketDBRegioAutozug extends CombiTicketTicket
{
  const ID = 'db_regio_combiticket_autozug';

  public static function register()
  {
    sfContext::getInstance()->getEventDispatcher()->notify(new sfEvent(new self(),'oepnv.ticket_types.register'));
  }

  public function getId()
  {
    return self::ID;
  }

  public function getName()
  {
    return "DB Regio Combi Ticket Autozug";
  }

  /**
   * returns the template-dependent data fields (data by a TicketDataProvider)
   * (array with replacer_names as keys and the value as value)
   * @return arr<field_name::str>, field_name can be a ticket role
   */
  public function getDataFields()
  {
    return array();
  }
  
  public function getTicketUic918()
  {
    return $this->getTicketUic918_3Default();
  }	
  
    /**
   * returns the data for UIC918-3 filled with default values
   * @return UicLayoutContainer Data in UIC918-3 structure (head, body)
   */
  protected function getTicketUic918_3Default()
  {
    $ticket_head = new UicUheadLayout();
    $ticket_head->setTicketKey($this->getTicketData()->getTicketId(true));
    
    $uic918 = new Uic918_3_autozug();

    $uic918->setTicketType($this->getTicketData()->getTicketType());
	$uic918->setTicketType2($this->getTicketData()->getTicketType2());
	$uic918->setCarHValue($this->getTicketData()->getCarHValue());
    $uic918->setProductName($this->getTicketData()->getProductName());
    $uic918->setTariffZone($this->getTicketData()->getTariffZone());
	$uic918->setCarHValue($this->getTicketData()->getCarHValue());
	$uic918->setCarRValue($this->getTicketData()->getCarRValue());
	$uic918->setCarHPLaceValue($this->getTicketData()->getCarHPLaceValue());
	$uic918->setCarRPLaceValue($this->getTicketData()->getCarRPLaceValue());
	$uic918->setBefoerdererValue($this->getTicketData()->getBefoerdererValue());
	$uic918->setTarifOut($this->getTicketData()->getTarifOut());
	$uic918->setTarifRet($this->getTicketData()->getTarifRet());
    
	$uic918->setTrainType("AUTOZUG");
	$uic918->setVia($this->getTicketData()->getViaName());
	
    $uic918->setValidFrom($this->getTicketData()->getValidFrom());
    $uic918->setValidTill($this->getTicketData()->getValidTill());
	
	$uic918->setValidFromReturn($this->getTicketData()->getValidFromReturn());
    $uic918->setValidTillReturn($this->getTicketData()->getValidTillReturn());
	
	$uic918->setFrom($this->getTicketData()->getFromName());
    $uic918->setTo($this->getTicketData()->getToName());
	
	$uic918->setFromReturn($this->getTicketData()->getFromNameReturn());
    $uic918->setToReturn($this->getTicketData()->getToNameReturn());

    $uic918->setCustomerGroup1($this->getTicketData()->getPassengerType1Name());
    $uic918->setNumCustomerGroup1($this->getTicketData()->getPassengerType1Number());
	
	$uic918->setCustomerGroup2($this->getTicketData()->getPassengerType2Name());
    $uic918->setNumCustomerGroup2($this->getTicketData()->getPassengerType2Number());

    $uic918->setCurrency($this->getTicketData()->getCurrency());
    $uic918->setPrice($this->getTicketData()->getPrice(BaseTicketData::PRICE_FORMAT_DECIMAL));
	$uic918->setPriceLevel($this->getTicketData()->getPriceLevel());
	
    $uic918->setFirstName($this->getTicketData()->getPassengerFirstName());
    $uic918->setLastName($this->getTicketData()->getPassengerLastName());
    //$uic918->setBirthday($this->getTicketData()->getPassengerBirthday());
	
	$uic918->setCoachClass(substr($this->getTicketData()->getCouchClass(),-1));
	$uic918->setCouchClassReturn(substr($this->getTicketData()->getCouchClassReturn(),-1));
    
    $verifying = trim($this->getTicketData()->getVerifyingDocumentName());
    $verifying_no = trim($this->getTicketData()->getVerifyingDocumentNumber());
    if($verifying_no)
    { //@see: UIC SPEC page 30/31, min. die ersten 4 Zeichen von Nummer wenn >25 char
      $verifying = substr($verifying, 0, 20).' '.$verifying_no;
    }
    $uic918->setVerifyingDocument($verifying);

    $uic_ut_layout = $uic918->createUicUtLayout();
FB::log($uic_ut_layout, 'UicUtLayout');
$matrix = new Uic918LayoutMatrix($uic_ut_layout);
//echo($matrix->toHmtl());die();

    $ticket_container = new UicLayoutContainer($ticket_head, $uic_ut_layout);
    return $ticket_container;
  }
  
  public function getTicketData()
  {
    if($this->ticket_data!=null)
    {
      return $this->ticket_data;
    }
    
    $this->ticket_data = new RelationTicketDataAutozug();
    
    $this->ticket_data->setTicketId($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::TICKET_ID_EXTERNAL));
    $this->ticket_data->setTicketType($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'fahrscheinBezeichnung1'));
	$this->ticket_data->setTicketType2($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'fahrscheinBezeichnung2'));
	$this->ticket_data->setCarHValue($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'coachOut'));
	$this->ticket_data->setCarRValue($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'coachRet'));
	$this->ticket_data->setCarHPlaceValue($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'seatOut'));
	$this->ticket_data->setCarRPlaceValue($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'seatRet'));
	$this->ticket_data->setBefoerdererValue($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'carrier'));
	$this->ticket_data->setTarifOut($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'tarifOut'));
	$this->ticket_data->setTarifRet($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'tarifRet'));
    $this->ticket_data->setProductName($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::PRODUCT_NAME));
    $this->ticket_data->setProductReferenceId($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::PRODUCT_REFERENCE_ID));
	
    
    $tariff_zone = $this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::PRODUCT_TARIFF_ZONE_DEFAULT);
    if(!$tariff_zone)
    {
      $this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::PRODUCT_TARIFF_ZONE_LABEL);
    }
    $this->ticket_data->setTariffZone($tariff_zone);
    
    $this->ticket_data->setValidFrom($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::VALIDITY_START));
    $this->ticket_data->setValidTill($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::VALIDITY_END));
	
	$this->ticket_data->setFromName($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'fromOut'));
	$this->ticket_data->setToName($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'toOut'));
	
	$this->ticket_data->setFromNameReturn($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'fromRet'));
	$this->ticket_data->setToNameReturn($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'toRet'));
	
	$v=$this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'validRetFrom');
	if ($v!==null)
		$v=date("d.m.Y H:i", strtotime($v));
	$this->ticket_data->setValidFromReturn($v);
	
	$v=$this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'validRetTill');
	if ($v!==null)
		$v=date("d.m.Y H:i", strtotime($v));
    $this->ticket_data->setValidTillReturn($v);
		
	$this->ticket_data->setViaName($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'via'));
	
    $this->ticket_data->setCreatedAt($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::CREATED_AT, "d.m.Y H:i:s"));
    $this->ticket_data->setInfoText($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::VALIDITY_TEXT));

    $this->ticket_data->setPassengerType1Name('Erwachsene');
    $this->ticket_data->setPassengerType1Number($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'paxAdult'));
	
	$this->ticket_data->setPassengerType2Name('Kinder');
    $this->ticket_data->setPassengerType2Number($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'paxChild'));
	$this->ticket_data->setViaName($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'via'));

    $this->ticket_data->setCurrency('EUR');
    $this->ticket_data->setPrice(str_replace(',','.',$this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'price')));
    $this->ticket_data->setVatRate($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::PRODUCT_VAT_VALUE));

    $this->ticket_data->setPassengerFirstName($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::PASSENGER_FIRST_NAME));
    $this->ticket_data->setPassengerLastName($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::PASSENGER_LAST_NAME));
	
    $this->ticket_data->setVerifyingDocumentName($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'verifying_document'));
    //$this->ticket_data->setVerifyingDocumentShort($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::IDENTITY_CARD_TYPE_CHAR));
	$this->ticket_data->setPriceLevel($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'priceType'));
    $this->ticket_data->setVerifyingDocumentNumber($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'verifying_id'));
    
	$this->ticket_data->setCouchClass($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'class'));
	$this->ticket_data->setCouchClassReturn($this->getDataProviderValue(TicketDataProvider::COMBI_TICKET, TicketDataProviderCombiTicket::ASSIGNMENT_VALUE,'class_return'));
	
    // TODO: aus constraintem Produkt holen sobald erforderlich
    #$uul->setCoachClass($this->getDataProviderValue(TicketDataProvider::TICKEOS_PRODUCT, TicketDataProviderTickeosProduct::TARIFF_COACH_CLASS_NUMERIC));
//var_dump($this->ticket_data->toArray());
//die();
FB::log($this->ticket_data->toArray());
    return $this->ticket_data;
  }
  
  
}