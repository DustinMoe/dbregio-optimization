<?php

    $background_pdf = '[background_pdf]';
    $personalization_area = '[personalization_area]';
    $security_symbol = '[ticket_security_symbol]';
    $serial_code = '[ticket_serial_code]';
    $information_area = '[information_area]';
    $aztec_barcode = '[aztec_barcode]';
    $qr_code = '[qr_barcode]';
    $logo_db = '[logo_db]';

    // Fix da sonst eine notice wegen undefined variable erscheint
    $border_left = (!isset($border_left)) ? 0 : $border_left;
    $border_top = (!isset($border_top)) ? 0 : $border_top;

    if(isset($background_pdf) && $background_pdf)
    {
        $pagecount = $pdf->setSourceFile($background_pdf);
        $tplidx = $pdf->ImportPage(1);
        $pdf->useTemplate($tplidx);
    }

    // logo
    system(IMAGEMAGICK_DIR.'convert '.$logo_db.' +matte '.$logo_db);
    $pdf->Image($logo_db, 30.2, 10.2, 9, 6.24);

    // left box
    system(IMAGEMAGICK_DIR.'convert '.$personalization_area.' +matte '.$personalization_area);
    $pdf->Image($personalization_area, 28, 22.2, 50, 65);

    // right box
    system(IMAGEMAGICK_DIR.'convert '.$information_area.' +matte '.$information_area);
    $pdf->Image($information_area, 132.5, 22.4, 50, 65);

    // security symbol (right box)
    system(IMAGEMAGICK_DIR.'convert '.$security_symbol.' +matte '.$security_symbol);
    $pdf->Image($security_symbol, 146, 76, 10, 10);


    $pdf->SetFont('Arial', 'I', 10);

    // aztec barcode (middle box)
    system(IMAGEMAGICK_DIR.'convert '.$aztec_barcode.' +matte '.$aztec_barcode);
    $pdf->Image($aztec_barcode, 86, 29.1, 40, 40);

    // serial number (middle box)
    system(IMAGEMAGICK_DIR.'convert '.$serial_code.' +matte '.$serial_code);
    $pdf->Image($serial_code, 86, 71, 40, 14.5);

    // qr code (right box)
//    system(IMAGEMAGICK_DIR.'convert '.$qr_code.' +matte '.$qr_code);
//    $pdf->Image($qr_code, 152, 61, 26, 26);


    $pdf->SetFont('Arial', 'b', 18);
    $pdf->SetTextColor(250, 250, 250);