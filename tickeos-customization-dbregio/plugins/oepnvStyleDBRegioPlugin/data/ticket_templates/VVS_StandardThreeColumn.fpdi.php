<?php

    $background_pdf = '[background_pdf]';
    $personalization_area = '[personalization_area]';
    $security_symbol = '[ticket_security_symbol]';
    $serial_code = '[ticket_serial_code]';
    $information_area = '[information_area]';
    $aztec_barcode = '[aztec_barcode]';
    $qr_code = '[qr_barcode]';
    $logo_db = '[logo_db]';

    // Fix da sonst eine notice wegen undefined variable erscheint
    $border_left = (!isset($border_left)) ? 0 : $border_left;
    $border_top = (!isset($border_top)) ? 0 : $border_top;

    if(isset($background_pdf) && $background_pdf)
    {
        $pagecount = $pdf->setSourceFile($background_pdf);
        $tplidx = $pdf->ImportPage(1);
        $pdf->useTemplate($tplidx);
    }

    // logo
    system(IMAGEMAGICK_DIR.'convert '.$logo_db.' +matte'.$logo_db);
    $pdf->Image($logo_db, 21.42, 14.2, 13.45, 9.32);

    // left box
    system(IMAGEMAGICK_DIR.'convert '.$personalization_area.' +matte '.$personalization_area);
    $pdf->Image($personalization_area, 22, 27.25, 50, 64.5);

    // right box
    system(IMAGEMAGICK_DIR.'convert '.$information_area.' +matte '.$information_area);
    $pdf->Image($information_area, 128, 27.25, 50, 64.5);

    // security symbol
    system(IMAGEMAGICK_DIR.'convert '.$security_symbol.' +matte '.$security_symbol);
    $pdf->Image($security_symbol, 142, 80, 10, 10);

    $pdf->SetFont('Arial', 'I', 10);

    // barcode info (middle box)
    $barcode_info = 'Barcode bitte nicht knicken!';
    $pdf->Text(78, 32, $barcode_info);

    // aztec barcode (middle box)
    system(IMAGEMAGICK_DIR.'convert '.$aztec_barcode.' +matte '.$aztec_barcode);
    $pdf->Image($aztec_barcode, 80, 35, 40, 40);

    // serial number (middle box)
    system(IMAGEMAGICK_DIR.'convert '.$serial_code.' +matte '.$serial_code);
    $pdf->Image($serial_code, 80, 75, 40, 16);

    // qr code (right box)
    system(IMAGEMAGICK_DIR.'convert '.$qr_code.' +matte '.$qr_code);
    $pdf->Image($qr_code, 152, 65.5, 26, 26);


    $pdf->SetFont('Arial', 'b', 18);

    // Ticket name
    $pdf->Text(40, 19, utf8_decode('[ticket_name]'));

    $pdf->SetFont('Arial', '', 14);
    // Tariff name
    $pdf->Text(40, 25, utf8_decode('[tariff_name]'));