<?php
if (eosLicenseManager::getInstance()->isModuleLicensed('ticket'))
{
  $this->dispatcher->connect('oepnv.semesterticket.student_verification_service.registry_ready', array('oepnvStyleDBRegioEventListener', 'semesterticketStudentVerificationServiceRegistryReadyEvent'));
  $this->dispatcher->connect('oepnv.ticket_types.registry_ready', array('oepnvStyleDBRegioEventListener', 'ticketTypesRegistryReadyEvent'));
  $this->dispatcher->connect('oepnv.price.calculation.registry_ready',array('oepnvStyleDBRegioEventListener','priceCalculationRegistryReadyEvent'));
  $this->dispatcher->connect('oepnv.product.post_validation.registry_ready',array('oepnvStyleDBRegioEventListener','productPostValidationRegistryReadyEvent'));
}
