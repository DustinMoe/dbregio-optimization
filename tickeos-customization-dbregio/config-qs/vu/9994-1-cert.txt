Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            9a:72:ad:74:41:2f:91:d5
        Signature Algorithm: dsaWithSHA1
        Issuer: C=DE, ST=Hessen, L=Frankfurt/Main, O=DB Vertrieb GmbH, OU=P.DVD34, CN=Thorge Loh/emailAddress=thorge.loh@deutschebahn.com
        Validity
            Not Before: Oct  7 09:09:59 2010 GMT
            Not After : Oct  2 09:09:59 2030 GMT
        Subject: C=DE, ST=Hessen, L=Frankfurt/Main, O=DB Vertrieb GmbH, OU=P.DVD34, CN=Thorge Loh/emailAddress=thorge.loh@deutschebahn.com
        Subject Public Key Info:
            Public Key Algorithm: dsaEncryption
            DSA Public Key:
                pub: 
                    7e:ca:16:55:f1:69:50:73:b2:fc:fd:63:3e:12:c2:
                    87:1b:38:fc:89:1a:f9:ea:b1:e7:e5:c7:98:10:0e:
                    f4:24:47:d3:00:04:8d:2c:5b:f7:6a:2a:34:e2:5c:
                    fc:f9:59:b1:25:41:52:6b:21:84:73:a0:5f:70:3e:
                    23:79:da:2c:aa:3d:52:02:58:0e:95:89:3f:9a:c1:
                    94:38:32:28:5b:a9:5b:e2:95:92:ad:d8:43:38:31:
                    42:20:2d:03:3d:ee:3f:3a:90:6c:b6:97:a4:ab:1c:
                    16:b3:35:f6:f1:4e:91:e0:cd:41:9a:b9:08:65:da:
                    9d:f1:09:76:1a:eb:f9:ae
                P:   
                    00:cd:a8:41:b3:3d:d4:36:eb:dd:7d:84:af:6a:34:
                    8d:2f:ea:90:8e:bc:45:74:b0:1d:f3:d0:26:7a:18:
                    8e:d9:90:ca:90:85:d6:6b:3b:73:59:7a:53:2b:85:
                    4f:71:b2:18:68:8b:a1:f6:a7:6f:b3:03:e2:0f:19:
                    b1:02:50:49:3c:0c:2d:f2:e8:1c:bb:50:27:ca:1c:
                    cf:8e:78:23:13:9b:a7:31:9c:0c:6d:c5:d6:c1:1d:
                    54:10:59:16:84:01:97:4d:ef:95:54:31:60:74:58:
                    f4:cd:c4:f3:0b:ab:23:c4:f7:12:c4:c8:d8:44:db:
                    df:f5:83:4b:09:62:38:0a:17
                Q:   
                    00:de:7f:31:37:2c:25:7c:d7:76:44:ef:42:97:11:
                    d6:c5:a8:bf:d2:55
                G:   
                    3e:96:5b:aa:21:6b:71:5b:3e:17:b6:5a:e3:24:52:
                    9f:23:c6:5d:f1:18:56:8c:0e:88:60:0f:57:89:01:
                    4c:14:a2:17:4a:78:e4:1a:81:93:77:e0:23:54:ab:
                    8a:a2:89:27:ef:b5:8e:d2:13:3c:f5:33:8b:dd:f5:
                    67:fd:8d:fc:05:5a:0e:f5:18:98:d1:7f:8a:e3:8f:
                    f6:44:d3:ea:44:6d:ee:89:d5:33:01:d2:2d:13:0f:
                    06:70:3d:a1:be:4d:24:9c:75:51:64:1a:ca:ca:b5:
                    51:8c:0a:92:2d:5a:0b:2a:04:20:c1:d4:26:59:5d:
                    64:cc:21:b4:0a:15:00:ac
        X509v3 extensions:
            X509v3 Subject Key Identifier: 
                92:EB:9A:95:3B:45:F3:07:90:95:66:8A:C9:61:CD:35:8D:BD:1B:0A
            X509v3 Authority Key Identifier: 
                keyid:92:EB:9A:95:3B:45:F3:07:90:95:66:8A:C9:61:CD:35:8D:BD:1B:0A

            X509v3 Basic Constraints: 
                CA:TRUE
    Signature Algorithm: dsaWithSHA1
        30:2d:02:15:00:c5:c0:3f:72:ad:d2:53:63:0e:17:dc:2b:e2:
        03:0f:fa:0f:c3:68:4c:02:14:68:81:f8:6d:5b:c9:b1:36:26:
        68:ee:52:42:b0:ef:f3:b3:69:71:1b
