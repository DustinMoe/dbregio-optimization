<?php
/* This file was generated by the metarefresh module at 2017-01-23T23:55:00Z
 Do not update it manually as it will get overwritten
*/

$metadata['https://idp-rz.uni-mannheim.de/idp/shibboleth'] = array (
  'expire' => 1485558014,
  'protocols' => 
  array (
    0 => 'urn:oasis:names:tc:SAML:1.1:protocol',
    1 => 'urn:oasis:names:tc:SAML:2.0:protocol',
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIHZzCCBk+gAwIBAgIHFMxQvSU8uTANBgkqhkiG9w0BAQUFADCByTELMAkGA1UE
BhMCREUxGzAZBgNVBAgTEkJhZGVuLVd1ZXJ0dGVtYmVyZzERMA8GA1UEBxMITWFu
bmhlaW0xHjAcBgNVBAoTFVVuaXZlcnNpdGFldCBNYW5uaGVpbTEWMBQGA1UECxMN
UmVjaGVuemVudHJ1bTEoMCYGA1UEAxMfUlVNLUNBLUcgWmVydGlmaXppZXJ1bmdz
aW5zdGFuejEoMCYGCSqGSIb3DQEJARYZcnVtLWNhQHJ6LnVuaS1tYW5uaGVpbS5k
ZTAeFw0xMjExMjExMzEyMTNaFw0xNzExMjAxMzEyMTNaMIGWMQswCQYDVQQGEwJE
RTEbMBkGA1UECBMSQmFkZW4tV3VlcnR0ZW1iZXJnMREwDwYDVQQHEwhNYW5uaGVp
bTEeMBwGA1UEChMVVW5pdmVyc2l0YWV0IE1hbm5oZWltMRYwFAYDVQQLEw1SZWNo
ZW56ZW50cnVtMR8wHQYDVQQDExZpZHAtcnoudW5pLW1hbm5oZWltLmRlMIICIjAN
BgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAxMOaiDXF5E1nw8kpdfyVRqv3KWFt
zPGuy6Ry/KitGXS7oO3YCqVsTY5R18tdQRNucaxMp+BjZm2KrZ7ybFrDu3u/A4Ml
WX4iJmnKvwVQkSbeW/5c6LhfrysHmtkiZTn0Y/XQg8L7aDUwfa1/fhF3c1GcL5to
WvPzsxVXyHsa/50o+yZd5k+C7YeF+hfz/y5Zo7IlPCEMv7z4/CQjsl8RgClffF+h
xDrfZG+Wuh82veyd+EaYadWjsyyVYk1WVnPqePVyIO1R0rpd7mlB/XZv0RAIzoRK
xARIwDck30h57ZrR0j5rTARVQqG0jvuKAWkCmpozzu7r32DRTPSH4pH7HdqPNh4H
JR92QB6Kr1pTAH6CGSsLmLSYpBztNGwWoQSHqY5Na3vGLrZSgPcLrrpgJmcJjZR3
4duB6d8JBWZBuoBH3ashjEbxaHM9SDa1ifG9Huzjmnx+dm8yKWGJpqsgu6AGIfU6
+OZ75Ew+smRVSk68AeKk92N1aaaBbSn4RnBYriZe9tfEviCQygDPaAcXjowujFF0
dplO1u/CFHnhrxWACX2l/IMSu6DcobcmGZKcZ+wyilmToqaCZc9cJCtV9OQKOHYl
9wvcJIfPzjn4YmGFqkRlb/RgMNLwnG5f4Mp7+NKGzkoNJ+sarnuSg+N/E+glKole
CJ6aPptnJ7EOWYsCAwEAAaOCAoMwggJ/MC8GA1UdIAQoMCYwEQYPKwYBBAGBrSGC
LAEBBAMAMBEGDysGAQQBga0hgiwCAQQDADAJBgNVHRMEAjAAMAsGA1UdDwQEAwIF
4DAdBgNVHSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwEwHQYDVR0OBBYEFKK/bvUP
WeAphd3A0l6zEEUp2otSMB8GA1UdIwQYMBaAFFkgKUBC1v6OBy66PYdPMq1kdo4M
MGsGA1UdEQRkMGKCGWlkcC1yei12MS51bmktbWFubmhlaW0uZGWCFmlkcC1yei51
bmktbWFubmhlaW0uZGWGLWh0dHBzOi8vaWRwLXJ6LnVuaS1tYW5uaGVpbS5kZS9p
ZHAvc2hpYmJvbGV0aDCBiwYDVR0fBIGDMIGAMD6gPKA6hjhodHRwOi8vY2RwMS5w
Y2EuZGZuLmRlL3VuaS1tYW5uaGVpbS1jYS9wdWIvY3JsL2NhY3JsLmNybDA+oDyg
OoY4aHR0cDovL2NkcDIucGNhLmRmbi5kZS91bmktbWFubmhlaW0tY2EvcHViL2Ny
bC9jYWNybC5jcmwwgdkGCCsGAQUFBwEBBIHMMIHJMDMGCCsGAQUFBzABhidodHRw
Oi8vb2NzcC5wY2EuZGZuLmRlL09DU1AtU2VydmVyL09DU1AwSAYIKwYBBQUHMAKG
PGh0dHA6Ly9jZHAxLnBjYS5kZm4uZGUvdW5pLW1hbm5oZWltLWNhL3B1Yi9jYWNl
cnQvY2FjZXJ0LmNydDBIBggrBgEFBQcwAoY8aHR0cDovL2NkcDIucGNhLmRmbi5k
ZS91bmktbWFubmhlaW0tY2EvcHViL2NhY2VydC9jYWNlcnQuY3J0MA0GCSqGSIb3
DQEBBQUAA4IBAQBOpxdzeLwUhve54VShisHOo2Omm+9SqXspHIOni3LNDnFmowro
+gxgNLRwaRaELZRHCzfPugbIiNAWB7PeAG8ptbGupS+x6s29xS70POGC9ciqqIGC
eXEbhDH7J99/PbI8v3Hk3MbLXfmiLSylnV/RMMKmGLjkvAURf0pGoUH+poZysCl5
EIZ7M+yg+MOAo65vNoyomP3p3C9c9UJg7sXOq4R18k0urf8HTDjVqPv5mmIhdptV
nOMPPeIyD3ww0cp+uACQqdtYkXvViWxScP7t5D9IyibxkV6l20Fv+90LEyejrPmu
LX2bs0Mmoy8+2S1INk3ZbQlLt00NK90nE+LN
',
    ),
    1 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIHZzCCBk+gAwIBAgIHFMxQvSU8uTANBgkqhkiG9w0BAQUFADCByTELMAkGA1UE
BhMCREUxGzAZBgNVBAgTEkJhZGVuLVd1ZXJ0dGVtYmVyZzERMA8GA1UEBxMITWFu
bmhlaW0xHjAcBgNVBAoTFVVuaXZlcnNpdGFldCBNYW5uaGVpbTEWMBQGA1UECxMN
UmVjaGVuemVudHJ1bTEoMCYGA1UEAxMfUlVNLUNBLUcgWmVydGlmaXppZXJ1bmdz
aW5zdGFuejEoMCYGCSqGSIb3DQEJARYZcnVtLWNhQHJ6LnVuaS1tYW5uaGVpbS5k
ZTAeFw0xMjExMjExMzEyMTNaFw0xNzExMjAxMzEyMTNaMIGWMQswCQYDVQQGEwJE
RTEbMBkGA1UECBMSQmFkZW4tV3VlcnR0ZW1iZXJnMREwDwYDVQQHEwhNYW5uaGVp
bTEeMBwGA1UEChMVVW5pdmVyc2l0YWV0IE1hbm5oZWltMRYwFAYDVQQLEw1SZWNo
ZW56ZW50cnVtMR8wHQYDVQQDExZpZHAtcnoudW5pLW1hbm5oZWltLmRlMIICIjAN
BgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAxMOaiDXF5E1nw8kpdfyVRqv3KWFt
zPGuy6Ry/KitGXS7oO3YCqVsTY5R18tdQRNucaxMp+BjZm2KrZ7ybFrDu3u/A4Ml
WX4iJmnKvwVQkSbeW/5c6LhfrysHmtkiZTn0Y/XQg8L7aDUwfa1/fhF3c1GcL5to
WvPzsxVXyHsa/50o+yZd5k+C7YeF+hfz/y5Zo7IlPCEMv7z4/CQjsl8RgClffF+h
xDrfZG+Wuh82veyd+EaYadWjsyyVYk1WVnPqePVyIO1R0rpd7mlB/XZv0RAIzoRK
xARIwDck30h57ZrR0j5rTARVQqG0jvuKAWkCmpozzu7r32DRTPSH4pH7HdqPNh4H
JR92QB6Kr1pTAH6CGSsLmLSYpBztNGwWoQSHqY5Na3vGLrZSgPcLrrpgJmcJjZR3
4duB6d8JBWZBuoBH3ashjEbxaHM9SDa1ifG9Huzjmnx+dm8yKWGJpqsgu6AGIfU6
+OZ75Ew+smRVSk68AeKk92N1aaaBbSn4RnBYriZe9tfEviCQygDPaAcXjowujFF0
dplO1u/CFHnhrxWACX2l/IMSu6DcobcmGZKcZ+wyilmToqaCZc9cJCtV9OQKOHYl
9wvcJIfPzjn4YmGFqkRlb/RgMNLwnG5f4Mp7+NKGzkoNJ+sarnuSg+N/E+glKole
CJ6aPptnJ7EOWYsCAwEAAaOCAoMwggJ/MC8GA1UdIAQoMCYwEQYPKwYBBAGBrSGC
LAEBBAMAMBEGDysGAQQBga0hgiwCAQQDADAJBgNVHRMEAjAAMAsGA1UdDwQEAwIF
4DAdBgNVHSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwEwHQYDVR0OBBYEFKK/bvUP
WeAphd3A0l6zEEUp2otSMB8GA1UdIwQYMBaAFFkgKUBC1v6OBy66PYdPMq1kdo4M
MGsGA1UdEQRkMGKCGWlkcC1yei12MS51bmktbWFubmhlaW0uZGWCFmlkcC1yei51
bmktbWFubmhlaW0uZGWGLWh0dHBzOi8vaWRwLXJ6LnVuaS1tYW5uaGVpbS5kZS9p
ZHAvc2hpYmJvbGV0aDCBiwYDVR0fBIGDMIGAMD6gPKA6hjhodHRwOi8vY2RwMS5w
Y2EuZGZuLmRlL3VuaS1tYW5uaGVpbS1jYS9wdWIvY3JsL2NhY3JsLmNybDA+oDyg
OoY4aHR0cDovL2NkcDIucGNhLmRmbi5kZS91bmktbWFubmhlaW0tY2EvcHViL2Ny
bC9jYWNybC5jcmwwgdkGCCsGAQUFBwEBBIHMMIHJMDMGCCsGAQUFBzABhidodHRw
Oi8vb2NzcC5wY2EuZGZuLmRlL09DU1AtU2VydmVyL09DU1AwSAYIKwYBBQUHMAKG
PGh0dHA6Ly9jZHAxLnBjYS5kZm4uZGUvdW5pLW1hbm5oZWltLWNhL3B1Yi9jYWNl
cnQvY2FjZXJ0LmNydDBIBggrBgEFBQcwAoY8aHR0cDovL2NkcDIucGNhLmRmbi5k
ZS91bmktbWFubmhlaW0tY2EvcHViL2NhY2VydC9jYWNlcnQuY3J0MA0GCSqGSIb3
DQEBBQUAA4IBAQBOpxdzeLwUhve54VShisHOo2Omm+9SqXspHIOni3LNDnFmowro
+gxgNLRwaRaELZRHCzfPugbIiNAWB7PeAG8ptbGupS+x6s29xS70POGC9ciqqIGC
eXEbhDH7J99/PbI8v3Hk3MbLXfmiLSylnV/RMMKmGLjkvAURf0pGoUH+poZysCl5
EIZ7M+yg+MOAo65vNoyomP3p3C9c9UJg7sXOq4R18k0urf8HTDjVqPv5mmIhdptV
nOMPPeIyD3ww0cp+uACQqdtYkXvViWxScP7t5D9IyibxkV6l20Fv+90LEyejrPmu
LX2bs0Mmoy8+2S1INk3ZbQlLt00NK90nE+LN
',
    ),
    2 => 
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIHlTCCBn2gAwIBAgIHGVW4qn2PozANBgkqhkiG9w0BAQsFADCByTELMAkGA1UE
BhMCREUxGzAZBgNVBAgTEkJhZGVuLVd1ZXJ0dGVtYmVyZzERMA8GA1UEBxMITWFu
bmhlaW0xHjAcBgNVBAoTFVVuaXZlcnNpdGFldCBNYW5uaGVpbTEWMBQGA1UECxMN
UmVjaGVuemVudHJ1bTEoMCYGA1UEAxMfUlVNLUNBLUcgWmVydGlmaXppZXJ1bmdz
aW5zdGFuejEoMCYGCSqGSIb3DQEJARYZcnVtLWNhQHJ6LnVuaS1tYW5uaGVpbS5k
ZTAeFw0xNTA0MjExMTU3MTVaFw0xODA3MTgxMTU3MTVaMIGYMQswCQYDVQQGEwJE
RTEbMBkGA1UECAwSQmFkZW4tV3VlcnR0ZW1iZXJnMREwDwYDVQQHDAhNYW5uaGVp
bTEeMBwGA1UECgwVVW5pdmVyc2l0YWV0IE1hbm5oZWltMRYwFAYDVQQLDA1SZWNo
ZW56ZW50cnVtMSEwHwYDVQQDDBhpZHAtdGVzdC51bmktbWFubmhlaW0uZGUwggIi
MA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQC2wzcilA+l5yvnlM6YjHIS7xbX
h1dYcRtbI0aKc89o72qBWdAgtB3lILyDNlMO+C6ri0mwFLTurmod385Dc0O29ESI
Hi4tBPmHd6++stKZrneiD/RhirTf7mX/Zkm1tiuK0Uug1U70jOQXy5hlWSSkAgKh
sIFE6hOPFLUqT/Q8J5PJeSHIp4x1u5zxNfuYw8goZPabNBmWfvWavnEfHaiG/xxK
mA56w4gaYJk+qLgFkiGLMx45C2QYxpKTW3iL3czRlck85gDl70SZp5W3GZ+/msCc
Xl2kWgKOemeAmbYRAmaP86K2HCh7GmKmbdXCZUPifLO2fuOxc13RloOh1QNzX1y+
mtO22C05dsFlsjl07Q5VLpQ07+X72A39vx3TuuCVxmdY4/oxndWk1CNOhVTeo09n
GM4XpI6rf9luCINxrIZerc87Rj5T+92GnYailGJ95oDJ9tKQX9PNP9l8IZZqu6go
p4/dlR/a4a6hMkuMvJem8EBwGcqq8lHHozFAd1+13kBmDEUgN0vn1g/9D8/ZemRR
3mHrLFydC6MQGZzAnhPurDASmUIzJs934DLl/IwZYnIcIejNA2kZmQ7NVj6klodD
5RAfWoopuIEX1vl3+lG5kqqBQaMQRPT4EcsURP2fYhDCZRygDigzeI7HPD43G9jt
AWK4VcBl6cfR8bEbswIDAQABo4ICrzCCAqswTwYDVR0gBEgwRjARBg8rBgEEAYGt
IYIsAQEEAwMwEQYPKwYBBAGBrSGCLAIBBAMBMA8GDSsGAQQBga0hgiwBAQQwDQYL
KwYBBAGBrSGCLB4wCQYDVR0TBAIwADALBgNVHQ8EBAMCBeAwHQYDVR0lBBYwFAYI
KwYBBQUHAwIGCCsGAQUFBwMBMB0GA1UdDgQWBBTl2YrMPCMtdaraj22virpY7HL8
9zAfBgNVHSMEGDAWgBRZIClAQtb+jgcuuj2HTzKtZHaODDB3BgNVHREEcDBughZp
ZHAtcnoudW5pLW1hbm5oZWltLmRlghhpZHAtdGVzdC51bmktbWFubmhlaW0uZGWC
HGlkcC10ZXN0MS5yei51bmktbWFubmhlaW0uZGWCHGlkcC10ZXN0Mi5yei51bmkt
bWFubmhlaW0uZGUwgYsGA1UdHwSBgzCBgDA+oDygOoY4aHR0cDovL2NkcDEucGNh
LmRmbi5kZS91bmktbWFubmhlaW0tY2EvcHViL2NybC9jYWNybC5jcmwwPqA8oDqG
OGh0dHA6Ly9jZHAyLnBjYS5kZm4uZGUvdW5pLW1hbm5oZWltLWNhL3B1Yi9jcmwv
Y2FjcmwuY3JsMIHZBggrBgEFBQcBAQSBzDCByTAzBggrBgEFBQcwAYYnaHR0cDov
L29jc3AucGNhLmRmbi5kZS9PQ1NQLVNlcnZlci9PQ1NQMEgGCCsGAQUFBzAChjxo
dHRwOi8vY2RwMS5wY2EuZGZuLmRlL3VuaS1tYW5uaGVpbS1jYS9wdWIvY2FjZXJ0
L2NhY2VydC5jcnQwSAYIKwYBBQUHMAKGPGh0dHA6Ly9jZHAyLnBjYS5kZm4uZGUv
dW5pLW1hbm5oZWltLWNhL3B1Yi9jYWNlcnQvY2FjZXJ0LmNydDANBgkqhkiG9w0B
AQsFAAOCAQEAaL/YWQNnXkoPR74lftCu/9Gb1iGKe5mQiYue+1/S3BQzkigDzFF+
ApMDcX7jTjmTEbU4Mz5ESFx2bzTtt9oRsV4nk3DL4rRrPtr+4N6qeEc89nP3s+g9
+4mKVjuAqjkRmurRSRBk1ViHk83Rkp/qXMYuVGxZNVEybAqlUwuUKsLEJbxI59dO
uDLg+zIZrO71Im8ICZWx5wCxGlDVP34WLjRVDu5WkONBqWxXaHcHqRzvll/QgARP
EvVZ2E9wtEdLFufBiCHLF1Md0e7oWFZ8o9hy/VP/8lbCUYEcRTHmkVM/RxEEG32u
Mdmxe+RTVU/wpkZtLqEmdmA+5N9qPacmYw==
',
    ),
    3 => 
    array (
      'encryption' => true,
      'signing' => false,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIHlTCCBn2gAwIBAgIHGVW4qn2PozANBgkqhkiG9w0BAQsFADCByTELMAkGA1UE
BhMCREUxGzAZBgNVBAgTEkJhZGVuLVd1ZXJ0dGVtYmVyZzERMA8GA1UEBxMITWFu
bmhlaW0xHjAcBgNVBAoTFVVuaXZlcnNpdGFldCBNYW5uaGVpbTEWMBQGA1UECxMN
UmVjaGVuemVudHJ1bTEoMCYGA1UEAxMfUlVNLUNBLUcgWmVydGlmaXppZXJ1bmdz
aW5zdGFuejEoMCYGCSqGSIb3DQEJARYZcnVtLWNhQHJ6LnVuaS1tYW5uaGVpbS5k
ZTAeFw0xNTA0MjExMTU3MTVaFw0xODA3MTgxMTU3MTVaMIGYMQswCQYDVQQGEwJE
RTEbMBkGA1UECAwSQmFkZW4tV3VlcnR0ZW1iZXJnMREwDwYDVQQHDAhNYW5uaGVp
bTEeMBwGA1UECgwVVW5pdmVyc2l0YWV0IE1hbm5oZWltMRYwFAYDVQQLDA1SZWNo
ZW56ZW50cnVtMSEwHwYDVQQDDBhpZHAtdGVzdC51bmktbWFubmhlaW0uZGUwggIi
MA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQC2wzcilA+l5yvnlM6YjHIS7xbX
h1dYcRtbI0aKc89o72qBWdAgtB3lILyDNlMO+C6ri0mwFLTurmod385Dc0O29ESI
Hi4tBPmHd6++stKZrneiD/RhirTf7mX/Zkm1tiuK0Uug1U70jOQXy5hlWSSkAgKh
sIFE6hOPFLUqT/Q8J5PJeSHIp4x1u5zxNfuYw8goZPabNBmWfvWavnEfHaiG/xxK
mA56w4gaYJk+qLgFkiGLMx45C2QYxpKTW3iL3czRlck85gDl70SZp5W3GZ+/msCc
Xl2kWgKOemeAmbYRAmaP86K2HCh7GmKmbdXCZUPifLO2fuOxc13RloOh1QNzX1y+
mtO22C05dsFlsjl07Q5VLpQ07+X72A39vx3TuuCVxmdY4/oxndWk1CNOhVTeo09n
GM4XpI6rf9luCINxrIZerc87Rj5T+92GnYailGJ95oDJ9tKQX9PNP9l8IZZqu6go
p4/dlR/a4a6hMkuMvJem8EBwGcqq8lHHozFAd1+13kBmDEUgN0vn1g/9D8/ZemRR
3mHrLFydC6MQGZzAnhPurDASmUIzJs934DLl/IwZYnIcIejNA2kZmQ7NVj6klodD
5RAfWoopuIEX1vl3+lG5kqqBQaMQRPT4EcsURP2fYhDCZRygDigzeI7HPD43G9jt
AWK4VcBl6cfR8bEbswIDAQABo4ICrzCCAqswTwYDVR0gBEgwRjARBg8rBgEEAYGt
IYIsAQEEAwMwEQYPKwYBBAGBrSGCLAIBBAMBMA8GDSsGAQQBga0hgiwBAQQwDQYL
KwYBBAGBrSGCLB4wCQYDVR0TBAIwADALBgNVHQ8EBAMCBeAwHQYDVR0lBBYwFAYI
KwYBBQUHAwIGCCsGAQUFBwMBMB0GA1UdDgQWBBTl2YrMPCMtdaraj22virpY7HL8
9zAfBgNVHSMEGDAWgBRZIClAQtb+jgcuuj2HTzKtZHaODDB3BgNVHREEcDBughZp
ZHAtcnoudW5pLW1hbm5oZWltLmRlghhpZHAtdGVzdC51bmktbWFubmhlaW0uZGWC
HGlkcC10ZXN0MS5yei51bmktbWFubmhlaW0uZGWCHGlkcC10ZXN0Mi5yei51bmkt
bWFubmhlaW0uZGUwgYsGA1UdHwSBgzCBgDA+oDygOoY4aHR0cDovL2NkcDEucGNh
LmRmbi5kZS91bmktbWFubmhlaW0tY2EvcHViL2NybC9jYWNybC5jcmwwPqA8oDqG
OGh0dHA6Ly9jZHAyLnBjYS5kZm4uZGUvdW5pLW1hbm5oZWltLWNhL3B1Yi9jcmwv
Y2FjcmwuY3JsMIHZBggrBgEFBQcBAQSBzDCByTAzBggrBgEFBQcwAYYnaHR0cDov
L29jc3AucGNhLmRmbi5kZS9PQ1NQLVNlcnZlci9PQ1NQMEgGCCsGAQUFBzAChjxo
dHRwOi8vY2RwMS5wY2EuZGZuLmRlL3VuaS1tYW5uaGVpbS1jYS9wdWIvY2FjZXJ0
L2NhY2VydC5jcnQwSAYIKwYBBQUHMAKGPGh0dHA6Ly9jZHAyLnBjYS5kZm4uZGUv
dW5pLW1hbm5oZWltLWNhL3B1Yi9jYWNlcnQvY2FjZXJ0LmNydDANBgkqhkiG9w0B
AQsFAAOCAQEAaL/YWQNnXkoPR74lftCu/9Gb1iGKe5mQiYue+1/S3BQzkigDzFF+
ApMDcX7jTjmTEbU4Mz5ESFx2bzTtt9oRsV4nk3DL4rRrPtr+4N6qeEc89nP3s+g9
+4mKVjuAqjkRmurRSRBk1ViHk83Rkp/qXMYuVGxZNVEybAqlUwuUKsLEJbxI59dO
uDLg+zIZrO71Im8ICZWx5wCxGlDVP34WLjRVDu5WkONBqWxXaHcHqRzvll/QgARP
EvVZ2E9wtEdLFufBiCHLF1Md0e7oWFZ8o9hy/VP/8lbCUYEcRTHmkVM/RxEEG32u
Mdmxe+RTVU/wpkZtLqEmdmA+5N9qPacmYw==
',
    ),
  ),
  'scope' => 
  array (
    0 => 'uni-mannheim.de',
  ),
  'tags' => 
  array (
    0 => 'dfn-aai-advanced',
  ),
  'EntityAttributes' => 
  array (
  ),
  'UIInfo' => 
  array (
  ),
  'DiscoHints' => 
  array (
  ),
  'entityid' => 'https://idp-rz.uni-mannheim.de/idp/shibboleth',
  'metadata-set' => 'attributeauthority-remote',
  'AttributeService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:1.0:bindings:SOAP-binding',
      'Location' => 'https://idp-rz.uni-mannheim.de:8443/idp/profile/SAML1/SOAP/AttributeQuery',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://idp-rz.uni-mannheim.de:8443/idp/profile/SAML2/SOAP/AttributeQuery',
    ),
  ),
  'AssertionIDRequestService' => 
  array (
  ),
  'NameIDFormat' => 
  array (
    0 => 'urn:mace:shibboleth:1.0:nameIdentifier',
    1 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
    2 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
  ),
  'authproc' => 
  array (
    51 => 
    array (
      'class' => 'core:AttributeMap',
      0 => 'oid2name',
    ),
  ),
  'metarefresh:src' => 'https://www.aai.dfn.de/fileadmin/metadata/DFN-AAI-Test-metadata.xml',
);

$metadata['https://idp-test.uni-tuebingen.de/idp/shibboleth'] = array (
  'expire' => 1485558014,
  'protocols' => 
  array (
    0 => 'urn:oasis:names:tc:SAML:1.1:protocol',
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIGNjCCBR6gAwIBAgIHGfZvET3wozANBgkqhkiG9w0BAQsFADB3MQswCQYDVQQG
EwJERTEfMB0GA1UEChMWVW5pdmVyc2l0YWV0IFR1ZWJpbmdlbjEcMBoGA1UEAxMT
R2xvYmFsLVVOSVRVRS1DQSAwMTEpMCcGCSqGSIb3DQEJARYadW5pdHVlLWNhQHVu
aS10dWViaW5nZW4uZGUwHhcNMTUwODIxMDkzODEwWhcNMTgxMTE3MDkzODEwWjCB
tzELMAkGA1UEBhMCREUxGzAZBgNVBAgMEkJhZGVuLVd1ZXJ0dGVtYmVyZzESMBAG
A1UEBwwJVHVlYmluZ2VuMR8wHQYDVQQKDBZVbml2ZXJzaXRhZXQgVHVlYmluZ2Vu
MScwJQYDVQQLDB5aZW50cnVtIGZ1ZXIgRGF0ZW52ZXJhcmJlaXR1bmcxDDAKBgNV
BAsMA1NTTDEfMB0GA1UEAwwWcGJtMTIudW5pLXR1ZWJpbmdlbi5kZTCCASIwDQYJ
KoZIhvcNAQEBBQADggEPADCCAQoCggEBALn7MJa6uO/dN1mJruEkApRL4x0ZQ6BR
OCi1Wn56mClrUEma6btIvMuIiEkvK6We5qy76nme9nHdEBIGh7n9SEwAN9Y1mLN0
ndiuvHbyQm7CUa0TsC5ee8rsU+i131KAnm5A79nvvvq1z2JKq7QATWppWWjOl0/A
x6ItwI5UJWZRVJFa2bPRvsQ1Syx9viAnqQQR9bbUeWlGAD7XFy5eIeZFc8neRu8F
8TcRtI80EpjyCfMvgGMH1PIMyr4SQh0SX2OwQQVUpFCfsllMOHBT5lXL1dbpFl75
aZBbucryvM+uqjAegI2LTq98Bjhkdut02UhkyGRef2xuZu902ibxGF0CAwEAAaOC
AoQwggKAME8GA1UdIARIMEYwEQYPKwYBBAGBrSGCLAEBBAMDMBEGDysGAQQBga0h
giwCAQQDATAPBg0rBgEEAYGtIYIsAQEEMA0GCysGAQQBga0hgiweMAkGA1UdEwQC
MAAwCwYDVR0PBAQDAgXgMB0GA1UdJQQWMBQGCCsGAQUFBwMCBggrBgEFBQcDATAd
BgNVHQ4EFgQUI9Wf6SSGWdb/sIPXWRLQxctSPZkwHwYDVR0jBBgwFoAUsMG7aDV/
4tZBnBpxr+D8QTTNw6gwPAYDVR0RBDUwM4IZaWRwLXRlc3QudW5pLXR1ZWJpbmdl
bi5kZYIWcGJtMTIudW5pLXR1ZWJpbmdlbi5kZTCBkwYDVR0fBIGLMIGIMEKgQKA+
hjxodHRwOi8vY2RwMS5wY2EuZGZuLmRlL2NsYXNzaWMtdW5pdHVlLWNhL3B1Yi9j
cmwvZ19jYWNybC5jcmwwQqBAoD6GPGh0dHA6Ly9jZHAyLnBjYS5kZm4uZGUvY2xh
c3NpYy11bml0dWUtY2EvcHViL2NybC9nX2NhY3JsLmNybDCB4QYIKwYBBQUHAQEE
gdQwgdEwMwYIKwYBBQUHMAGGJ2h0dHA6Ly9vY3NwLnBjYS5kZm4uZGUvT0NTUC1T
ZXJ2ZXIvT0NTUDBMBggrBgEFBQcwAoZAaHR0cDovL2NkcDEucGNhLmRmbi5kZS9j
bGFzc2ljLXVuaXR1ZS1jYS9wdWIvY2FjZXJ0L2dfY2FjZXJ0LmNydDBMBggrBgEF
BQcwAoZAaHR0cDovL2NkcDIucGNhLmRmbi5kZS9jbGFzc2ljLXVuaXR1ZS1jYS9w
dWIvY2FjZXJ0L2dfY2FjZXJ0LmNydDANBgkqhkiG9w0BAQsFAAOCAQEAGV5E8OS2
fHa/I8VhMkiKyDrAp44cSYiGCbwuyGkJRZxuamX6FUa+szCeyeYWsdhBbzTdsfwz
Z4etqz6VBCViH3JqNAbhtpBEWmStPG/Gn3BOCucl5D4gQUyJ4U+8cQQFfSwFfPmu
ZgiAVRaPs3wQR4QESV5YNUHIOaf/fZy6UIjZBzbO0aAUOW5kXvRRs2+jViSuP3a7
U9TmAfvHgGgea59atRFVBIYYtTWxdFOU8u8bqLiyKDOOD5tAc5lDlTHJfQAWrII+
0De5ytGoFp548o+JW5Krrtx4vclTeVp6T4k9NWRsLhTUaftVr4gQuS0i5gFBczzl
L3M61LCYonLIZQ==
',
    ),
    1 => 
    array (
      'encryption' => true,
      'signing' => false,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIGNjCCBR6gAwIBAgIHGfZvET3wozANBgkqhkiG9w0BAQsFADB3MQswCQYDVQQG
EwJERTEfMB0GA1UEChMWVW5pdmVyc2l0YWV0IFR1ZWJpbmdlbjEcMBoGA1UEAxMT
R2xvYmFsLVVOSVRVRS1DQSAwMTEpMCcGCSqGSIb3DQEJARYadW5pdHVlLWNhQHVu
aS10dWViaW5nZW4uZGUwHhcNMTUwODIxMDkzODEwWhcNMTgxMTE3MDkzODEwWjCB
tzELMAkGA1UEBhMCREUxGzAZBgNVBAgMEkJhZGVuLVd1ZXJ0dGVtYmVyZzESMBAG
A1UEBwwJVHVlYmluZ2VuMR8wHQYDVQQKDBZVbml2ZXJzaXRhZXQgVHVlYmluZ2Vu
MScwJQYDVQQLDB5aZW50cnVtIGZ1ZXIgRGF0ZW52ZXJhcmJlaXR1bmcxDDAKBgNV
BAsMA1NTTDEfMB0GA1UEAwwWcGJtMTIudW5pLXR1ZWJpbmdlbi5kZTCCASIwDQYJ
KoZIhvcNAQEBBQADggEPADCCAQoCggEBALn7MJa6uO/dN1mJruEkApRL4x0ZQ6BR
OCi1Wn56mClrUEma6btIvMuIiEkvK6We5qy76nme9nHdEBIGh7n9SEwAN9Y1mLN0
ndiuvHbyQm7CUa0TsC5ee8rsU+i131KAnm5A79nvvvq1z2JKq7QATWppWWjOl0/A
x6ItwI5UJWZRVJFa2bPRvsQ1Syx9viAnqQQR9bbUeWlGAD7XFy5eIeZFc8neRu8F
8TcRtI80EpjyCfMvgGMH1PIMyr4SQh0SX2OwQQVUpFCfsllMOHBT5lXL1dbpFl75
aZBbucryvM+uqjAegI2LTq98Bjhkdut02UhkyGRef2xuZu902ibxGF0CAwEAAaOC
AoQwggKAME8GA1UdIARIMEYwEQYPKwYBBAGBrSGCLAEBBAMDMBEGDysGAQQBga0h
giwCAQQDATAPBg0rBgEEAYGtIYIsAQEEMA0GCysGAQQBga0hgiweMAkGA1UdEwQC
MAAwCwYDVR0PBAQDAgXgMB0GA1UdJQQWMBQGCCsGAQUFBwMCBggrBgEFBQcDATAd
BgNVHQ4EFgQUI9Wf6SSGWdb/sIPXWRLQxctSPZkwHwYDVR0jBBgwFoAUsMG7aDV/
4tZBnBpxr+D8QTTNw6gwPAYDVR0RBDUwM4IZaWRwLXRlc3QudW5pLXR1ZWJpbmdl
bi5kZYIWcGJtMTIudW5pLXR1ZWJpbmdlbi5kZTCBkwYDVR0fBIGLMIGIMEKgQKA+
hjxodHRwOi8vY2RwMS5wY2EuZGZuLmRlL2NsYXNzaWMtdW5pdHVlLWNhL3B1Yi9j
cmwvZ19jYWNybC5jcmwwQqBAoD6GPGh0dHA6Ly9jZHAyLnBjYS5kZm4uZGUvY2xh
c3NpYy11bml0dWUtY2EvcHViL2NybC9nX2NhY3JsLmNybDCB4QYIKwYBBQUHAQEE
gdQwgdEwMwYIKwYBBQUHMAGGJ2h0dHA6Ly9vY3NwLnBjYS5kZm4uZGUvT0NTUC1T
ZXJ2ZXIvT0NTUDBMBggrBgEFBQcwAoZAaHR0cDovL2NkcDEucGNhLmRmbi5kZS9j
bGFzc2ljLXVuaXR1ZS1jYS9wdWIvY2FjZXJ0L2dfY2FjZXJ0LmNydDBMBggrBgEF
BQcwAoZAaHR0cDovL2NkcDIucGNhLmRmbi5kZS9jbGFzc2ljLXVuaXR1ZS1jYS9w
dWIvY2FjZXJ0L2dfY2FjZXJ0LmNydDANBgkqhkiG9w0BAQsFAAOCAQEAGV5E8OS2
fHa/I8VhMkiKyDrAp44cSYiGCbwuyGkJRZxuamX6FUa+szCeyeYWsdhBbzTdsfwz
Z4etqz6VBCViH3JqNAbhtpBEWmStPG/Gn3BOCucl5D4gQUyJ4U+8cQQFfSwFfPmu
ZgiAVRaPs3wQR4QESV5YNUHIOaf/fZy6UIjZBzbO0aAUOW5kXvRRs2+jViSuP3a7
U9TmAfvHgGgea59atRFVBIYYtTWxdFOU8u8bqLiyKDOOD5tAc5lDlTHJfQAWrII+
0De5ytGoFp548o+JW5Krrtx4vclTeVp6T4k9NWRsLhTUaftVr4gQuS0i5gFBczzl
L3M61LCYonLIZQ==
',
    ),
  ),
  'scope' => 
  array (
    0 => 'uni-tuebingen.de',
  ),
  'tags' => 
  array (
    0 => 'dfn-aai-advanced',
  ),
  'EntityAttributes' => 
  array (
  ),
  'UIInfo' => 
  array (
  ),
  'DiscoHints' => 
  array (
  ),
  'entityid' => 'https://idp-test.uni-tuebingen.de/idp/shibboleth',
  'metadata-set' => 'attributeauthority-remote',
  'AttributeService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:1.0:bindings:SOAP-binding',
      'Location' => 'https://idp-test.uni-tuebingen.de:8443/idp/profile/SAML1/SOAP/AttributeQuery',
    ),
  ),
  'AssertionIDRequestService' => 
  array (
  ),
  'NameIDFormat' => 
  array (
    0 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
    1 => 'urn:mace:shibboleth:1.0:nameIdentifier',
    2 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
  ),
  'authproc' => 
  array (
    51 => 
    array (
      'class' => 'core:AttributeMap',
      0 => 'oid2name',
    ),
  ),
  'metarefresh:src' => 'https://www.aai.dfn.de/fileadmin/metadata/DFN-AAI-Test-metadata.xml',
);

$metadata['https://idp.dhbw-mannheim.de/idp/shibboleth'] = array (
  'expire' => 1485558014,
  'protocols' => 
  array (
    0 => 'urn:oasis:names:tc:SAML:1.1:protocol',
    1 => 'urn:oasis:names:tc:SAML:2.0:protocol',
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIFxzCCBK+gAwIBAgIHGN7tjKxAeDANBgkqhkiG9w0BAQsFADByMQswCQYDVQQG
EwJERTEsMCoGA1UEChMjRHVhbGUgSG9jaHNjaHVsZSBCYWRlbi1XdWVydHRlbWJl
cmcxFjAUBgNVBAMTDURIQlcgQ0EgLSBHMDExHTAbBgkqhkiG9w0BCQEWDnBraUBk
aGJ3LXZzLmRlMB4XDTE1MDEyMTA5MjMwOFoXDTE5MDcwOTIzNTkwMFowgZ0xCzAJ
BgNVBAYTAkRFMRswGQYDVQQIExJCYWRlbi1XdWVydHRlbWJlcmcxETAPBgNVBAcT
CE1hbm5oZWltMSwwKgYDVQQKEyNEdWFsZSBIb2Noc2NodWxlIEJhZGVuLVd1ZXJ0
dGVtYmVyZzERMA8GA1UECxMITWFubmhlaW0xHTAbBgNVBAMTFGlkcC5kaGJ3LW1h
bm5oZWltLmRlMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlV1g/eUy
szoQCzDRcHSgN/I/WtkVtcgIf+O0/rAZP8NrLp+J9DhiNx+atE4LGzMoP9N1MHhe
weSHi7YDnM4M/9l2yytdvsImh+R6v48uvcQgHYr78JoqqCCUO3Rraq1eUYyful2G
AW7X0Z911SHxzBi3uNs01Shweruin3rbaJ4aDsn/G5ZUnVXs3Gh9mOotTmScQNKc
mcr+uYBQzods4ZKSdAnBaFCIRcjYGfMnWIaCzSASVG8G7TSlXiRwfhwy6rlySKcd
URk0KbMYBIeiMu7C2kdXyj/0Jq5DfHEhocEYpNp60Ynue8VCq1IoNEsZGGe7v6VQ
gvvQHaRoRpYw+wIDAQABo4ICNDCCAjAwTwYDVR0gBEgwRjARBg8rBgEEAYGtIYIs
AQEEAwMwEQYPKwYBBAGBrSGCLAIBBAMBMA8GDSsGAQQBga0hgiwBAQQwDQYLKwYB
BAGBrSGCLB4wCQYDVR0TBAIwADALBgNVHQ8EBAMCBeAwHQYDVR0lBBYwFAYIKwYB
BQUHAwIGCCsGAQUFBwMBMB0GA1UdDgQWBBQNUhe2yD8Dkxr2raiugwAbOd+yGjAf
BgNVHSMEGDAWgBSM6XBxYLhTn0TGl15CH+0ocr0OfjAfBgNVHREEGDAWghRpZHAu
ZGhidy1tYW5uaGVpbS5kZTB5BgNVHR8EcjBwMDagNKAyhjBodHRwOi8vY2RwMS5w
Y2EuZGZuLmRlL2RoYnctY2EvcHViL2NybC9jYWNybC5jcmwwNqA0oDKGMGh0dHA6
Ly9jZHAyLnBjYS5kZm4uZGUvZGhidy1jYS9wdWIvY3JsL2NhY3JsLmNybDCByQYI
KwYBBQUHAQEEgbwwgbkwMwYIKwYBBQUHMAGGJ2h0dHA6Ly9vY3NwLnBjYS5kZm4u
ZGUvT0NTUC1TZXJ2ZXIvT0NTUDBABggrBgEFBQcwAoY0aHR0cDovL2NkcDEucGNh
LmRmbi5kZS9kaGJ3LWNhL3B1Yi9jYWNlcnQvY2FjZXJ0LmNydDBABggrBgEFBQcw
AoY0aHR0cDovL2NkcDIucGNhLmRmbi5kZS9kaGJ3LWNhL3B1Yi9jYWNlcnQvY2Fj
ZXJ0LmNydDANBgkqhkiG9w0BAQsFAAOCAQEAhCPDn29bbw06XGg2bUpFHu4vdpLV
2vadbmscDCJBZFd80m6D9ftNWMHXOErbtgQE/xf8IVwxxtoh/cq5ipsahcMCJU1P
YsL2ZIdcU1yqS6VcE9Ed18D6GIkcjDQ6K6Sf2v5kXRa4SvBVfgZidAA3fwV3uBPf
f1NpQ4qS+7MUpHnJDMzlrRJAU7lRDVDFQ4D+Ot0c+57PSjF+Z08S5iB1e3MbPhcJ
MEAhV5uqARdSWaVrN7Nrz8yapcl7/6syIE2qpy8ch1tiTlmlrHZQNX0Hk9+GO7RG
Ecu/TbJ6i7kOIPN0lULLlvkVxPUYCv1ZOpBJ/n7XsYaKgGk4QoVM3Zzpdw==
',
    ),
  ),
  'scope' => 
  array (
    0 => 'dhbw-mannheim.de',
  ),
  'tags' => 
  array (
    0 => 'dfn-aai-advanced',
  ),
  'EntityAttributes' => 
  array (
  ),
  'UIInfo' => 
  array (
  ),
  'DiscoHints' => 
  array (
  ),
  'entityid' => 'https://idp.dhbw-mannheim.de/idp/shibboleth',
  'metadata-set' => 'attributeauthority-remote',
  'AttributeService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:1.0:bindings:SOAP-binding',
      'Location' => 'https://idp.dhbw-mannheim.de:8443/idp/profile/SAML1/SOAP/AttributeQuery',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://idp.dhbw-mannheim.de:8443/idp/profile/SAML2/SOAP/AttributeQuery',
    ),
  ),
  'AssertionIDRequestService' => 
  array (
  ),
  'NameIDFormat' => 
  array (
    0 => 'urn:mace:shibboleth:1.0:nameIdentifier',
    1 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
  ),
  'authproc' => 
  array (
    51 => 
    array (
      'class' => 'core:AttributeMap',
      0 => 'oid2name',
    ),
  ),
  'metarefresh:src' => 'https://www.aai.dfn.de/fileadmin/metadata/DFN-AAI-Test-metadata.xml',
);

$metadata['https://idptest.scc.kit.edu/idp/shibboleth'] = array (
  'expire' => 1485558014,
  'protocols' => 
  array (
    0 => 'urn:oasis:names:tc:SAML:1.1:protocol',
    1 => 'urn:oasis:names:tc:SAML:2.0:protocol',
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIGCDCCBPCgAwIBAgIHG5mPz3JZgDANBgkqhkiG9w0BAQsFADCBvzELMAkGA1UE
BhMCREUxGzAZBgNVBAgTEkJhZGVuLVd1ZXJ0dGVtYmVyZzESMBAGA1UEBxMJS2Fy
bHNydWhlMSowKAYDVQQKEyFLYXJsc3J1aGUgSW5zdGl0dXRlIG9mIFRlY2hub2xv
Z3kxJzAlBgNVBAsTHlN0ZWluYnVjaCBDZW50cmUgZm9yIENvbXB1dGluZzEPMA0G
A1UEAxMGS0lULUNBMRkwFwYJKoZIhvcNAQkBFgpjYUBraXQuZWR1MB4XDTE2MDcw
NDA3MzczNVoXDTE5MDcwNDA3MzczNVowgYgxCzAJBgNVBAYTAkRFMRswGQYDVQQI
DBJCYWRlbi1XdWVydHRlbWJlcmcxEjAQBgNVBAcMCUthcmxzcnVoZTEqMCgGA1UE
CgwhS2FybHNydWhlIEluc3RpdHV0ZSBvZiBUZWNobm9sb2d5MRwwGgYDVQQDDBNp
ZHB0ZXN0LnNjYy5raXQuZWR1MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKC
AQEAncZQvTdQnNfCibKIeL/hFOgQ0R4GOkS9fMSCsO5lBvVswz1g42CNvvHhVdCz
jpId8d8oVfOAI2wMP1skooTZkTyon0V2NgifMT41e9ich+7uJZ75fUqol5Iq7Wya
VMSjIe/aOD9ZQ+BqIiGVnIyZD6Xq0fi+WL54WDoIUJhCUSjlnaiaFlZp1crBj3+y
jrO4RdO3oy8U2sx7X+PJ0JkxOTazbJiN2oy/ZBI1Ayfn9gnFnVSU1ZfnqmReh/FN
nK+A7NIJOniYm4NTEmAp89PbT9jI0B9zgO/mFwD8O4qyQ7am5z8mZcCwkA3xsEC/
CmdJkSaNScMbJkR7KKfOueCr9QIDAQABo4ICPDCCAjgwWQYDVR0gBFIwUDARBg8r
BgEEAYGtIYIsAQEEAwUwEQYPKwYBBAGBrSGCLAIBBAMBMA8GDSsGAQQBga0hgiwB
AQQwDQYLKwYBBAGBrSGCLB4wCAYGZ4EMAQICMAkGA1UdEwQCMAAwDgYDVR0PAQH/
BAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMCBggrBgEFBQcDATAdBgNVHQ4EFgQU
7MuoN6WUEem0asDx8+BflBfBWqUwHwYDVR0jBBgwFoAUH3Rl9JodevYx6d9hG3Mr
DW3QM0kwHgYDVR0RBBcwFYITaWRwdGVzdC5zY2Mua2l0LmVkdTB3BgNVHR8EcDBu
MDWgM6Axhi9odHRwOi8vY2RwMS5wY2EuZGZuLmRlL2tpdC1jYS9wdWIvY3JsL2Nh
Y3JsLmNybDA1oDOgMYYvaHR0cDovL2NkcDIucGNhLmRmbi5kZS9raXQtY2EvcHVi
L2NybC9jYWNybC5jcmwwgccGCCsGAQUFBwEBBIG6MIG3MDMGCCsGAQUFBzABhido
dHRwOi8vb2NzcC5wY2EuZGZuLmRlL09DU1AtU2VydmVyL09DU1AwPwYIKwYBBQUH
MAKGM2h0dHA6Ly9jZHAxLnBjYS5kZm4uZGUva2l0LWNhL3B1Yi9jYWNlcnQvY2Fj
ZXJ0LmNydDA/BggrBgEFBQcwAoYzaHR0cDovL2NkcDIucGNhLmRmbi5kZS9raXQt
Y2EvcHViL2NhY2VydC9jYWNlcnQuY3J0MA0GCSqGSIb3DQEBCwUAA4IBAQDDzzWb
JJidypaoSxFTTtIPa3s7760ekxpKeV2ovq/T5FYuPARNEtOBhchqXdvBjeP/DSta
zQuBJCr7Z4mbkWAUTaUS8EqSNroQq2ny/kiNPgtjhNRLYX2KbBeafquHyayYTARP
ok3jVFJpbz4Gqpxy6Qg+IucStNjHv0UsK3OEdq/aF35p8hU6CmzG9/fjdREB9x6r
H2ujtgQS6exNeWShny3DhN8nPyqzIJLeXM1aawOMUPXCwUArGbUSLmdrfAHfHbXy
7lD95SUuuA3UNV4c9e0bXmIrjRFVJC8a2vPrt3DFmIwCF6P18BPt7CzwyCcPHT7i
xG8Jb/0RqJw3u6Le
',
    ),
  ),
  'scope' => 
  array (
    0 => 'kit.edu',
    1 => 'student.kit.edu',
    2 => 'partner.kit.edu',
  ),
  'tags' => 
  array (
    0 => 'dfn-aai-advanced',
  ),
  'EntityAttributes' => 
  array (
  ),
  'UIInfo' => 
  array (
  ),
  'DiscoHints' => 
  array (
  ),
  'entityid' => 'https://idptest.scc.kit.edu/idp/shibboleth',
  'metadata-set' => 'attributeauthority-remote',
  'AttributeService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:1.0:bindings:SOAP-binding',
      'Location' => 'https://idptest.scc.kit.edu:8443/idp/profile/SAML1/SOAP/AttributeQuery',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://idptest.scc.kit.edu:8443/idp/profile/SAML2/SOAP/AttributeQuery',
    ),
  ),
  'AssertionIDRequestService' => 
  array (
  ),
  'NameIDFormat' => 
  array (
    0 => 'urn:mace:shibboleth:1.0:nameIdentifier',
    1 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
    2 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
  ),
  'authproc' => 
  array (
    51 => 
    array (
      'class' => 'core:AttributeMap',
      0 => 'oid2name',
    ),
  ),
  'metarefresh:src' => 'https://www.aai.dfn.de/fileadmin/metadata/DFN-AAI-Test-metadata.xml',
);

?>