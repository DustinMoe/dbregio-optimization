<?php
return array(
  # DFN Föderation Advanced, nur Mannheim importieren
  array(
    'whitelist' => array(
        'https://idp-rz.uni-mannheim.de/idp/shibboleth', # Uni Mannheim - Test
	'https://idptest.scc.kit.edu/idp/shibboleth', # Karlsruhe Institute of Technology (KIT) - Test
	'https://idp.dhbw-mannheim.de/idp/shibboleth', # Duale Hochschule BaWü Mannheim
        'https://idp-test.uni-tuebingen.de/idp/shibboleth', # Uni Tübingen - Test
    ),
    'src' => 'https://www.aai.dfn.de/fileadmin/metadata/DFN-AAI-Test-metadata.xml',
    'template' => array(
      'tags'    => array('dfn-aai-advanced'),
      'authproc' => array(
        51 => array('class' => 'core:AttributeMap', 'oid2name'),
      ),
    ),
  ),
);